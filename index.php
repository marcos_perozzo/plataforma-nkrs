<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

////////////////////////////
// Version 1.1.0
////////////////////////////

//The main include to call the model, view, controller and JWT vendor of the authentication
include("vendor/firebase/php-jwt/src/JWT.php");
include("vendor/firebase/php-jwt/src/BeforeValidException.php");
include("vendor/firebase/php-jwt/src/ExpiredException.php");
include("vendor/firebase/php-jwt/src/SignatureInvalidException.php");

setlocale(LC_ALL, "pt_BR", "ptb");
date_default_timezone_set('America/Sao_Paulo');
mb_internal_encoding("UTF-8");

require_once "includes/main.php";

//Variable to init the system get from the htaccess
if (!empty($_GET["v1"])) {
	switch($_GET["v1"]){
		case "auth/recuperar-senha":
			$receivedURL = "auth/forgot";
			break;
		case "auth/redefinir-senha":
			$receivedURL = "auth/reset";
			break;
		case "usuario/cadastro":
			$receivedURL = "user/add";
			break;
		case "usuario/editar":
			$receivedURL = "user/edit";
			break;
		case "painel":
			$receivedURL = "panel";
			break;
		case "painel/editar-senha":
			$receivedURL = "panel/editPassword";
			break;
		case "painel/editar-usuario":
			$receivedURL = "panel/editUser";
			break;
		case "painel/enderecos":
			$receivedURL = "panel/address";
			break;
		case "painel/enderecos-editar":
			$receivedURL = "panel/crudAddress";
			break;
		case "painel/enderecos-novo":
			$receivedURL = "panel/crudAddress";
			break;
		case "painel/resultados":
			$receivedURL = "panel/results";
			break;
		case "painel/webcup":
			$receivedURL = "panel/webcup";
			break;
		case "painel/webcup-editar":
			$receivedURL = "panel/editWebcup";
			break;
		case "painel/crowdfounding":
			$receivedURL = "panel/crowdfounding";
			break;
		case "painel/crowdfounding-novo":
			$receivedURL = "panel/crudCrowdfounding";
			break;
		case "painel/crowdfounding-editar":
			$receivedURL = "panel/crudCrowdfounding";
			break;
		case "sobre":
			$receivedURL = "about";
			break;
		case "apoiar":
			$receivedURL = "crowdfounding/index";
			break;
		case "inscricoes":
			$receivedURL = "signed";
			break;
		case "blog":
			$receivedURL = "blog/index";
			break;
		case "blog/detalhes":
			$receivedURL = "blog/details";
			break;
		case "contato":
			$receivedURL = "contact";
			break;
		case "loja":
			$receivedURL = "products";
			break;
		case "loja/detalhes":
			$receivedURL = "products/details";
			break;
		case "carrinho":
			$receivedURL = "cart";
			break;
		default:
			$receivedURL = $_GET["v1"];
			break;
	}
} else {
	$receivedURL = false;
}
$application = new application($receivedURL);
$application->index();
?>