"use strict";
var app;

function Main() {
	this.init();
};

Main.prototype.init = function(){
	var self = this;  
	self.sendForm();
	//self.timerCounter();
	self.initAos();
	
    if($(window).width() <= 991){
		self.triggerMenu();
	}

	$('input[type="tel"]').mask('(00) 00000-0000');
	$('[name="cpf"]').mask('000.000.000-00');
	$('[name="date"]').mask('00/00/0000');
	$('[name="birthdate"]').mask('00/00/0000');
	$('[name="zipcode"]').mask('00000-000');
		
};

Main.prototype.initAos = function(){
	setTimeout(() => {
		AOS.init()
	}, 500);
}

Main.prototype.triggerMenu = function() {
	$('[data-navbar]').on('click', function(e){
		e.preventDefault();
		var btn = $(this),
			menu = $('.navbar-collapse');

		if(menu.hasClass('collapse'))
			menu.removeClass('collapse');
		else
			menu.addClass('collapse');
	});
}

Main.prototype.initSlick = function() {
    $('[data-slickProducts]').slick({
        variableWidth: false,
        touchMove: true,
        swipe: false,
        infinite: true,
        prevArrow: false,
        nextArrow: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
					slidesToShow: 1,
					touchMove: true
                }
            }
        ]
    });

    $('.products-carousel__next').click(function(){
		$('[data-slickProducts]').slick('slickNext');
	});	  
	$('.products-carousel__prev').click(function(){
		$('[data-slickProducts]').slick('slickPrev');
	});
}

Main.prototype.isJson = function(str){
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

Main.prototype.sendForm = function(){
	var self = this;
	if ($('[data-validate]').length) {

		$('[data-validate]').each(function(index, el) {
			var form = $(this);
			$(this).validate({
				onkeyup: false,
				onclick: false,
				onfocusout: false,
				errorPlacement: function(error, element) {
					//element.before(error);
					var count = 0;
					var fields = form.serializeArray();

					fields.forEach(function(field){
						if(!field.value)
							count++;
					});

					if(count > 1 && count <= fields.length)
						swal({ title: "Ops!", text: 'Preencha os campos obrigatórios', type: "warning"});
					else
						swal({ title: "Ops!", text: error[0].textContent, type: "warning"});
					
					return false;
				},
				messages: {
					name: {
						required: "Campo Nome é obrigatório",
						minlength: "Campo Nome é obrigatório",
					},
					last_name: {
						required: "Campo Sobrenome é obrigatório",
						minlength: "Campo Sobrenome é obrigatório",
					},
					cpf: {
						required: "Campo CPF é obrigatório",
					},
					email: {
						required : "Campo E-mail é obrigatório",
						email : "Por favor insira um e-mail válido"
					},
					phone: {
						required : "Campo Telefone é obrigatório",
						number : "Campo telefone tem que ser numérico",
						minlength: "Campo Telefone é obrigatório",
					},
					phone_mobile: {
						required : "Campo Celular é obrigatório",
						number : "Campo Celular tem que ser numérico",
						minlength: "Campo Celular é obrigatório",
					},
					gender: {
						required : "Selecione o sexo",
					},
					birthdate: {
						required : "Campo Data de Nascimento é obrigatório",
					},
					message: {
						required : "Campo Mensagem é obrigatório"
					},
					password: {
						required: "O campo senha é obrigatório"
					},
					password_confirm: {
						required: "Você precisa repetir a senha"
					},
					terms: {
						required: "Você deve aceitar os termos de uso"
					}
				},
				submitHandler: function(form){
					$.ajax({
						type        : $(form).attr('method'),
						url         : $(form).attr('action'),
						data        : $(form).serialize(),
						dataType    : "json",
						beforeSend: function(){

							$(form).find('button').addClass('load');
							$(form).find('input,button').prop("disabled", true);
						},
						success:function(response) {
							if(self.isJson(response)){
								response = JSON.parse(response);
							}
							console.log(response);

							if (response.status === true) {
								if(!!response.message)
									swal({ title: "Sucesso!", text: response.message , type: "success"});

								$(form).find('input, button').prop("disabled", false);
								//form.reset();

								if(!!response.data.url){
									setTimeout(()=>{
										window.location.href = response.data.url;
									}, 4000);
								}
							} else {
								swal({ title: "Ops!", text: response.message , type: "warning"});
								$(form).find('input,button').prop("disabled", false);
								$(form).find('button').removeClass('load');
							}
						},
						error:function(){
							swal({title:"Erro!" , text: 'Não foi possível conectar aos serviços.' , type: "error" });
							$(form).find('input,button').prop("disabled", false);
							$(form).find('button').removeClass('load');
						}
					});
				}
			});
		});
	}
}

Main.prototype.timerCounter = () => {
	window.onload = function(e){
    
		var $clock = $('#timer'),
			eventTime = moment('27-11-2020 08:30:00', 'DD-MM-YYYY HH:mm:ss').unix(),
			currentTime = moment().unix(),
			diffTime = eventTime - currentTime,
			duration = moment.duration(diffTime * 1000, 'milliseconds'),
			interval = 1000;
	
		// if time to countdown
		if(diffTime > 0) {
	
			// Show clock
			// $clock.show();
	
			var $d = $('<div class="timer-value"><span>00</span><small>Dias</small></div>').appendTo($clock),
				$h = $('<div class="timer-value"><span>00</span><small>Horas</small></div>').appendTo($clock),
				$m = $('<div class="timer-value"><span>00</span><small>Minutos</small></div>').appendTo($clock),
				$s = $('<div class="timer-value"><span>00</span><small>Segundos</small></div>').appendTo($clock);
	
			setInterval(function(){
	
				duration = moment.duration(duration.asMilliseconds() - interval, 'milliseconds');
				var d = moment.duration(duration).days(),
					h = moment.duration(duration).hours(),
					m = moment.duration(duration).minutes(),
					s = moment.duration(duration).seconds();
	
				d = $.trim(d).length === 1 ? '0' + d : d;
				h = $.trim(h).length === 1 ? '0' + h : h;
				m = $.trim(m).length === 1 ? '0' + m : m;
				s = $.trim(s).length === 1 ? '0' + s : s;
	
				// show how many hours, minutes and seconds are left
				$d.find('span').text(d);
				$h.find('span').text(h);
				$m.find('span').text(m);
				$s.find('span').text(s);
	
			}, interval);
	
		}
	}
}

Main.prototype.doSearch = function() {
	$('[data-search]').on('submit', function(e){
		e.preventDefault();

		$.ajax({
			url: $(this).attr('action'),
			method: 'POST',
			data: $(this).val(),
			error: function(response){
				console.log(response);
				swal({title:"Ops!", text: "Não foi possível fazer a busca =/", type: "error"});
			}
		});
	});
}

$(document).ready(function(){
	app = new Main();
});
