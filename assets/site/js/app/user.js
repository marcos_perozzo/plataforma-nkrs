"use strict";

function User() {
    this.init();
};

User.prototype.init = function(){
    var self = this;
    self.logout();
};

User.prototype.logout = function(){
    $('[data-logout]').on('click', function(e){
        e.preventDefault();

        $.ajax({
            url: $(this).data('logout'),
            method: 'POST',
            success: function(response){
                response = JSON.parse(response);
                if(!!response.status){
                    swal({ title: "Sucesso!", text: response.message , type: "success"});
                    setTimeout(()=>{ window.location.href = response.url },4000);
                }
            },
            error: function(response){
                swal({ title: "Erro!", text: response.message , type: "error"});
            }
        });
    });
    
}

$(document).ready(function(){
    new User();
});
