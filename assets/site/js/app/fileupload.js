"use strict";

function Fileupload() {
    this.init();
};

Fileupload.prototype.init = function(){
    console.log("fileupload() - init");
    var self = this;
    $('#fileupload').fileupload({
        dataType: 'json',
        done: function (e, data) {
            if(!!data.result.status){
                $('input[name="cover"]').val(data.result.cover);
                $('.preview-files').html(data.result.preview);
                //window.setTimeout(self.reset(), 2000);             
            } else {
                swal({ title: "Erro!", text: "Houve um erro no upload." , type: "error"});
            }
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css('width', progress + '%');
        }
    });
};

Fileupload.prototype.reset = function(){
    $('#progress .progress-bar').css('width', '0%');
}

$(document).ready(function(){
    new Fileupload();
});
