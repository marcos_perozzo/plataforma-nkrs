"use strict";

function Cart() {
    this.init();
};

Cart.prototype.init = function(){
    var self = this;
    self.add();
};

Cart.prototype.add = function(){
    $('[data-cart]').on('click',function(e){
        e.preventDefault();

        if(!!$('[name="additional"]').val() && !!$('[name="optional"]').val()){
            let data = {
                product: $('[name="product"]').val(),
                quantity: $('[name="quantity"]').val(),
                additional: $('[name="additional"]').val(),
                optional: $('[name="optional"]').val()
            }
    
            $.ajax({
                url: $(this).data('url'),
                method: 'POST',
                data: data,
                success: function(response){
                    console.log('Cart add', response);
                },
                error: function(response){
                    console.log('Cart Error', response);
                    swal({title: "Erro!", text:"Não foi possível acessar os serviços.", type: 'error'});
                }
            });
        } else {
            swal({title: "Ops!", text:"Selecione as opções do produto.", type: 'warning'});
        }

    });
}

$(document).ready(function(){
    new Cart();
});
