$(function () {
    'use strict';

    //////////////////////////////////
    //Auth functions
    //////////////////////////////////

	if ($("div.error").length>0) {
		var msg = $('div.error').html(),
		  type = "error",
		  layout = "loginError";

		$.noty.layouts.loginError = {
		  name: 'top',
		  options: {},
		  container: {
		    object: '<ul class="app-noty-top login-screen" />',
		    selector: 'ul.app-noty-top',
		    style: function () {}
		  },
		  parent: {
		    object: '<li />',
		    selector: 'li',
		    css: {}
		  },
		  css: {
		    display: 'none'
		  },
		  addClass: ''
		};

		noty({
		  theme: 'app-noty',
		  text: msg,
		  type: type,
		  timeout: false,
		  layout: layout,
		  closeWith: ['button', 'click'],
		  animation: {
		    open: 'animated bounceInDown',
		    close: 'animated bounceOutUp'
		  },
		});
	}
	$('#rememberPass').click(function(event){
		var $vldtRemember=$('.forgot').validate({
		  lang: 'pt-br',
		  rules: {
		    username: {
		      required: true,
		      email: true
		    }
		  }
		});

		var $validRemember = $('.forgot').valid();

		if (!$validRemember) {
		  $vldtRemember.focusInvalid();
		  event.preventDefault();
		  return false;
		} else {
		  event.preventDefault();
		  var actionUrl=$("form.forgot").attr("action");
		  var username=$("input[name='username']").val();

		  $.ajax({
		    url : actionUrl,
		    type: "POST",
		    data : {username:username},
		    success: function(data, textStatus, jqXHR) {
		      if (data.status=="ok") {
		        var msg = data.message,
		            type = "success",
		            layout = "forgotAlert";
		            $("input[name='username']").val("");
		      } else {
		        var msg = data.message,
		            type = "error",
		            layout = "forgotAlert";
		      }

		      $.noty.layouts.forgotAlert = {
		        name: 'top',
		        options: {},
		        container: {
		          object: '<ul class="app-noty-top forgot-screen" />',
		          selector: 'ul.app-noty-top',
		          style: function () {}
		        },
		        parent: {
		          object: '<li />',
		          selector: 'li',
		          css: {}
		        },
		        css: {
		          display: 'none'
		        },
		        addClass: ''
		      };

		      noty({
		        theme: 'app-noty',
		        text: msg,
		        type: type,
		        timeout: 6000,
		        layout: layout,
		        closeWith: ['button', 'click'],
		        animation: {
		          open: 'animated tada',
		          close: 'animated zoomOutDown'
		        },
		      });
		    },
		    error: function (jqXHR, textStatus, errorThrown) {
		      var errorReturn=JSON.parse(jqXHR.responseText)
		      alert(errorReturn.message);
		    }
		  });
		}
	});

	$('#resetPass').click(function(event){
		var $vldtReset=$('.resetPass').validate({
		  lang: 'pt-br',
		  rules: {
		    username: {
		      required: true,
		      email: true
		    },
		    password: {
		      required: true,
		      minlength: 6
		    },
		    password_confirmation: {
		      equalTo: "#password",
		      required: true,
		      minlength: 6
		    },
		    hash: {
		      required: true
		    }
		  }
		});

		var $validReset = $('.resetPass').valid();

		if (!$validReset) {
		  $vldtReset.focusInvalid();
		  event.preventDefault();
		  return false;
		} else {
		  event.preventDefault();
		  var actionUrl=$("form.resetPass").attr("action");
		  var username=$("input[name='username']").val();
		  var hash=$("input[name='hash']").val();
		  var password=$("input[name='password']").val();

		  $.ajax({
		    url : actionUrl,
		    type: "POST",
		    data : {username:username,hash:hash,password:password},
		    success: function(data, textStatus, jqXHR) {
		      if (data.status=="ok") {
		        var msg = data.message,
		            type = "success",
		            layout = "forgotAlert";
		            $("input[name='username']").val("");
		      } else {
		        var msg = data.message,
		            type = "error",
		            layout = "forgotAlert";
		      }

		      $.noty.layouts.forgotAlert = {
		        name: 'top',
		        options: {},
		        container: {
		          object: '<ul class="app-noty-top forgot-screen" />',
		          selector: 'ul.app-noty-top',
		          style: function () {}
		        },
		        parent: {
		          object: '<li />',
		          selector: 'li',
		          css: {}
		        },
		        css: {
		          display: 'none'
		        },
		        addClass: ''
		      };

		      noty({
		        theme: 'app-noty',
		        text: msg,
		        type: type,
		        timeout: 6000,
		        layout: layout,
		        closeWith: ['button', 'click'],
		        animation: {
		          open: 'animated zoomInDown',
		          close: 'animated zoomOutDown'
		        },
		      });
		    },
		    error: function (jqXHR, textStatus, errorThrown) {
		      var errorReturn=JSON.parse(jqXHR.responseText)
		      alert(errorReturn.message);
		    }
		  });
		}
	});

});