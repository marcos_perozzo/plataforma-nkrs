<?php
function getBaseUrl() {
	$currentPath = $_SERVER['PHP_SELF']; 
	$pathInfo = pathinfo($currentPath); 
	$hostName = $_SERVER['HTTP_HOST']; 
	if (!empty($_SERVER['HTTPS'])) {
		$protocol="https://";
	} else {
		$protocol="http://";
	}
	$return=$protocol.$hostName.$pathInfo['dirname'];
	if (substr($return, -1)=="/") {
		return $return;
	} else {
		return $return."/";
	}
}

require_once "config.php";
require_once "connect.php";
require_once "helpers.php";

class application {

	public $url;
	public $method;
	public $controller;
	public $controllerName;

	function __construct ($urlRecebida) {
		//Ajusta URL
		$urlRecebida=str_replace(array("-"),"",$urlRecebida);
		//Faz um array com os parametros das variáveis. O index zero é o Controller
		$urlRecebida=explode("/", $urlRecebida);

		$url=array();
		foreach ($urlRecebida as $urlLimpa)  {
			array_push($url, protegeURL($urlLimpa));
		}

		//Lê a URL
		$controllerURL=$url[0];

		//Compara o controller, se for referente ao ADM cria variáveis específicas, caso contrário, mantém a ordem da URL
		if ($controllerURL=="adm") {
			$controllerURL=$url[1];
			if (empty($controllerURL)) {
				$controllerName="ADMhome_Controller";
			} else {
				$controllerName="ADM".$url[1]."_Controller";
			}
			$method=$url[2];
		} else {
			if (empty($controllerURL)) {
				$controllerName="home_Controller";
			} else {
				$controllerName=$url[0]."_Controller";
			}
			$method=$url[1];
		}

		//Função que faz o load do da classe que estiver sendo solicitada
		function __autoload($classe) {			
			$admDir = substr($classe, 0, 3);
			if ($admDir=="ADM") {
				$admDir="adm/";
			} else {
				$admDir=false;
			}

			$tipo=explode("_",$classe);
			$tipo=$tipo[1]; // Pega o tipo da classe, se é um controller ou uma view.

			try {
				if (!@include_once( "includes/".$tipo."/".$admDir.$classe.".php" )) {
					throw new Exception ("O arquivo includes/".$tipo."/".$admDir.$classe.".php não existe. A classe não foi carregada.");
				}
			}
			catch(Exception $e) {    
				$errno = "404";
				$erro=$e->getMessage();
				error_log($erro);
				$dadosErro["erro"]=array("titulo"=>"ERRO ".$errno,"conteudo"=>$erro,"linkADM"=>false);
				loadView("__erro",$dadosErro);
				exit;
			}
		}

		$controller = new $controllerName();		

		// Cache das páginas para poupar rendimento
		if ($cache) {
			header('Cache-Control: max-age=3600, public');
			header('Pragma: cache');
			header("Last-Modified: ".gmdate("D, d M Y H:i:s",time())." GMT");
			header("Expires: ".gmdate("D, d M Y H:i:s",time()+3600)." GMT");
		}
		header("Content-Type: text/html; charset=utf-8");
		header("X-XSS-Protection: 1; mode=block");
		$this->url=$url;
		$this->method=$method;
		$this->controller=$controller;
		$this->controllerName=$controllerName;
	}
	
	function index () {
		global $publicado;
		
		$url = $this->url;
		$method = $this->method;
		$controller = $this->controller;
		$controllerName = $this->controllerName;

		$admDir = substr($controllerName, 0, 3);
		if ($admDir=="ADM") {
			$admDir=true;
		} else {
			$admDir=false;
		}

		if (!$admDir) {
			if (!$publicado) {
				loadView("placeholder/index");
				die;
			}
		}

		//Executa a index do controlador ou a sua função se for o caso da URL estar informando
		if ($method) {
			$parametros=$url;
			unset($parametros[0]);
			unset($parametros[1]);
			if ($admDir) {
				unset($parametros[2]);
			}
			ksort($parametros);
			$parametros = array_values($parametros); // re-ordena o array para ficar com as keys em ordem, partindo do zero
			if (method_exists($controller,$method)) { // verifica se o método da classe existe, se não existe chama um erro 404
				$controller->$method($parametros);
			} else {
				$slugParameters=$url;
				unset($slugParameters[0]);
				ksort($slugParameters);
				$slugParameters = array_values($slugParameters); // reorder the array to make the first key as zero
				$controller->index($slugParameters);

				//OLD WAY WITH 404 ERROR IF THE METHOD DOESN'T EXIST
				// $data["erro"]["titulo"]="ERRO 404";
				// $data["erro"]["contedo"]="Ops, não encotramos a página que você procurava.";
				// loadView("__erro",$data);
				// exit;
			}
		} else {
			$controller->index();
		}

	}

}
?>