<?php
class user_Controller extends common_Controller {

	function index() {
		$modelSite=new site_Model();
		
		$page = $modelSite->getPage(1);
		$data['title'] = $page[0]->title;
		$data['metaDescription'] = $page[0]->description;
		$data['keywords'] = $page[0]->keywords;

		$data['jsApp']=array('home.js');

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		//Model to catch the interface content - You can edit this line
		// $data["content"]=$modelGeral->listaItens("interface",0,999999999,false,"no");
		loadView(array("layout/_header","pages/index","layout/_footer"),$data);
	}

	function add($error = false) {
        $data["title"] = "Cadastro de usuário - ".$this->nome_site;
        $data["page_name"] = "Cadastro";
		$data["message"] = $error;

		loadView(array("layout/_header","pages/user/form","layout/_footer"),$data);
    }

    function edit($error = false) {
        $data["title"]="Editar usuário - ".$this->nome_site;
		$data["message"]=$error;
		$data['content'] = "includes/View/pages/user/form.php";
		$data["page_name"] = "Editar";

		// Puxar informações do usuário

		$data['jsApp'] = "user.js";

		loadView(array("layout/_header","pages/user/form","layout/_footer"),$data);
    }

    function delete($error = false) {
        $data["siteData"]["title"]="Excluir usuário - ".$this->nome_site;
		$data["message"]=$error;
		loadView(array("layout/_header","pages/user/form","layout/_footer"),$data);
    }

    function save($error = false) {
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Client.php";
		$storetrooper = new ST_Client;
		$modelGeral = new geral_Model();
		
        postRequest();

		if(!empty($_POST)){
			$data['id']				  = isset($_POST['id']) ? intval($_POST['id']) : false;
			$data['name'] 	  	  	  = $_POST['name'];
			$data['last_name'] 	  	  = $_POST['last_name'];
			$data['email'] 	  	  	  = $_POST['email'];
			$data['phone'] 	  	  	  = !empty($_POST['phone']) ? $_POST['phone'] : '(00) 0000-0000';
			$data['phone_mobile'] 	  = $_POST['phone_mobile'];
			$data['gender']  	  	  = $_POST['gender'];
			$data['cpf']  		  	  = $_POST['cpf'];
			$data['birthdate']    	  = $_POST['birthdate'];
			$data['password']     	  = $_POST['password'];
			$data['password_confirm'] = $_POST['password_confirm'];

			$validCPF = $this->validateCpf($data['cpf']);
			$samePassword = $data['password'] === $data['password_confirm'] ? true : false;
			//Tirar a validação das senhas se for edição
			if($validCPF == true && $samePassword == true){
				//Salvar no banco e montar e-mail para a Morderniza com os dados
				//dbSave("contacts",$data);
				if(!isset($data['id'])){
					$response = json_decode($storetrooper->add($data));
					if($response->status == true){
						$response->message = "Seu cadastro foi finalizado com sucesso!";
						$response->url = $this->endereco_site."auth/login";
					}
				} else {
					$response = json_decode($storetrooper->edit($data));
					if($response->status == true){
						$response->message = "Seus dados foram editados com sucesso!";
						$response->url = $this->endereco_site."painel";
					}
				}
			} else {
				$response['status'] = false;
				if(!$validCPF){
					$response['message'] = "CPF inválido!";
				}
				if(!$samePassword){
					$response['message'] = "As senhas não conferem!";
				}
			}
		} else {
			$response['status'] = false;
			$response['message'] = "Houve um erro! Tente novamente mais tarde.";
		}

		echo json_encode($response);
	}
	
	function validateCpf($cpf){
		// Verifica se um número foi informado
		if(empty($cpf)) {
			return false;
		}

		// Elimina possivel mascara
		$cpf = preg_replace("/[^0-9]/", "", $cpf);
		$cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
		
		// Verifica se o numero de digitos informados é igual a 11 
		if (strlen($cpf) != 11) {
			return false;
		}
		// Verifica se nenhuma das sequências invalidas abaixo 
		// foi digitada. Caso afirmativo, retorna falso
		else if ($cpf == '00000000000' || 
			$cpf == '11111111111' || 
			$cpf == '22222222222' || 
			$cpf == '33333333333' || 
			$cpf == '44444444444' || 
			$cpf == '55555555555' || 
			$cpf == '66666666666' || 
			$cpf == '77777777777' || 
			$cpf == '88888888888' || 
			$cpf == '99999999999') {
			return false;
		// Calcula os digitos verificadores para verificar se o
		// CPF é válido
		} else {   
			
			for ($t = 9; $t < 11; $t++) {
				
				for ($d = 0, $c = 0; $c < $t; $c++) {
					$d += $cpf{$c} * (($t + 1) - $c);
				}
				$d = ((10 * $d) % 11) % 10;
				if ($cpf{$c} != $d) {
					return false;
				}
			}

			return true;
		}
	}
}
?>
