<?php
class common_Controller extends bootstrap_Controller {

	public $footerData=false;
	public $headerData=false;
	public $generalSiteData=false;

	function __construct() {
		$modelGeral=new geral_Model();
		$this->footerData=false; //Do the SELECT of the footer data on the database
		$this->headerData=false; //Do the SELECT of the header data on the database
		$general = $modelGeral->dataItem("general",array("id"=>"1"),false);

		$this->generalSiteData = (object) array(
			'general' => $general
		);
	}

	function files() {
		if (isset($_FILES) && isset($_FILES['files'])){
			$files=array();
			$fdata=$_FILES["files"];
			
			if(is_array($fdata['name'])){
				for($i=0;$i<count($fdata['name']);++$i){
					$files[]=array(
						'name'=>$fdata['name'][$i],
						'type'=> $fdata['type'][$i],
						'tmp_name'=>$fdata['tmp_name'][$i],
						'error'=>$fdata['error'][$i], 
						'size'=>$fdata['size'][$i]  
					);
				}
			} else {
				$files[]=$fdata;
			}
			
			foreach ($files as $file) {		
				$tempFile = $file['tmp_name'];
				$targetPath = ENDERECO_FISICO."/uploads/".$file['type'];
				
				$extension = getExtension($file['name']);
				$fileName = uniqid (rand ()) . "." . $extension;
				
				$targetFile =  $targetPath ."/". $fileName;
				$finalName = $file['type']."/".$fileName;

				move_uploaded_file($tempFile,$targetFile);
				
				$data["cover"] = $finalName;
				$data["preview"] = "<img src='".ENDERECO_SITE."uploads/".$finalName."' alt='Prévia'/>";
				$data["status"] = true;
			}		
		} else {
			$data["status"] = false;
			$data["message"] = "Não foi possível fazer o upload do arquivo";
		}

		echo json_encode($data);
	}

	function getModuleId($moduleName) {
		$modelGeral=new geral_Model();
		$moduleData=$modelGeral->dataItem("modules",array("name"=>$moduleName));
		return $moduleData->id;
	}

	function newsletter() {
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Utils.php";
		$storetrooper = new ST_Utils;
		postRequest();

		$data["email"] = $_POST["email"];
		$data["name"] = $_POST["email"];

		if (empty($data["email"])) {
			$jsonReturn["status"] = "error";
			$jsonReturn["message"] = "Informe um e-mail válido";
		} else {
			$jsonReturn = json_decode($storetrooper->add_newsletter($data));
			if($jsonReturn->status == true){
				$jsonReturn->message = "E-mail cadastrado com sucesso";
			}
		}

		echo newJSON($jsonReturn);
		die;
	}

	function urlTransform($string) {
	    $url = preg_replace(
			array(
				"/(á|à|ã|â|ä)/",
				"/(Á|À|Ã|Â|Ä)/",
				"/(é|è|ê|ë)/",
				"/(É|È|Ê|Ë)/",
				"/(í|ì|î|ï)/",
				"/(Í|Ì|Î|Ï)/",
				"/(ó|ò|õ|ô|ö)/",
				"/(Ó|Ò|Õ|Ô|Ö)/",
				"/(ú|ù|û|ü)/",
				"/(Ú|Ù|Û|Ü)/",
				"/(ñ)/",
				"/(Ñ)/"
			),
			explode(" ","a A e E i I o O u U n N"), $string);

		return strtolower($url);
	}
}
?>
