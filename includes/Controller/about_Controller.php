<?php
class about_Controller extends common_Controller {

	function index() {
		$modelSite=new site_Model();

		// busca a pagina para preencher SEO
		$page = $modelSite->getPage(1);
		$data["title"] = "Sobre o projeto - ".$this->nome_site;
		$data['metaDescription'] = $page[0]->description;
		$data['keywords'] = $page[0]->keywords;

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		//Model to catch the interface content - You can edit this line
		// $data["content"]=$modelGeral->listaItens("interface",0,999999999,false,"no");
		loadView(array("layout/_header","pages/about","layout/_footer"),$data);
	}
}
?>
