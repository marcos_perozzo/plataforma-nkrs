<?php
class contact_Controller extends common_Controller {

	function index() {
		$modelSite=new site_Model();
		// busca a pagina para preencher SEO
		//$page = $modelSite->getPage(1);
		$data["title"] = "Contato - ".$this->nome_site;
		//$data['metaDescription'] = $page[0]->description;
		//$data['keywords'] = $page[0]->keywords;
		$data['content'] = "includes/View/pages/user/index.php";

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/contact","layout/_footer"),$data);
	}

	function save(){
		postRequest();

		if(isset($_POST)){
			require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Contact.php";
			$storetrooper = new ST_Contact;

			$data['name'] = $_POST['name'];
			$data['email'] = $_POST['email'];
			$data['phone'] = $_POST['phone'];
			$additional_info = $_POST['additional_info'];

			$response = json_decode($storetrooper->add($data, $additional_info));

			if($response->status == true){
				// Tratar o sucesso
			}

			echo json_encode($response);
		}
		


	}
}
?>
