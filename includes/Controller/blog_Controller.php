<?php
class blog_Controller extends common_Controller {

	function index() {
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Blog.php";
		$storetrooper = new ST_Blog;
		$modelSite=new site_Model();

		// busca a pagina para preencher SEO
		$data["title"] = "Blog - ".$this->nome_site;
		$data['metaDescription'] = "";
		$data['keywords'] = "";
		//Passando o limite de posts como array
		$response = json_decode($storetrooper->get(array('limit' => 10)));

		foreach($response->data as $key => $post){
			$response->data[$key]->image = str_replace('/st/','/adsites/',$post->image);
		}
			
		if($response->status == true){
			$data['posts'] = $response->data;
		} else {
			$data['nodata'] = "Não foi possível carregar os posts";
		}
		
		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/blog/index","layout/_footer"),$data);
	}

	function details() {
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Blog.php";
		$storetrooper = new ST_Blog;

		if(isset($_GET['slug'])){
			$slug = $_GET['slug'];
			$response = json_decode($storetrooper->get(array('slug' => $slug)));
			$post = null;

			if($response->status == true){
				foreach($response->data->gallery as $key => $item){
					$response->data->gallery[$key]->path = str_replace('/st/','/adsites/',$item->path);
				}
				$post = $response->data;
			} else {
				$post = (object)array("title" => "Blog", "description" => null, "keyworks" => null, "message" => "Não foi possível carregar essa postagem");
			}

			$data["title"] = $post->title." - ".$this->nome_site;
			$data['metaDescription'] = $post->description;
			$data['keywords'] = $post->tags;
			$data['post'] = $post;

		} else {
			$data["title"] = "Blog - ".$this->nome_site;
			$data['metaDescription'] = "";
			$data['keywords'] = "";
		}

		$response = json_decode($storetrooper->get(array('offset' => 1, 'limit' => 3)));

		if($response->status == true){
			foreach($response->data as $key => $item){
				$response->data[$key]->image = str_replace('/st/','/adsites/',$item->image);
			}
			$data['posts'] = $response->data;
		}

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/blog/details","layout/_footer"),$data);
	}
}
?>
