<?php
class webcup_Controller extends common_Controller {

	function save() {
		postRequest();

		if(!empty($_POST["id"])){
			$data["id"] = $_POST["id"];
		}
		$data["category"] = $_POST["category"];
		$data["date"] = $_POST["date"];
		$data["link"] = $_POST["link"];

		if (empty($data["category"]) || empty($data["date"]) || empty($data["link"])) {
			$response["status"] = "error";
			$response["message"] = "Verifique os campos obrigatórios!";
		} else {
			if(empty($_POST["id"])){
				dbSave("webcup",$data);
				$response["status"] = "ok";
				$response["message"] = "Webcup cadastrado com sucesso!";
			} else {
				dbUpdate("webcup",$data);
				$response["status"] = "ok";
				$response["message"] = "Webcup editado com sucesso!";
			}
		}

		echo json_encode($response);
	}
}
?>
