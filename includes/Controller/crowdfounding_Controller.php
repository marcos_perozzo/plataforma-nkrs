<?php
class crowdfounding_Controller extends common_Controller {

	function index() {
		$modelSite=new site_Model();

		$data["title"] = "Quero apoiar o projeto - ".$this->nome_site;
		$data['metaDescription'] = "";
		$data['keywords'] = "";

		$data["data"] = $modelSite->getAllCrowdfounding();

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		//Model to catch the interface content - You can edit this line
		// $data["content"]=$modelGeral->listaItens("interface",0,999999999,false,"no");
		loadView(array("layout/_header","pages/crowdfounding/index","layout/_footer"),$data);
	}

	function save() {
		postRequest();

		if(!empty($_POST["id"])){
			$data["id"] = $_POST["id"];
		}
		$data["title"] = $_POST["title"];
		$data["desc"] = $_POST["desc"];
		$data["link"] = $_POST["link"];
		$data["value"] = $_POST["value"];
		$data["match"] = $_POST["match"];
		$data["cover"] = !empty($_POST["cover"]) ? $_POST["cover"] : null; 

		if (empty($data["title"]) || empty($data["desc"]) || empty($data["value"]) || empty($data["link"])) {
			$response["status"] = false;
			$response["message"] = "Verifique os campos obrigatórios!";
		} else {
			if(empty($data["id"])){
				dbSave("crowdfounding",$data);
				$response["status"] = true;
				$response["message"] = "Crowdfounding cadastrado com sucesso!";
			} else {
				dbUpdate("crowdfounding",$data);
				$response["status"] = true;
				$response["message"] = "Crowdfounding editado com sucesso!";
			}
		}

		echo json_encode($response);
	}

	function delete(){
		postRequest();

		if(!empty($_POST["id"])){
			dbDelete("crowdfounding",$_POST["id"]);
			$response['status'] = true;
			$response['message'] = "Seu registro foi excluido com sucesso.";
		} else {
			$response['status'] = false;
			$response['message'] = "Não foi possível concluir a operação.";
		}

		echo json_encode($response);
	}
}
?>
