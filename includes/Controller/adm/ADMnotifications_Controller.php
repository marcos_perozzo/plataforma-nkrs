<?php
class ADMnotifications_Controller extends ADMcommon_Controller {

	protected $moduleName="notifications";
	protected $mountMenu=false;

	function __construct() {
		$this->mountMenu=$this->checkRolesGenerateMenu($this->moduleName);
		parent::__construct();
	}

	function read($urlData) {
		$itemId=(int)$urlData[0];
		$userData=$this->getUserData();
	
		$modelGeral=new geral_Model();
		$dataReturn=$modelGeral->dataItem("notifications",array("id"=>$itemId));
		if (empty($dataReturn)) {
			$alertData["type"]="error";
			$alertData["title"]="OOPS!";
			$alertData["message"]="O item que você tentou abrir não existe ou já foi excluído";
			$this->index($alertData);
			die;
		}
		$readNotifyData["id"]=$itemId;
		$readNotifyData["readed"]=date("Y-m-d H:i:s");
		dbUpdate("notifications",$readNotifyData);

		header("Location:".$dataReturn->url);
	}
}
?>