<?php
class ADMcommon_Controller extends ADMauth_Controller {
	
	function getNotifications() {
		// $userData=$this->getUserData();
		// $modelGeral=new geral_Model();

		// $notifications->unreaded=$modelGeral->listaSQL("SELECT * FROM notifications WHERE readed IS NULL AND type<>'m' AND user_id='".$userData["id"]."' ORDER BY created DESC;");

		// $notifications->readed=$modelGeral->listaSQL("SELECT * FROM notifications WHERE readed IS NOT NULL AND type<>'m' AND user_id='".$userData["id"]."' ORDER BY readed DESC LIMIT 5;");
		$notifications=false;
		return $notifications;
	}

	function getModuleId($moduleName) {
		$modelGeral=new geral_Model();
		$moduleData=$modelGeral->dataItem("modules",array("name"=>$moduleName));
		return $moduleData->id;
	}

	function getUserData() {
		$this->verifyLogged();
		$endereco_site=ENDERECO_SITE;

		openSession();
		$avatar=showImage(false,100,100,false,false,$_SESSION["id"],1,false);
		if ($avatar){
			$_SESSION["avatar"]=$avatar;
		} else {
			$_SESSION["avatar"]=$endereco_site."/assets/adm/images/avatar.png";
		}
		return $_SESSION;
	}

	function verifyRole($roles) {
		openSession();
		$userdata=$_SESSION;
		if (empty($_SESSION["token"])){
			closeSession();
			$this->login("Ocorreu um problema ao verificar sua sessão. Faça login novamente.");
			die();
		}
		if (!in_array($userdata["role"], $roles) and $userdata["role"]!="0") {
			$data["erro"]["titulo"]="Usuário sem permissão (403)";
			$data["erro"]["conteudo"]="Seu usuário não possui permissão para acessar este conteúdo.";
			loadView("__erro",$data);
			die;
		}
	}

	function checkRolesGenerateMenu($actualModule=false,$ignoreModuleRoles=false) {
		$endereco_site=ENDERECO_SITE;
		$userdata=$this->getUserData();
		$modelGeral=new geral_Model();
		$modules=$modelGeral->listaSQL("SELECT * FROM modules WHERE visible='1' ORDER BY menu_order ASC");
		foreach ($modules as $key=>$module) {
			$roles=unserialize($module->roles);
			if (!in_array($userdata["role"], $roles)) {
				unset($modules[$key]);
				if ($module->name==$actualModule and !$ignoreModuleRoles) {
					$data["erro"]["titulo"]="Usuário sem permissão (403)";
					$data["erro"]["conteudo"]="Seu usuário não possui permissão para acessar este conteúdo.";
					loadView("__erro",$data);
					die;
				}
			}
		}
		return $modules;
	}
	
	function getCities() {
		postRequest();
		if (!empty($_POST["stateCompany"])) {
			$uf=$_POST["stateCompany"];
		} elseif (!empty($_POST["stateSubsidiary"])) {
			$uf=$_POST["stateSubsidiary"];
		} elseif (!empty($_POST["uf"])) {
			$uf=$_POST["uf"];
		}
		$modelGeral=new geral_Model();
		$cities=$modelGeral->listaSQL("SELECT * FROM cities WHERE state='".$uf."';");

		$jsonReturn=[];
		$returnData["id"]="";
		$returnData["nome"]="Escolha a opção";
		array_push($jsonReturn,$returnData);
		foreach ($cities as $city) {
			$returnData["id"]=$city->name;
			$returnData["nome"]=$city->name;
			array_push($jsonReturn,$returnData);
		}

		echo newJSON($jsonReturn);
		die;
	}
}
?>