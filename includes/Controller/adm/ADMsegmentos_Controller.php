<?php
class ADMsegmentos_Controller extends ADMcommon_Controller {

	protected $moduleName="segmentos";
	protected $mountMenu=false;

	function __construct() {
		$this->mountMenu=$this->checkRolesGenerateMenu($this->moduleName);
		parent::__construct();
	}

	function index($alertData=false) {
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Segmentos da página Seja Revendedor";
		$data["pageSubHeading"]="Controle os segmentos.";
		$data["tableHeading"]="Lista de segmentos";

		//Common Page Data
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch the results
		$modelGeral=new geral_Model();
		$dataReturn=$modelGeral->listaItens($this->moduleName,0,999999999,false,"no");

		//Adjust the itens to show on the table body
		$data["items"]=array();
		foreach ($dataReturn as $item) {
			$dataArray["id"]=$item->id;
			$dataArray["canEdit"]=true;
			$dataArray["canDelete"]=true;
			$dataArray["columns"][0]=$item->title;
			array_push($data["items"], $dataArray);
		}

		//Columns of the table
		$data["columns"][0]="Nome do Segmento";

		//Set the actions URLs
		$data["addUrl"]=$endereco_site."adm/".$this->moduleName."/add/";
		$data["editUrl"]=$endereco_site."adm/".$this->moduleName."/edit/";
		$data["deleteUrl"]=$endereco_site."adm/".$this->moduleName."/delete/";

		if (!empty($alertData)) {
			$data["alertData"]=$alertData;
		}

		//Load the view
		loadView("adm/lists",$data);
	}

	function add() {
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Adição de Segmentos";
		$data["pageSubHeading"]="Inclua um novo segmento.";
		$data["contentHeading"]="Formulário de cadasto";

		//Common Page Data
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		$data["attachments"][0]["label"]="Enviar imagem de fundo";
		$data["attachments"][0]["type"]="image";
		$data["attachments"][0]["block"]="banners";
		$data["attachments"][0]["iconSize"]="fa-2x";
		$data["attachments"][0]["url"]=$endereco_site."adm/attachments";
		$data["attachments"][0]["maxFiles"]=1;
		$data["attachments"][0]["fileTypesText"]="Permitido envio de JPG e PNG";
		$data["attachments"][0]["fileTypes"]="image/jpg,image/jpeg,image/png";
		$data["attachments"][0]["templateName"]="template-uploaded";
		$data["attachments"][0]["turnAvaliable"]="saveBtn";
		$data["attachments"][0]["preFiles"]="prefiles";
		$data["attachments"][0]["module"]=$this->getModuleId($this->moduleName);
		$data["attachments"][0]["relation"]=false;
		$data["attachments"][0]["list"]=false;
		$data["templates"]=array("_tpl_attachment_list.php");

		$data["token"]=$data["userData"]["token"];

		//Load the view
		loadView("adm/segmento",$data);
	}

	function edit($urlData) {
		$itemId=$urlData[0];
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Edição de Segmento";
		$data["pageSubHeading"]="Edite um segmento.";
		$data["contentHeading"]="Formulário de cadasto";

		//Common Page Data
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch the results
		$modelGeral=new geral_Model();
		$data["itemContent"]=$modelGeral->dataItem($this->moduleName,array("id"=>$itemId));
		if (!$data["itemContent"]) {
			$alertData["type"]="error";
			$alertData["title"]="OOPS!";
			$alertData["message"]="O item que você tentou abrir não está existe ou foi excluído.";
			$this->index($alertData);
			die;
		}

		$data["attachments"][0]["label"]="Enviar imagem de fundo";
		$data["attachments"][0]["type"]="image";
		$data["attachments"][0]["block"]="banners";
		$data["attachments"][0]["iconSize"]="fa-2x";
		$data["attachments"][0]["url"]=$endereco_site."adm/attachments";
		$data["attachments"][0]["maxFiles"]=1;
		$data["attachments"][0]["fileTypesText"]="Permitido envio de JPG e PNG";
		$data["attachments"][0]["fileTypes"]="image/jpg,image/jpeg,image/png";
		$data["attachments"][0]["templateName"]="template-uploaded";
		$data["attachments"][0]["turnAvaliable"]="saveBtn";
		$data["attachments"][0]["preFiles"]="prefiles";
		$data["attachments"][0]["module"]=$this->getModuleId($this->moduleName);
		$data["attachments"][0]["relation"]=$itemId;
		$data["attachments"][0]["list"]=loadMedia($data["attachments"][0]["type"],$data["attachments"][0]["module"],$data["attachments"][0]["relation"],$data["attachments"][0]["block"]);
		$data["templates"]=array("_tpl_attachment_list.php");

		$data["token"]=$data["userData"]["token"];

		//Load the view
		loadView("adm/segmento",$data);
	}

	function save() {
		postRequest();
		$userData=$this->getUserData();
		$endereco_site=$this->endereco_site;
		$endereco_fisico=$this->endereco_fisico;

		$modelGeral=new geral_Model();

		unset($_POST["files"]);
		unset($_POST["mentions"]);
		unset($_POST["module"]);
		$preFiles=$_POST["prefiles"];
		unset($_POST["prefiles"]);
		unset($_POST["relation"]);
		unset($_POST["type"]);
		unset($_POST["block"]);

		if (empty($_POST["id"])) {
			$itemData["title"]=$_POST["title"];
			$itemData["content"]=$_POST["content"];
			$itemData["problem"]=$_POST["problem"];
			$itemData["solution"]=$_POST["solution"];
			$itemData["button"]=$_POST["button"];
			$itemData["link"]=$_POST["link"];

			$itemId=dbSave($this->moduleName,$itemData,true);

			//Log Action
			$logData["user_id"]=$userData["id"];
			$logData["action"]="save";
			$logData["tablename"]=$this->moduleName;
			$logData["item"]=$itemId;
			$logData["date"]=date("Y-m-d H:i:s");
			dbSave("logs",$logData);

			if (!empty($preFiles)) {
				foreach ($preFiles as $fileId) {
					$fileData["relation"]=$itemId;
					$fileData["id"]=$fileId;
					dbUpdate("attachments",$fileData);
				}
			}

			$jsonReturn["status"]="ok";
			$jsonReturn["created_id"]=$itemId;
			$jsonReturn["message"]="Banner adicionado com sucesso";
		} else {
			$itemData["id"]=$_POST["id"];
			$itemData["title"]=$_POST["title"];
			$itemData["content"]=$_POST["content"];
			$itemData["problem"]=$_POST["problem"];
			$itemData["solution"]=$_POST["solution"];
			$itemData["button"]=$_POST["button"];
			$itemData["link"]=$_POST["link"];
			dbUpdate($this->moduleName,$itemData);

			//Log Action
			$logData["user_id"]=$userData["id"];
			$logData["action"]="update";
			$logData["tablename"]=$this->moduleName;
			$logData["item"]=$_POST["id"];
			$logData["date"]=date("Y-m-d H:i:s");
			dbSave("logs",$logData);

			$jsonReturn["status"]="ok";
			$jsonReturn["message"]="Banner editado com sucesso";
		}

		echo newJSON($jsonReturn);
		die;
	}

	function delete($urlData) {
		postRequest();
		$userData=$this->getUserData();
		$endereco_fisico=$this->endereco_fisico;
		$endereco_site=$this->endereco_site;

		$itemId=$urlData[0];

		$attachments=loadMedia("attachment",$this->getModuleId($this->moduleName),$itemId);
		foreach ($attachments as $attachmentData) {
			$filePath=$endereco_fisico."/uploads/".$attachmentData->type."/".$attachmentData->file;
			if (file_exists($filePath)) {
				unlink($filePath);
			}
			dbDelete("attachments",$attachmentData->id);

			//Log Action
			$logData["user_id"]=$userData["id"];
			$logData["action"]="delete";
			$logData["tablename"]="attachments";
			$logData["item"]=$attachmentData->id;
			$logData["date"]=date("Y-m-d H:i:s");
			dbSave("logs",$logData);
		}

		dbDelete($this->moduleName,$itemId);
		$jsonReturn["status"]="ok";
		$jsonReturn["id"]=$itemId;

		//Log Action
		$logData["user_id"]=$userData["id"];
		$logData["action"]="delete";
		$logData["tablename"]=$this->moduleName;
		$logData["item"]=$itemId;
		$logData["date"]=date("Y-m-d H:i:s");
		dbSave("logs",$logData);

		echo newJSON($jsonReturn);
		die;
	}
}
?>
