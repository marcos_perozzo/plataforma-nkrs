<?php
class ADMcontent_Controller extends ADMcommon_Controller {

	protected $moduleName="content";
	protected $mountMenu=false;

	function __construct() {
		$this->mountMenu=$this->checkRolesGenerateMenu($this->moduleName);
		parent::__construct();
	}

	function index($alertData=false) {
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Textos do site";
		$data["pageSubHeading"]="Controle os textos que o usuário irá ler em algumas das seções do site.";
		$data["tableHeading"]="Lista de textos";

		//Common Page Data
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch the results
		$modelGeral=new geral_Model();
		$dataReturn=$modelGeral->listaItens($this->moduleName,0,999999999,false,"no");

		//Adjust the itens to show on the table body
		$data["items"]=array();
		foreach ($dataReturn as $item) {
			$dataArray["id"]=$item->id;
			$dataArray["canEdit"]=true;
			$dataArray["canDelete"]=true;
			$dataArray["columns"][0]=$item->name;
			array_push($data["items"], $dataArray);
		}

		//Columns of the table
		$data["columns"][0]="Nome de identificação";

		//Set the actions URLs
		// if ($data["userData"]["role"]==0) {
		// 	$data["addUrl"]=$endereco_site."adm/".$this->moduleName."/add/";
		// }
		$data["editUrl"]=$endereco_site."adm/".$this->moduleName."/edit/";
		//$data["deleteUrl"]=$endereco_site."adm/".$this->moduleName."/delete/";

		if (!empty($alertData)) {
			$data["alertData"]=$alertData;
		}

		//Load the view
		loadView("adm/lists",$data);
	}

	function add() {
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Adição de texto";
		$data["pageSubHeading"]="Inclua um novo texto. Lembre-se que apenas os desenvolvedores do site poderão adicionar o conteúdo no site público";
		$data["contentHeading"]="Formulário de cadasto";

		//Common Page Data
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		$data["token"]=$data["userData"]["token"];

		//Load the view
		loadView("adm/content",$data);
	}

	function edit($urlData) {
		$itemId=$urlData[0];
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Edição de texto do site";
		$data["pageSubHeading"]="Edite um texto que está em uso no site. Estes textos não podem ser excluídos ou incluídos sem o auxílio técnico do desenvolvedor do site.";
		$data["contentHeading"]="Formulário de cadasto";

		//Common Page Data
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;

		//Catch the results
		$modelGeral=new geral_Model();
		$data["itemContent"]=$modelGeral->dataItem($this->moduleName,array("id"=>$itemId));
		if (!$data["itemContent"]) {
			$alertData["type"]="error";
			$alertData["title"]="OOPS!";
			$alertData["message"]="O item que você tentou abrir não existe ou foi excluído.";
			$this->index($alertData);
			die;
		}

		$data["token"]=$data["userData"]["token"];

		//Load the view
		loadView("adm/content",$data);
	}

	function save() {
		postRequest();
		$userData=$this->getUserData();
		$endereco_site=$this->endereco_site;
		$endereco_fisico=$this->endereco_fisico;

		unset($_POST["files"]);
		unset($_POST["mentions"]);
		unset($_POST["module"]);
		$preFiles=$_POST["prefiles"];
		unset($_POST["prefiles"]);
		unset($_POST["relation"]);
		unset($_POST["type"]);
		unset($_POST["block"]);

		$modelGeral=new geral_Model();

		if (empty($_POST["id"])) {
			$itemData["name"]=$_POST["name"];
			$itemData["title"]=$_POST["title"];
			$itemData["content"]=$_POST["content"];

			$itemId=dbSave($this->moduleName,$itemData,true);

			//Log Action
			$logData["user_id"]=$userData["id"];
			$logData["action"]="save";
			$logData["tablename"]=$this->moduleName;
			$logData["item"]=$itemId;
			$logData["date"]=date("Y-m-d H:i:s");
			dbSave("logs",$logData);

			if (!empty($preFiles)) {
				foreach ($preFiles as $fileId) {
					$fileData["relation"]=$itemId;
					$fileData["id"]=$fileId;
					dbUpdate("attachments",$fileData);
				}
			}

			$jsonReturn["status"]="ok";
			$jsonReturn["created_id"]=$itemId;
			$jsonReturn["message"]="Texto adicionado com sucesso";
		} else {
			$itemData["id"]=$_POST["id"];
			$itemData["title"]=$_POST["title"];
			$itemData["content"]=$_POST["content"];
			dbUpdate($this->moduleName,$itemData);

			//Log Action
			$logData["user_id"]=$userData["id"];
			$logData["action"]="update";
			$logData["tablename"]=$this->moduleName;
			$logData["item"]=$_POST["id"];
			$logData["date"]=date("Y-m-d H:i:s");
			dbSave("logs",$logData);

			$jsonReturn["status"]="ok";
			$jsonReturn["message"]="Texto editado com sucesso";
		}

		echo newJSON($jsonReturn);
		die;
	}

	function delete($urlData) {
		postRequest();
		$userData=$this->getUserData();
		$endereco_fisico=$this->endereco_fisico;
		$endereco_site=$this->endereco_site;

		$itemId=$urlData[0];

		dbDelete($this->moduleName,$itemId);
		$jsonReturn["status"]="ok";
		$jsonReturn["id"]=$itemId;

		//Log Action
		$logData["user_id"]=$userData["id"];
		$logData["action"]="delete";
		$logData["tablename"]=$this->moduleName;
		$logData["item"]=$itemId;
		$logData["date"]=date("Y-m-d H:i:s");
		dbSave("logs",$logData);

		echo newJSON($jsonReturn);
		die;
	}
}
?>
