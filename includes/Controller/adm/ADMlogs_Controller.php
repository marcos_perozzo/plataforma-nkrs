<?php
class ADMlogs_Controller extends ADMcommon_Controller {

	protected $moduleName="logs";
	protected $mountMenu=false;

	function __construct() {
		$this->mountMenu=$this->checkRolesGenerateMenu($this->moduleName);
		parent::__construct();
	}

	function index($alertData=false) {
		$endereco_site=$this->endereco_site;

		//Page Data
		$data["pageHeading"]="Logs do site";
		$data["pageSubHeading"]="Visualize os logs de ações gerados pelos usuários do painel do site";
		$data["tableHeading"]="Lista de logs";

		//Common Page Data
		$data["siteData"]["title"]=$data["pageHeading"]." - ".$this->nome_site;
		$data["siteData"]["nome_site"]=$this->nome_site;
		$data["siteData"]["menu"]=$this->mountMenu;
		$data["userData"]=$this->getUserData();
		$data["notifications"]=$this->getNotifications();
		$data["activeMenu"]=$this->moduleName;
		$data["moduleName"]=$this->moduleName;
		$data["additionalTableClass"]="desc";

		//Catch the results
		$modelGeral=new geral_Model();

		$dataReturn=$modelGeral->listaSQL('SELECT m.title as module, u.name, l.*  FROM `logs` as l 
										   LEFT JOIN modules as m on m.name = l.tablename
		                                   LEFT JOIN users as u on u.id = l.user_id;');

		//Adjust the itens to show on the table body
		$data["items"]=array();
		
		foreach ($dataReturn as $item) {
			$dataArray["canEdit"]=false;
			$dataArray["canDelete"]=false;

			switch ($item->action) {
				case 'update':
				$item->action = 'atualizou';
					break;
				case 'save':
				$item->action= 'inseriu';
					break;
				case 'delete':
				$item->action ='excluiu';
					break;								
			}

			$dataArray["columns"][0]=$item->id;
			$dataArray["columns"][1]=$item->name.', '. $item->action . ' um(a) '. $item->module . ' no dia '. convertDate($item->date,"%d/%m/%Y às %H:%Mh.");

			array_push($data["items"], $dataArray);
		}

		//Columns of the table
		$data["columns"][0]="ID";
		$data["columns"][1]="Ação do log";

		if (!empty($alertData)) {
			$data["alertData"]=$alertData;
		}

		//Load the view
		loadView("adm/lists",$data);
	}
}
?>