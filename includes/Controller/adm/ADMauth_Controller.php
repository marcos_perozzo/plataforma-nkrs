<?php
use Firebase\JWT\JWT;

class ADMauth_Controller extends auth_Controller {
		
	function verifyLogged() {
		openSession();
		if (!empty($_SESSION["token"])) {
		    $secret = base64_decode(SECRET_PHRASE);
			try {
				$tokenData= JWT::decode($_SESSION["token"], $secret, array('HS256'));
			} catch (\Firebase\JWT\ExpiredException $e) {
				closeSession();
				$this->login("Sua sessão expirou. Faça login novamente.");
				die();
			}
			$dateTime = new \DateTime();
			$now=$dateTime->getTimeStamp();
			$expire_time=$tokenData->exp;

			if ($expire_time>$now) {
				$this->userData=$tokenData;

				$future = new \DateTime("now +86400 seconds");

			    $payload = [
			        "iat" => $now,
			        "exp" => $future->getTimeStamp(),
			        "uid" => $tokenData->uid,
			        "role" => $tokenData->role,
			        "name" => $tokenData->name,
			        "email" => $tokenData->email
			    ];

			    $secret = base64_decode(SECRET_PHRASE);
			    $newToken = JWT::encode($payload, $secret, "HS256");

			    updateSession("token",$newToken);
				return true;
			} else {
				closeSession();
				$this->login("Sua sessão expirou. Faça login novamente.");
				die();
			}
		} else {
			closeSession();
			$this->login();
			die();
		}
	}

	function logout() {
		closeSession();
		$endereco_site=$this->endereco_site;
		header('Location:'.$endereco_site);
	}
}
?>