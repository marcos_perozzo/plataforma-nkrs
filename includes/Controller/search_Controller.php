<?php
class search_Controller extends common_Controller {

	function index(){
		$search = $_POST['search'];
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Products.php";
		$st_products = new ST_Products;
		require_once ENDERECO_FISICO."/vendor/storetrooper/ST_Blog.php";
		$st_blog = new ST_Blog;

		$posts = json_decode($st_blog->get(array('search' => $search)));
		$products = json_decode($st_products->get(array('search' => $search)));
		
		foreach($products->data as $key => $item){
			$products->data[$key]->image = str_replace('../','',$item->image);
		}
		foreach($posts->data as $key => $item){
			$posts->data[$key]->image = str_replace('/st/','/adsites/',$item->image);
		}

		$data["title"] = "Loja - ".$this->nome_site;
		$data['metaDescription'] = "";
		$data['keywords'] = "";
		$data['categories'] = $categories->data;
		$data['products'] = $products->data;
		$data['posts'] = $posts->data;

		//General data from the site
		$data["footerData"]=$this->footerData;
		$data["headerData"]=$this->headerData;
		$data["generalSiteData"]=$this->generalSiteData;

		loadView(array("layout/_header","pages/search","layout/_footer"),$data);
	}
}
?>
