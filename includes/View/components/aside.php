<!-- <div class="side-content__item">
    <h3 class="section-title">Próximo evento</h3>
    <div id="timer" data-timer="17/03/2019 15:00:00"></div>
    <div class="side-content__text">
        <p>para</p>
        <h4>1ª etapa do NKRS 2019</h4>
        <a href="#" class="btn btn-lg btn-primary">Confirme sua presença</a>
    </div>
</div> -->
<div class="side-content__item">
    <h3 class="section-title">Parceiros</h3>
    <a class="side-content__sponsor" href="http://adsites.store" target="_blank" title="Ir para AD Sites e Sistemas">
        <img class="img-fluid" src="<?= $endereco_site ?>assets/site/images/logo-ad-sites.png" alt="Logo AD Sites" />
    </a>
    <a class="side-content__sponsor" href="https://jametais.com" target="_blank" title="Ir para JA Metais">
        <img class="img-fluid" src="<?= $endereco_site ?>assets/site/images/logo-ja-metais-h.png" alt="Logo JA Metais" />
    </a>
    <a class="side-content__sponsor" href="https://paddocksportracer.com" target="_blank" title="Ir para Paddock">
        <img class="img-fluid" src="<?= $endereco_site ?>assets/site/images/logo-paddock.png" alt="Logo Paddock Sport Racer" />
    </a>
    <a class="side-content__sponsor" href="#" target="_blank" title="Ir para Pro-Kart">
        <img class="img-fluid" src="<?= $endereco_site ?>assets/site/images/logo-pro-kart.png" alt="Logo Pro-Kart" />
    </a>
</div>