<?php session_start(); ?>
<!doctype html>
<html class="no-js" lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $title;?></title>
        <meta name="description" content="<?php echo $metaDescription; ?>">
        <meta name="description" content="<?php echo $keywords; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <?php include('_favicon.php'); ?>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        <script src="https://kit.fontawesome.com/0fad5e6cde.js"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

        <link href="https://fonts.googleapis.com/css?family=Exo+2|Orbitron" rel="stylesheet">
	    <link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <?php if(isset($cssVendor)){ ?>
            <?php foreach($cssVendor as $css){ ?>
                <link rel="stylesheet" href="<?php echo $endereco_site; ?>assets/site/css/<?php echo $css; ?>">
            <?php } ?>
        <?php } ?>
        <link rel="stylesheet" href="<?php echo $endereco_site; ?>assets/site/css/vendor.min.css">
        <link rel="stylesheet" href="<?php echo $endereco_site; ?>assets/site/css/main.min.css">
    </head>
    <body>

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

		<header class="container mt-4 mb-4">
            <div class="row d-flex align-items-center">
                <div class="col-sm-4">
                    <a href="<?=$endereco_site?>" title="Para a homepage" class="header__logo"></a>
                </div>
                <div class="col-sm-4">
                    <form class="header__search" method="post" action="<?=$endereco_site?>search" data-search>
                        <input type="search" class="form-control" name="search" placeholder="Buscar"/>
                        <button class="header__search__button" type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                <div class="col-sm-4">
                    <?php if(isset($_SESSION['nkrs']['client'])) { ?>
                        <div class="header__auth">
                            <i class="fas fa-user"></i> 
                            <span>Olá, <?=$_SESSION['nkrs']['client']['name']?>!</span>
                            <a href="<?=$endereco_site?>painel" class="btn btn-primary ml-2" title="Acessar sua conta">Minha conta</a>
                            <a href="<?=$endereco_site?>carrinho" class="cart ml-3" title="Carrinho de compras">
                                <i class="fas fa-shopping-cart cart__icon"></i>
                                <span class="badge badge-primary cart__badge" data-badge>3</span>
                            </a>
                        </div>
                    <?php } else { ?>
                        <a href="<?=$endereco_site?>auth/login" class="header__auth" title="Login">
                            <i class="fas fa-user"></i> 
                            <span>Área do Piloto</span>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </header>
        <nav class="navbar navbar-expand-lg">
            <button class="navbar__trigger" type="button" data-navbar>
                <i class="fas fa-bars"></i>
            </button>
            <div class="container collapse navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item <?=strpos($_SERVER['REQUEST_URI'],'home') !== false ? 'active' : ''?>">
                        <a href="<?=$endereco_site?>" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item <?=strpos($_SERVER['REQUEST_URI'],'sobre') !== false ? 'active' : ''?>">
                        <a href="<?=$endereco_site?>sobre" class="nav-link">Sobre o projeto</a>
                    </li>
                    <li class="nav-item <?=strpos($_SERVER['REQUEST_URI'],'apoiar') !== false ? 'active' : ''?>">
                        <a href="<?=$endereco_site?>apoiar" class="nav-link">Quero apoiar o projeto</a>
                    </li>
                    <li class="nav-item <?=strpos($_SERVER['REQUEST_URI'],'inscricoes') !== false ? 'active' : ''?>">
                        <a href="<?=$endereco_site?>inscricoes" class="nav-link">Inscrições</a>
                    </li>
                    <li class="nav-item <?=strpos($_SERVER['REQUEST_URI'],'blog') !== false ? 'active' : ''?>">
                        <a href="<?=$endereco_site?>blog" class="nav-link">Blog</a>
                    </li>
                    <li class="nav-item <?=strpos($_SERVER['REQUEST_URI'],'loja') !== false ? 'active' : ''?>">
                        <a href="<?=$endereco_site?>loja" class="nav-link">Loja</a>
                    </li>
                </ul>
            </div>
        </nav>

		<main id="main">