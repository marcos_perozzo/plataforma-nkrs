<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ERRO</title>
  </head>
  <body>
      <h1><?php echo $erro["titulo"]; ?></h1>
      <div id="content">
        <h2><b>OPS! Ocorreu um problema.</b></h2>
        <p><pre><?php echo $erro["conteudo"]; ?></pre></p>
      </div>
  </body>
</html>