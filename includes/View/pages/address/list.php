<section class="container">
    <h3 class="section-title"><?=$page_name?></h3>
    <a href="<?=$endereco_site?>painel/enderecos-novo" class="btn btn-primary list-action-button">Adicionar</a>
    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action">
            <div class="row">
                <div class="col">Endereços</div>
                <div class="col">Rua/Número</div>
                <div class="col">CEP</div>
                <div class="col-1 text-right">Ações</div>
            </div>
        </a>
        <?php if(!empty($data)){?>
        <?php foreach($data as $item){ ?>
            <a href="<?=$endereco_site?>painel/enderecos-editar?id=<?=$item->id?>" class="list-group-item list-group-item-action" title="Editar">
                <div class="row">
                    <div class="col"><?=$item->label?></div>
                    <div class="col"><?=$item->street?>, <?=$item->number?></div>
                    <div class="col"><?=$item->zipcode?></div>
                    <div class="col-1">
                        <!-- POR BOTÕES -->
                        <button type="button" class="btn btn-sm btn-danger" data-remove="<?=$item->id?>" data-url="<?=$endereco_site?>address/delete" title="Remover endereço">
                            <i class="far fa-trash-alt"></i>
                        </button>
                    </div>
                </div>
            </a>
        <?php } ?>
        <?php } else { ?>
            <a href="#" class="list-group-item list-group-item-action text-center">
                Não há endereços cadastrados!
            </a>
        <?php } ?>
    </div>
</section>