<section id="address" class="container">
    <h3 class="section-title"><?= $page_name ?></h3>
    <p>Os campos com * são de preenchimento obrigatório.</p>
    <form class="form" method="POST" action="<?=$endereco_site ?>address/save" data-validate <?= !empty($data) ? "data-edit" : "" ?>>
        <?php if (!empty($data)) { ?>
            <input type="hidden" name="id" value="<?=$data->id ?>" />
        <?php } ?>
        <input type="hidden" name="id_client" value="<?= isset($data->id_client) ? isset($data->id_client) : $_SESSION['nkrs']['client']['id'] ?>" />
        <div class="form-row">
            <div class="col-md-6">
                <input type="text" class="form-control" name="label" value="<?=$data->label ?>" placeholder="Nome do endereço *" required/>
            </div>
            <div class="col-md-6">
                <input type="text" class="form-control" name="zipcode" value="<?=$data->zipcode ?>" placeholder="CEP *" required/>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6">
                <input type="text" class="form-control" name="street" value="<?=$data->street ?>" placeholder="Rua *" required/>
            </div>
            <div class="col-md-6">
                <input type="text" class="form-control" name="number" value="<?=$data->number ?>" placeholder="Número *" required/>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6">
                <input type="text" class="form-control" name="complement" value="<?=$data->complement ?>" placeholder="Complemento"/>
            </div>
            <div class="col-md-6">
                <input type="text" class="form-control" name="suburb" value="<?=$data->suburb ?>" placeholder="Bairro *" required/>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6">
                <select class="form-control" name="state" data-url="<?=$endereco_site?>address/get_cities" required>
                    <option value="" disabled <?=empty($data) ? 'selected' : ''?>>Estado *</option>
                    <?php foreach($states as $state) { ?>
                        <option value="<?=$state->id?>" data-uf="<?=$state->uf?>" <?=$data->state == $state->name ? 'selected' : '' ?>><?=$state->name?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-6">
                <input class="form-control" list="cities" name="aux-city" placeholder="Cidade *" value="<?=$data->city ?>" required>
                <datalist id="cities">
                </datalist>
                <input type="hidden" name="city" value="<?=$data->id_city ?>"/>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6">
                <div class="form-check">
                    <input id="default" type="checkbox" class="form-check-input" name="default" value="true" <?=$data->default === true ? 'checked' : '' ?>/>
                    <label for="default">Esse é seu endereço padrão de entrega?</label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-check">
                    <input id="billing" type="checkbox" class="form-check-input" name="billing" value="true" <?=$data->billing === true ? 'checked' : '' ?> />
                    <label for="billing">Esse é o seu endereço de cobrança?</label>
                </div>
            </div>
        </div>
        <div class="form__actions">
            <button type="button" class="btn btn-secondary" onclick="window.history.back()">Voltar</button>
            <button type="submit" class="btn btn-primary"><?= !empty($data) ? "Editar" : "Cadastrar" ?></button>
        </div>
    </form>
</section>