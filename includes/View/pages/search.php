<section id="search-results" class="container page">
    <div class="row">
        <div class="col-lg-8">
            <div class="search-results store">
                <h3 class="section-title">Produtos</h3>
                <?php if(count($products)> 0) { ?>
                    <?php foreach($products as $product){ ?>
                        <a href="<?=$endereco_site?>loja/detalhes?produto=<?=$product->slug?>" class="row box-news">
                            <div class="col-sm-4 col-md-3 box-news__thumb box-news__thumb--prod" style="background-image: url(<?=$product->image?>)"></div>
                            <div class="col-sm-8 box-news__info">
                                <span class="box-news__category"><?=$product->category?></span>
                                <p class="box-news__title"><?=$product->title?></p>
                                <h4>R$ <?=number_format($product->price, 2, ',', '.');?></h4>
                            </div>
                        </a>
                    <?php } ?>
                <?php } else { ?>
                    <p>Não há produtos para essa pesquisa.</p>
                <?php } ?>
            </div>
            <div class="search-results">
                <h3 class="section-title">Posts relacionados</h3>
                <?php if(!empty($posts)) { ?>
                    <?php foreach($posts as $post) { ?>
                        <a href="<?=$endereco_site?>blog/detalhes?slug=<?=$post->slug?>" class="row box-news">
                            <div class="col-sm-4 col-md-3 box-news__thumb" style="background-image: url(<?=$post->image?>)"></div>
                            <div class="col-sm-8 box-news__info">
                                <span class="box-news__category"><?=$post->category?></span>
                                <p class="box-news__title"><?=$post->title?></p>
                                <p class="box-news__date"><?=$post->date?></p>
                            </div>
                        </a>
                    <?php } ?>
                <?php } else { ?>
                    <p>Não há posts para essa pesquisa.</p>
                <?php } ?>
            </div>
            <button type="button" class="btn btn-secondary" onclick="window.history.back()">Voltar</button>
        </div>
        <div class="col-lg-4 side-content">
            <?php include ENDERECO_FISICO."/includes/View/components/aside.php"; ?>
        </div>
    </div>
</section>