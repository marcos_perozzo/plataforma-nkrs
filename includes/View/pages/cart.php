<section id="cart" class="container page">
    <h3 class="section-title">Carrinho de compras</h3>
    <ul class="list-group cart-table">
        <li class="list-group-item">
            <div class="row">
                <div class="col-sm-8"><b>Produto(s)</b></div>
                <div class="col"><b>Quantidade</b></div>
                <div class="col text-right"><b>Ação</b></div>
            </div>
        </li>
        <li class="list-group-item">
            <div class="row align-items-center">
                <div class="col-sm-8">
                    <div class="cart-item">
                        <img class="cart-item__img" src="#" alt="Nome">
                        <div class="cart-item__info">
                            <h5>Nome do produto</h5>
                            <p>R$ 999,00</p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <input type="number" class="form-control" name="quantity" min="1" value="1"/>
                </div>
                <div class="col text-right">
                    <button class="btn btn-danger" data-delete="1" data-url="<?=$endereco_site?>cart/delete">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            </div>
        </li>
    </ul>
    <div class="row">
        <div class="col-sm-2">
            <div class="cart-step">
                <input type="tel" class="form-control" name="zipcode" placeholder="CEP"/>
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
            </div>
            <div class="cart-step">
                <input type="text" class="form-control" name="coupom" placeholder="Cupom"/>
                <button type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div class="col text-right">
            <div class="cart-resume">
                <p>Produto(s): R$ <span data-price>999,99</span></p>
                <p>Frete: + R$ <span data-freight>10,99</span></p>
                <p>Cupom: - R$ <span data-cupom>10,99</span></p>
                <h5>Total: <b data-total>R$ 999,00</b></h5>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col">
            <button type="button" class="btn btn-secondary" onclick="window.history.back()">Voltar</button>
        </div>
        <div class="col text-right">
            <button type="button" class="btn btn-primary">Ir para o Checkout</button>
        </div>
    </div>
</section>