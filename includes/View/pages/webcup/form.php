<section id="webcup" class="container">
    <h3 class="section-title"><?=$page_name?></h3>
    <p>Os campos com * são de preenchimento obrigatório.</p>
    <form class="form" method="POST" action="<?=$endereco_site?>webcup/save" data-validate <?=!empty($data[0]) ? "data-edit" : ""?>>
        <?php if(!empty($data[0])){ ?>
        <input type="hidden" name="id" value="<?=$data[0]->id?>"/>
        <?php } ?>
        <input type="text" class="form-control" name="category" value="<?=$data[0]->category?>" placeholder="Categoria *" required />
        <input type="date" class="form-control" name="date" value="<?=$data[0]->date?>" placeholder="Data *" required />
        <input type="text" class="form-control" name="link" value="<?=$data[0]->link?>" placeholder="Link *" required />
        <div class="form__actions">
            <button class="btn btn-secondary" onclick="window.history.back()">Voltar</button>
            <button class="btn btn-primary"><?=!empty($data[0]) ? "Editar" : "Cadastrar"?></button>
        </div>
    </form>
</section>