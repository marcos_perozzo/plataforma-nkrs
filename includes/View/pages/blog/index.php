<section id="blog" class="container">
    <div class="row">
        <div class="col-sm-8">
            <h3 class="section-title">Últimas notícias</h3>
            <?php if(!empty($posts)) { ?>
                <?php foreach($posts as $post) { ?>
                    <a href="<?=$endereco_site?>blog/detalhes?slug=<?=$post->slug?>" class="row box-news">
                        <div class="col-sm-4 col-md-3 box-news__thumb" style="background-image: url(<?=$post->image?>)"></div>
                        <div class="col-sm-8 box-news__info">
                            <span class="box-news__category"><?=$post->category?></span>
                            <p class="box-news__title"><?=$post->title?></p>
                            <p class="box-news__date"><?=$post->date?></p>
                        </div>
                    </a>
                <?php } ?>
            <?php } else { ?>
                <p>Não foi possível carregar os posts.</p>
            <?php } ?>
        </div>
        <div class="col-sm-4 side-content">
            <?php include ENDERECO_FISICO."/includes/View/components/aside.php"; ?>
        </div>
    </div>
</section>