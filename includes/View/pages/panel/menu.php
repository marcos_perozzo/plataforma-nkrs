<aside class="list-group">
    <a href="#" class="list-group-item list-group-item-action" title="Dashboard">Dashboard</a>
    <a href="<?=$endereco_site?>painel/editar-usuario" class="list-group-item list-group-item-action" title="Dados de cadastro">Dados de cadastro</a>
    <a href="<?=$endereco_site?>painel/editar-senha" class="list-group-item list-group-item-action" title="Dados de cadastro">Mudar senha</a>
    <a href="<?=$endereco_site?>painel/enderecos" class="list-group-item list-group-item-action" title="Endereços">Endereços</a>
    <a href="#" class="list-group-item list-group-item-action" title="Pedidos">Pedidos</a>
    <a href="<?=$endereco_site?>painel/resultados" class="list-group-item list-group-item-action" title="Resultados NKRS">Resultados NKRS</a>
    <a href="<?=$endereco_site?>painel/webcup" class="list-group-item list-group-item-info list-group-item-action" title="Resultados NKRS">Cadastro de resultados</a>
    <a href="<?=$endereco_site?>painel/crowdfounding" class="list-group-item list-group-item-info list-group-item-action" title="Resultados NKRS">Gerenciamento crowdfounding</a>
    <a href="<?=$endereco_site?>auth/logout" class="list-group-item list-group-item-danger list-group-item-action" title="Sair da plataforma" data-logout="<?=$endereco_site?>auth/logout">Sair</a>
</aside>