<section id="about">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 page-bg-title page-bg-title--about-1">
                <h1>O Renascimento do Automobilismo</h1>
            </div>
        </div>
    </div>
    <div class="container page-box-text">
        <div class="row">
            <div class="col-md-6">
                <h3 class="section-title">Visão geral</h3>
                <p>Além de não ser um esporte popular desde a morte do Senna, no final da década de 2000, o automobilismo brasileiro sofreu um duro golpe. Por causa da crise de 2008, muitas categorias escola e profissionais foram encerradas por falta de apoio financeiro privado ou do governo e isso congelou o desenvolvimento de pilotos brasileiros em nível internacional.</p>
                <p>Porém, no meio da década seguinte, algumas organizações conseguiram conceber ou remodelar campeonatos como a Sprint Race, Fórmula Inter, Fórmula Vee e a Fórmula Academy Sudamericana que tem função de categorias escola/intermediárias entre os campeonatos nacionais e internacionais de Fórmula ou Turismo.</p>
                <p>Por mais que existam iniciativas que busquem facilitar os degraus entre o kart e o profissionalismo, o kartismo brasileiro ainda é uma enorme barreira que precisa ser superada pelos pilotos.</p>
                <p>Mas, por quê?</p>
            </div>
            <div class="col-md-6">
                <div id="sliderAbout1" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#sliderAbout1" data-slide-to="0" class="active"></li>
                        <li data-target="#sliderAbout1" data-slide-to="1"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active" style="background-image: url('<?=$endereco_site?>assets/site/images/slide4.jpg');">
                            <div class="carousel-caption d-none d-md-block">
                                <div class="carousel-caption__text">
                                    <h3>Sprint Race</h3>
                                    <p>"Um verdadeiro carro de corrida a seu alcance."</p>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item" style="background-image: url('<?=$endereco_site?>assets/site/images/slide5.jpg');">
                            <div class="carousel-caption d-none d-md-block">
                                <div class=" carousel-caption__text">
                                    <h3>Fórmula Inter</h3>
                                    <p>"Quanto custa o seu sonho?"</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#sliderAbout1" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#sliderAbout1" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container page-box-text">
        <h3 class="section-title">Problemas gerais para resolver</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="page-box-text__item-list">
                    <div class="page-box-text__num">
                        <span>1</span>
                    </div>
                    <div class="page-box-text__info">
                        <h5>
                            Alto custo para formação de um piloto.
                            <br />
                            <i>(Aprendizado)</i>
                        </h5>
                        <p>Qualidade em treinamento teórico, prático e mental, assim como o valor alto dos equipamentos e das estruturas de treino.</p>
                    </div>
                </div>
                <div class="page-box-text__item-list">
                    <div class="page-box-text__num">
                        <span>2</span>
                    </div>
                    <div class="page-box-text__info">
                        <h5>
                            Auxílio com o plano de carreira do piloto.
                            <br />
                            <i>(Motivação)</i>
                        </h5>
                        <p>A difícil troca de categoria. Ponto em que muitos pilotos abandonam o automobilismo profissional.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="page-box-text__item-list">
                    <div class="page-box-text__num">
                        <span>3</span>
                    </div>
                    <div class="page-box-text__info">
                        <h5>
                            Tornar o automobilismo mais popular.
                            <br />
                            <i>(Cultura)</i>
                        </h5>
                        <p>Corridas não estão na boca do povo e deixaram de fazer parte da cultura brasileira.</p>
                    </div>
                </div>
                <div class="page-box-text__item-list">
                    <div class="page-box-text__num">
                        <span>4</span>
                    </div>
                    <div class="page-box-text__info">
                        <h5>
                            Educação dos condutores.
                            <br />
                            <i>(Complementação)</i>
                        </h5>
                        <p>É sabido que as auto-escolas não entregam o conhecimento necessário para os condutores brasileiros.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid page-box-text page-box-text--blue page-box-text--bigger">
        <div class="container">
            <h3 class="section-title section-title--h">Objetivos do projeto</h3>
            <p class="text-center">O Novo Kart RS tem por objetivo fornecer um ambiente de desenvolvimento e de competição com um investimento baixo para o piloto, tendo por base os <b><i>Rental Karts</i></b> (karts de aluguel) e mostrar para a população brasileira que o kart, além de gerar diversão, é possível chegar mais longe e criar verdadeiros atletas.</p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 page-bg-title page-bg-title--about-2">
                <h1>O mercado do kartismo amador brasileiro</h1>
            </div>
        </div>
    </div>
    <div class="container page-box-text page-box-text--right">
        <div class="row">
            <div class="col-md-6">
                <img class="img-fluid" src="<?=$endereco_site?>assets/site/images/about1.png" />
            </div>
            <div class="col-md-6">
                <h3 class="section-title">01 - Aluguel de karts</h3>
                <p>O aluguel de kart, geralmente, é considerado uma forma de diversão. São karts mais robustos e não requerem tanta manutenção como os karts de competição. Acabam sendo opção para grupos de competição.</p>
                <h5>Implicações para o cliente</h5>
                <p>O kart de competição é um veículo de manutenção muito cara e tem impactado na diminuição de horas treino dos pilotos. Os karts de aluguel seriam uma boa plataforma de treino e até mesmo de competição.</p>
            </div>
        </div>
    </div>
    <div class="separator">
        <div class="separator__bar"></div>
    </div>
    <div class="container page-box-text">
        <div class="row">
            <div class="col-md-6">
                <h3 class="section-title">02 - E-Sports</h3>
                <p>Com a evolução da tecnologia em simuladores de baixo custo e da conexão de internet, tem sido possível organizar campeonatos sérios de corridas virtuais onde até pilotos de renome competem.</p>
                <h5>Implicações para o cliente</h5>
                <p>Além de fornecer mais um ambiente de treino e simulação de táticas, o objetivo é prospectar novos pilotos na região, convidando aqueles que se destacaram a participar de provas reais.</p>
            </div>
            <div class="col-md-6">
                <img class="img-fluid" src="<?=$endereco_site?>assets/site/images/about3.png" />
            </div>
        </div>
    </div>
    <div class="separator">
        <div class="separator__bar"></div>
    </div>
    <div class="container page-box-text page-box-text--right">
        <div class="row">
            <div class="col-md-6">
                <img class="img-fluid" src="<?=$endereco_site?>assets/site/images/about4.png" />
            </div>
            <div class="col-md-6">
                <h3 class="section-title">03 - Escolas de kart</h3>
                <p>Seguindo a doutrina francesa, as escolas de pilotagem não tem o intuito de formar pilotos, mas tem o objetivo de complementar a formação de motoristas e educar os jovens.</p>
                <h5>Implicações para o cliente</h5>
                <p>Adquirir o conhecimento de técnicas de pilotagem e educação para com os outros condutores. Também é possível despertar a paixão pelo kartismo ao ponto de se tornar um hobbie ou um piloto de alto nível.</p>
            </div>
        </div>
    </div>
    <div class="separator">
        <div class="separator__bar"></div>
    </div>
    <div class="container page-box-text">
        <div class="row">
            <div class="col-md-6">
                <h3 class="section-title">Público alvo</h3>
                <p class="">Em uma primeira fase, seriam jovens com faixa etária de 16 à 34 anos. A predominância é masculina, mas queremos mostrar que as mulheres podem fazer parte desse mundo.</p>
                <div class="list-alt">
                    <div class="list-alt__item">
                        <span class="list-alt__number">01</span>
                        <span class="list-alt__text">O "Cabeça de gasolina"</span>
                    </div>
                    <div class="list-alt__item">
                        <span class="list-alt__number">02</span>
                        <span class="list-alt__text">Em busca de competição</span>
                    </div>
                    <div class="list-alt__item">
                        <span class="list-alt__number">03</span>
                        <span class="list-alt__text">Em busca de aperfeiçoamento</span>
                    </div>
                    <div class="list-alt__item">
                        <span class="list-alt__number">04</span>
                        <span class="list-alt__text">Por hobbie</span>
                    </div>
                    <div class="list-alt__item">
                        <span class="list-alt__number">05</span>
                        <span class="list-alt__text">Desenvolver trabalho em equipe</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <img class="img-fluid" src="<?=$endereco_site?>assets/site/images/about5.png" />
            </div>
        </div>
    </div>
    <div class="container-fluid page-box-text page-box-text--blue page-box-text--bigger">
        <div class="container">
            <h3 class="section-title section-title--h">Solução proposta</h3>
            <p class="text-center">Promover um ambiente de competição baseado nas 3 categorias de <b>Rental Karts</b> disponibilizadas pelo kartódromo de Farroupilha - RS. Os pilotos serão separados de acordo com a experiência e, de acordo com a sua evolução, trocarão de categoria dentro do plano de carreira prospospo.</p>
            <p class="text-center">Os integrantes do grupo terão acesso a um material de aprendizado e terão espaço para treinar no simulador e sessões no próprio kartódromo, onde serão acompanhados pela equipe de instrução.</p>
            <p class="text-center">Aqueles que se destacarem serão convidados a participar da equipe do NKRS que irá disputar eventos externos.</p>
        </div>
    </div>
    <div class="container page-box-text">
        <h3 class="section-title">Processo</h3>
        <div class="proccess-box">
            <div class="proccess-box__col">
                <div class="proccess-box__img proccess-box__img--01">
                    <span>01</span>
                </div>
                <div class="proccess-box__text proccess-box__text--bottom">
                    <h5>Formar categorias de base</h5>
                    <p>Buscar novos pilotos para consolidar as categorias de base e promover aqueles que se destacarem na competição.</p>
                </div>
            </div>
            <div class="proccess-box__col">
                <div class="proccess-box__text proccess-box__text--top">
                    <h5>Nivelamento de habilidades</h5>
                    <p>Os pilotos que se destacarem participarão de treinos para desenvolver suas habilidades o melhor possível para participar de eventos externos pela equipe do NKRS.</p>
                </div>
                <div class="proccess-box__img proccess-box__img--02">
                    <span>02</span>
                </div>
            </div>
            <div class="proccess-box__col">
                <div class="proccess-box__img proccess-box__img--03">
                    <span>03</span>
                </div>
                <div class="proccess-box__text proccess-box__text--bottom">
                    <h5>Eventos externos</h5>
                    <p>A equipe formada irá participar de eventos ligado ao kartismo de aluguel e profissional tanto a nível estadual como nacional.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container page-box-text" style="padding-bottom: 0;">
        <h3 class="section-title section-title--h">Ações</h3>
    </div>
    <div class="container page-box-text">
        <div class="row">
            <div class="col-md-6">
                <h3 class="section-title">Campeonatos de Rental Karts</h3>
                <p>Através da estrutura fornecida pelo kartódromo de Farroupilha, uma pista homologada permanentemente para competição, o campeonato será dividido em 3 categorias (GP, Pro e Elite) com equipamentos que condizem com o nível de habilidade dos pilotos.</p>
            </div>
            <div class="col-md-6">
                <img class="img-fluid" src="<?=$endereco_site?>assets/site/images/about10.jpg" />
            </div>
        </div>
    </div>
    <div class="separator">
        <div class="separator__bar"></div>
    </div>
    <div class="container page-box-text page-box-text--right">
        <div class="row">
            <div class="col-md-6">
                <img class="img-fluid" src="<?=$endereco_site?>assets/site/images/about7.png" />
            </div>
            <div class="col-md-6">
                <h3 class="section-title">Treinamento</h3>
                <p>Sessões de treinamento virtual/real serão feitas para desenvolver as habilidades ou doutrinar os pilotos do programa. Levantar a importância do preparo físico, mental e nutricional.</p>
            </div>
        </div>
    </div>
    <div class="separator">
        <div class="separator__bar"></div>
    </div>
    <div class="container page-box-text">
        <div class="row">
            <div class="col-md-6">
                <h3 class="section-title">Scuderia NKRS</h3>
                <p>Equipe formada pelos melhores pilotos que irá representar o projeto em eventos externos e de grande relevância para o kartismo estadual e nacional.</p>
            </div>
            <div class="col-md-6">
                <img class="img-fluid" src="<?=$endereco_site?>assets/site/images/about8.jpg" />
            </div>
        </div>
    </div>
    <div class="separator">
        <div class="separator__bar"></div>
    </div>
    <div class="container page-box-text page-box-text--right">
        <div class="row">
            <div class="col-md-6">
                <img class="img-fluid" src="<?=$endereco_site?>assets/site/images/about11.jpg" />
            </div>
            <div class="col-md-6">
                <h3 class="section-title">Eventos especiais</h3>
                <p>
                    Esses eventos seriam organizados para projetar as marcas dos patrocinadores e apoiadores, assim como o projeto em si.
                    <br />
                    Seriam eventos desde corridas à jantas e celebrações.
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <h3 class="section-title">Visão</h3>
    </div>
    <!-- Fazer timeline igual ao do criança feliz -->
</section>