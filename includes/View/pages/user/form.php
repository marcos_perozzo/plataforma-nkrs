<section id="user" class="container page" <?=$page_name === "Editar" ? 'style="margin-top: 0;"' : ''?>>
    <h3 class="section-title"><?=$page_name?></h3>
    <p>Os campos com * são de preenchimento obrigatório.</p>
    <form class="form" method="POST" action="<?=$endereco_site?>user/save" data-validate>
        <input type="hidden" name="id" value="<?=$data->id?>" />
        <div class="form-row">
            <div class="col-lg-6">
                <input type="text" class="form-control" name="name" value="<?=$data->name?>" placeholder="Nome *" required />
            </div>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="last_name" value="<?=$data->last_name?>" placeholder="Sobrenome *" required />
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-6">
                <input type="text" class="form-control" name="cpf" data-cpf value="<?=$data->cpf?>" placeholder="CPF *" minlength="11" maxlength="11" required />
            </div>
            <div class="col-lg-6 align-center pb-3">
                <legend class="form-label">Sexo *</legend>
                <div class="form-check">
                    <input id="sex-masc" type="radio" class="form-check-input" name="gender" value="m" <?=$data->gender === "m" ? "checked" : ""?> />
                    <label for="sex-masc">Masculino</label>
                </div>
                <div class="form-check">
                    <input id="sex-fem" type="radio" class="form-check-input" name="gender" value="f" <?=$data->gender === "f" ? "checked" : ""?> />
                    <label for="sex-fem">Feminino</label>
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-6">
                <input type="email" class="form-control" name="email" value="<?=$data->email?>" placeholder="E-mail *" required />
            </div>
            <div class="col-lg-6">
                <input type="text" class="form-control" name="birthdate" data-date value="<?=$data->birthdate?>" placeholder="Data de Nascimento *" required />
            </div>
        </div>
        <div class="form-row">
            <div class="col-lg-6">
                <input type="tel" class="form-control" name="phone" value="<?=$data->phone?>" placeholder="Telefone" />
            </div>
            <div class="col-lg-6">
                <input type="tel" class="form-control" name="phone_mobile" value="<?=$data->phone_mobile?>" placeholder="Celular *" required />
            </div>
        </div>
        <?php if($page_name === "Cadastro"){ ?>
        <div class="form-row">
            <div class="col-lg-6">
                <input type="password" class="form-control" name="password" placeholder="Senha*" value="" />
            </div>
            <div class="col-lg-6">
                <input type="password" class="form-control" name="password_confirm" placeholder="Repita a senha *" value="" required />
            </div>
        </div>
        <div class="form-row">
            <div class="col-12">
                <div class="form-check">
                    <input id="terms" type="checkbox" class="form-check-input" name="terms" />
                    <label for="terms">Aceito os <a href="<?=$endereco_site?>uploads/documents/pdf/nkrs-termos-de-uso.pdf" target="_blank" title="Termos de uso">termos</a> de uso</label>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="form__actions">
            <a href="" class="btn btn-secondary">Voltar</a>
            <button class="btn btn-primary"><?=$page_name === "Editar" ? "Editar" : "Cadastrar"?></button>
        </div>
    </form>
</section>