<section class="container crowdfounding">
    <div class="row">
        <div class="col text-center mb-5">
            <h3 class="section-title section-title--h">Apoie o NKRS!</h3>
            <p>Com a sua contribuição, poderemos atingir nossas metas e em troca, você ganhará vantagens.</p>
        </div>
    </div>
    <?php foreach($data as $key => $item) { ?>
        <div class="row <?= $key%2!=0 ? 'row--inverted' : '' ?>">
            <div class="col-lg-6 crowdfounding__info <?= $key%2!=0 ? 'crowdfounding__info--right' : '' ?>"">
                <h3 class="section-title"><?=$item->title?></h3>
                <h4><b>Meta:</b> R$<?=$item->value?></h4>
                <h4><b>Alcançada:</b> <?=$item->match === "1" ? "Sim" : "Não" ?></h4>
                <p class="mt-2"><?=nl2br($item->desc)?></p>
                <a href="<?=$item->link?>" class="btn btn-primary" target="_blank">Quero apoiar</a>
            </div>
            <div class="col-lg-6">
                <img class="crowdfounding__cover" src="<?=$endereco_site?>uploads/<?=$item->cover?>" alt="Capa <?=$item->title?>"/>
            </div>
        </div>
        <?php if(count($data) != $key+1){ ?>
            <div class="separator">
                <div class="separator__bar"></div>
            </div>
        <?php } ?>
    <?php } ?>
</section>