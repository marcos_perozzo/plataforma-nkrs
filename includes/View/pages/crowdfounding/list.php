<section class="container">
    <h3 class="section-title"><?=$page_name?></h3>
    <a href="<?=$endereco_site?>painel/crowdfounding-novo" class="btn btn-primary list-action-button">Adicionar</a>
    <div class="list-group">
        <a href="#" class="list-group-item list-group-item-action">
            <div class="row">
                <div class="col">Nome da campanha</div>
                <div class="col">Meta</div>
                <div class="col">Alcançado</div>
                <div class="col-1">Ações</div>
            </div>
        </a>
        <?php if(!empty($data)){?>
        <?php foreach($data as $item){ ?>
            <a href="<?=$endereco_site?>painel/crowdfounding-editar?id=<?=$item->id?>" class="list-group-item list-group-item-action" title="Editar">
                <div class="row">
                    <div class="col"><?=$item->title?></div>
                    <div class="col">R$<?=$item->value?></div>
                    <div class="col"><?=$item->match === "1" ? "Sim" : "Não" ?></div>
                    <div class="col-1">
                        <!-- POR BOTÕES -->
                        <button type="button" class="btn btn-sm btn-danger" data-remove="<?=$item->id?>" data-url="<?=$endereco_site?>crowdfounding/delete" title="Remover registro">
                            <i class="far fa-trash-alt"></i>
                        </button>
                    </div>
                </div>
            </a>
        <?php } ?>
        <?php } else { ?>
            <a href="#" class="list-group-item list-group-item-action text-center">
                Não há ações cadastradas!
            </a>
        <?php } ?>
    </div>
</section>