<section id="crowdfounding" class="container">
    <h3 class="section-title"><?= $page_name ?></h3>
    <p>Os campos com * são de preenchimento obrigatório.</p>
    <form class="form" method="POST" action="<?= $endereco_site ?>crowdfounding/save" data-validate <?= !empty($data[0]) ? "data-edit" : "" ?>>
        <?php if (!empty($data[0])) { ?>
            <input type="hidden" name="id" value="<?= $data[0]->id ?>" />
        <?php } ?>
        <input type="text" class="form-control" name="title" value="<?= $data[0]->title ?>" placeholder="Título da ação *" required />
        <textarea class="form-control" name="desc" placeholder="Descrição *" rows="7" required><?= $data[0]->desc ?></textarea>
        <input type="text" class="form-control" name="link" value="<?= $data[0]->link ?>" placeholder="Link *" required />
        <input type="text" class="form-control" name="value" value="<?= $data[0]->value ?>" placeholder="Valor *" required />
        <select name="match" class="form-control" required>
            <option disabled>Atingiu a meta? *</option>
            <option value="0" <?= $data[0]->match === "0" ? "selected" : "" ?>>Não</option>
            <option value="1" <?= $data[0]->match === "1" ? "selected" : "" ?>>Sim</option>
        </select>
        <input type="hidden" name="cover" value="<?=$data[0]->cover?>"/>
        <!-- ARQUIVOS -->
        <!-- POR OS SCRIPTS DO FILEUPLOADER DIRETO NO HEADER OU NA PASTA VENDOR -->
        <div class="form-group">
            <span class="btn btn-success fileinput-button">
                <i class="fas fa-plus"></i>
                <span>Escolha a foto de capa</span>
                <input id="fileupload" type="file" name="files[]" data-url="<?=$endereco_site?>common/files">
                <!-- data-url para o caso de separar o serviço de upload -->
            </span>
        </div>
        <div class="preview-files">
            <?php if(!empty($data[0]->cover)) { ?>
                <img src="<?=$endereco_site."uploads/".$data[0]->cover?>" alt="Prévia" />
            <?php } ?>
        </div>
        <!-- END ARQUIVOS -->
        <div class="form__actions">
            <button class="btn btn-secondary" onclick="window.history.back()">Voltar</button>
            <button class="btn btn-primary"><?= !empty($data[0]) ? "Editar" : "Cadastrar" ?></button>
        </div>
    </form>
</section>