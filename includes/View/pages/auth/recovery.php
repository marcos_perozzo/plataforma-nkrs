<section class="auth">
    <div class="auth__box">
        <h4>Esqueceu sua senha?</h4>
        <p>Sem problemas! Informe seu e-mail de login.</p>
        <form class="form" method="POST" action="<?=$endereco_site?>auth/recovery" data-validate>
            <input type="email" class="form-control" name="email" placeholder="E-mail" required/>
            <div class="form__actions form__actions--center">
                <button class="btn btn-primary">Recuperar</button>
            </div>
        </form>
    </div>
</section>