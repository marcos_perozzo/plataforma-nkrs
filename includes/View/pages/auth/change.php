<section class="auth auth--intern">
    <div class="auth__box">
        <h3 class="section-title"><?=$page_name?></h3>
        <form class="form" method="POST" action="<?=$endereco_site?>auth/recovery" data-validate>
            <input type="password" class="form-control" name="current_password" placeholder="Sua senha atual" required/>
            <input type="password" class="form-control" name="password" placeholder="Nova senha" required/>
            <input type="password" class="form-control" name="password_confirm" placeholder="Repita a senha" required/>
            <div class="form__actions">
                <button type="button" class="btn btn-secondary" onclick="window.history.back()">Voltar</button>
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
</section>