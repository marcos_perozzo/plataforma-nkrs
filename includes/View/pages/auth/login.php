<section class="auth">
    <div class="auth__box">
        <h4>Seja bem-vindo!</h4>
        <form class="form" method="POST" action="<?=$endereco_site?>auth/doLogin" data-validate>
            <input type="email" class="form-control" name="email" placeholder="E-mail" required/>
            <input type="password" class="form-control" name="password" placeholder="Senha" required/>
            <div class="form__actions">
                <a href="<?=$endereco_site?>auth/recuperar-senha" title="Recuperar Senha">Esqueceu a senha?</a>
                <button class="btn btn-primary">Entrar</button>
            </div>
        </form>
        <hr/>
        <div class="auth__box__content">
            <p>Novo por aqui? Deixa a gente te conhecer um pouco mais =D</p>
            <a href="<?=$endereco_site?>usuario/cadastro" class="btn btn-primary">Cadastro</a>
        </div>
    </div>
</section>