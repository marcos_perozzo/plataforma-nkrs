<section class="auth">
    <div class="auth__box">
        <h4>Agora, vamos redefinir sua senha</h4>
        <form class="form" method="POST" action="<?=$endereco_site?>auth/recovery" data-validate>
            <input type="password" class="form-control" name="new-password" placeholder="Nova senha" required/>
            <input type="password" class="form-control" name="test-password" placeholder="Repita a senha" required/>
            <div class="form__actions form__actions--center">
                <button class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
</section>