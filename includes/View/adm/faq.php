<?php include("_header.php");?>
      <!-- main area -->
      <div class="main-content tarefas">
        <div class="page-title">
          <div class="title"><?php echo $pageHeading;?></div>
          <div class="sub-title"><?php echo $pageSubHeading;?></div>
        </div>
        <div class="card bg-white m-b">
          <div class="card-header">
            <?php echo $contentHeading;?>
          </div>
          <div class="card-block">
            <div class="row m-a-0">
              <div class="col-lg-12">
                <form class="form-horizontal" id="main-form" role="form" data-module="<?php echo $moduleName;?>" data-url="<?php echo $endereco_site;?>adm/<?php echo $moduleName;?>/save">
                  <?php if (!empty($itemContent->id)) { ?>
                    <input type="hidden" name="id" value="<?php echo $itemContent->id;?>">
                  <?php } else { ?>
                    <input type="hidden" name="id" value="">
                  <?php } ?>
				  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="product_id">Produto</label>
                    <div class="col-sm-10">
                      <select data-placeholder="Escolha o produto" class="chosen" name="product_id" id="product_id" style="width: 100%;" data-rule-required="true">
                        <option value=""></option>
                          <option value="1"<?php if ($itemContent->product_id=="1") { echo 'selected'; } ?>>Varejo</option>
                          <option value="2"<?php if ($itemContent->product_id=="2") { echo 'selected'; } ?>>Franchising</option>
                          <option value="3"<?php if ($itemContent->product_id=="3") { echo 'selected'; } ?>>Gastronomia</option>
                          <option value="4"<?php if ($itemContent->product_id=="4") { echo 'selected'; } ?>>Beleza</option>
                          <option value="5"<?php if ($itemContent->product_id=="5") { echo 'selected'; } ?>>Logística</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="question">Pergunta</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="question" name="question" data-rule-required="true" maxlength="255" value="<?php echo $itemContent->question;?>" placeholder="Pergunta" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="answer">Resposta</label>
                    <div class="col-sm-10">
                      <textarea class="form-control editor" id="answer" name="answer" rows="5" data-rule-required="true"><?php echo $itemContent->answer;?></textarea>
                    </div>
                  </div>
                  <?php include("_upload.php"); ?>
                  <div class="form-group text-right">
                    <a href="<?php echo $endereco_site;?>adm/<?php echo $moduleName;?>/" id="backBtn" class="btn btn-default btn-sm btn-icon loading-demo mr5" type="button">
                      <i class="icon-action-undo mr5"></i>
                      <span>Voltar</span>
                    </a>
                    <button class="btn btn-success btn-icon loading-demo mr5" id="saveBtn" type="button">
                      <i class="icon-cursor mr5"></i>
                      <span>Salvar</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /main area -->

      <?php
      foreach ($templates as $template) {
        include($template);
      }
      ?>
    <!-- /content panel -->
<?php include("_footer.php");?>
