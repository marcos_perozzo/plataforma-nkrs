        <ul class="nav">
          <li class="<?php if ($activeMenu=="home") { ?>active<?php } ?>">
            <a href="<?php echo $endereco_site;?>adm">
              <i class="fa fa-compass fa-fw icon-left-padding"></i>
              <span>Página inicial</span>
            </a>
          </li>
          <?php foreach ($siteData["menu"] as $menu) { ?>
          <li class="<?php if ($activeMenu==$menu->name) { ?>active<?php } ?>">
            <a href="<?php echo $endereco_site;?>adm/<?php echo $menu->name;?>">
              <i class="fa <?php echo $menu->icon;?> fa-fw icon-left-padding"></i>
              <span><?php echo $menu->title;?></span>
            </a>
          </li>
          <?php } ?>
        </ul>
