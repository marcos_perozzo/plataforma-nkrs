<?php include("_header.php");?>
      <!-- main area -->
      <div class="main-content tarefas">
        <div class="page-title">
          <div class="title"><?php echo $pageHeading;?></div>
          <div class="sub-title"><?php echo $pageSubHeading;?></div>
        </div>
        <div class="card bg-white m-b">
          <div class="card-header">
            <?php echo $contentHeading;?>
          </div>
          <div class="card-block">
            <div class="row m-a-0">
              <div class="col-lg-12">
                <form class="form-horizontal" id="main-form" role="form" data-module="<?php echo $moduleName;?>" data-url="<?php echo $endereco_site;?>adm/<?php echo $moduleName;?>/save">
                  <?php if (!empty($itemContent->id)) { ?>
                    <input type="hidden" name="id" value="<?php echo $itemContent->id;?>">
                  <?php } else { ?>
                    <input type="hidden" name="id" value="">
                  <?php } ?>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="address">Endereço</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="address" name="address" data-rule-required="true" maxlength="255" value="<?php echo $itemContent->address;?>" placeholder="Endereço da empresa" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="neighborhood">Bairro</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="neighborhood" name="neighborhood" data-rule-required="true" maxlength="100" value="<?php echo $itemContent->neighborhood;?>" placeholder="Bairro da empresa" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="state">Estado</label>
                    <div class="col-sm-10">
                      <select data-placeholder="Escolha a opção" class="chosen ajaxUpdate" data-showId="false" data-target="city" data-url="<?php echo $endereco_site;?>adm/common/getCities" name="state" id="state" style="width: 100%;" data-rule-required="true">
                        <option value=""></option>
                        <?php foreach ($states as $stateData) { ?>
                          <option value="<?php echo $stateData->abbr;?>"<?php if ($itemContent->state==$stateData->abbr) { ?> selected<?php } ?>><?php echo $stateData->name;?> ( <?php echo $stateData->abbr;?> )</option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="city">Cidade</label>
                    <div class="col-sm-10">
                      <select data-placeholder="Escolha a opção" class="chosen" name="city" id="city" style="width: 100%;" data-rule-required="true">
                        <option value=""></option>
                        <?php foreach ($cities as $cityData) { ?>
                          <option value="<?php echo $cityData->name;?>"<?php if ($itemContent->city==$cityData->name) { ?> selected<?php } ?>><?php echo $cityData->name;?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="phone">Telefone</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="phone" name="phone" data-rule-required="true" maxlength="255" value="<?php echo $itemContent->phone;?>" placeholder="Telefone da empresa" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="mobile">Celular</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="mobile" name="mobile" data-rule-required="true" maxlength="255" value="<?php echo $itemContent->mobile;?>" placeholder="Celular da empresa" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="facebook">Link do Facebook</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="facebook" name="facebook" data-rule-required="true" maxlength="255" value="<?php echo $itemContent->facebook;?>" placeholder="Facebook da empresa" >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="gmaps">Link do GMaps</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="gmaps" name="gmaps" data-rule-required="true" value="<?php echo $itemContent->gmaps;?>" placeholder="Link do Google Maps" >
                    </div>
                  </div>
                  <div class="form-group text-right">
                    <button class="btn btn-success btn-icon loading-demo mr5" id="saveBtn" type="button">
                      <i class="icon-cursor mr5"></i>
                      <span>Salvar</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /main area -->

      <?php
      foreach ($templates as $template) {
        include($template);
      }
      ?>
    <!-- /content panel -->
<?php include("_footer.php");?>