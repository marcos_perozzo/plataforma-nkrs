<?php include("_header.php");?>
      <!-- main area -->
      <div class="main-content">
        <div class="page-title">
          <div class="title"><?php echo $pageHeading;?></div>
          <div class="sub-title"><?php echo $pageSubHeading;?></div>
        </div>
        <div class="card bg-white">
          <div class="card-header">
            <?php echo $contentHeading;?>
          </div>
          <div class="card-block">
            <div class="row m-a-0">
              <div class="col-lg-12">
                <form class="form-horizontal" role="form" data-module="users" data-url="<?php echo $endereco_site;?>adm/<?php echo $activeMenu;?>/save">
                  <?php if (!empty($itemContent->id)) { ?>
                    <input type="hidden" name="id" value="<?php echo $itemContent->id;?>">
                  <?php } else { ?>
                    <input type="hidden" name="id" value="">
                  <?php } ?>
                  <?php if (!$myInfo) { ?>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="role">Tipo de perfil</label>
                    <div class="col-sm-10">
                      <select data-placeholder="Escolha o tipo" class="chosen" name="role" id="role" style="width: 100%;" data-rule-required="true">
                        <option value=""></option>
                        <?php if ($userData["role"]<="0") { ?>
                          <option value="0"<?php if ($itemContent->role=="0") { ?> selected<?php } ?>>Weecom</option>
                        <?php } ?>
                        <?php if ($userData["role"]<="1") { ?>
                          <option value="1"<?php if ($itemContent->role=="1") { ?> selected<?php } ?>>Administrador</option>
                        <?php } ?>
                        <?php if ($userData["role"]<="2") { ?>
                          <option value="2"<?php if ($itemContent->role=="2") { ?> selected<?php } ?>>Editor</option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <?php } ?>
                  <div class="form-group">
                    <label class="col-sm-2 control-label required" for="name">Nome</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="name" name="name" maxlength="255" value="<?php echo $itemContent->name;?>" placeholder="Pedro Luís Alcantara" data-rule-required="true">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="email">E-mail</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" name="email" id="email" maxlength="255" value="<?php echo $itemContent->email;?>" placeholder="pedro@dominio.com.br" data-rule-email="true">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label<?php if (empty($itemContent->id)) { ?> required<?php } ?>" for="password">Senha</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="password" name="password" value=""<?php if (empty($itemContent->id)) { ?> data-rule-required="true" data-rule-minlength="6"<?php } ?> placeholder="Digite sua senha">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label<?php if (empty($itemContent->id)) { ?> required<?php } ?>">Confirmação de senha</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" data-rule-equalto="#password" data-rule-minlength="6" value="" placeholder="Confirme a senha digitada anteriormente">
                    </div>
                  </div>
                  <?php include("_upload.php"); ?>
                  <div class="form-group text-right">
                    <a href="<?php echo $endereco_site;?>adm/users" id="backBtn" class="btn btn-default btn-sm btn-icon loading-demo mr5" type="button">
                      <i class="icon-action-undo mr5"></i>
                      <span>Voltar</span>
                    </a>
                    <button class="btn btn-success btn-icon loading-demo mr5" id="saveBtn" type="button">
                      <i class="icon-cursor mr5"></i>
                      <span>Salvar</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /main area -->

      <?php
      foreach ($templates as $template) {
        include($template);
      }
      ?>
<?php  include("_footer.php"); ?>