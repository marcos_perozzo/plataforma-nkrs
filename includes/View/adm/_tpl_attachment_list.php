    <!-- Script to the attachment template -->
    <script id="<?php echo $attachment["templateName"];?>" type="text/x-handlebars-template">
      <li class="col-md-3" id="{{type}}-{{id}}" data-id="{{id}}" data-url="<?php echo $attachment["url"];?>">
        <div class="thumbnail file text-center m-b">
          {{#if highlight}}
            <i class="fa fa-star btn-highlight-attach" aria-hidden="true"></i>
          {{else}}
            <i class="fa fa-star-o btn-highlight-attach" aria-hidden="true"></i>
          {{/if}}
          {{{element}}}
        </div>
        <div class="input-group m-b-0">
          <input type="text" class="form-control br0 attach-desc" maxlength="255" value="{{name}}">
          <span class="input-group-btn">
            <button class="btn btn-danger btn-delete-attach" type="button">
              <i class="fa fa-trash-o" aria-hidden="true"></i>
            </button>
            <button class="btn btn-default btn-cancel hidden" type="button">
              <i class="fa fa-times-circle" aria-hidden="true"></i>
            </button>
            <button class="btn btn-success btn-save-attach hidden" type="button">
              <i class="fa fa-floppy-o" aria-hidden="true"></i>
            </button>
          </span>
        </div>
      </li>
    </script>
    <!-- /Script to the attachment template -->