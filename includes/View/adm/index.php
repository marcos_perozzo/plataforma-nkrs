<?php include("_header.php");?>
      <!-- main area -->
      <div class="main-content">
        <div class="page-title">
          <div class="title"><?php echo $siteData["nome_site"];?></div>
          <div class="sub-title">Página inicial</div>
        </div>
        <div class="row dashboard">
          <h2><?php echo $siteData["nome_site"];?></h2>
          <h3>Estatísticas básicas</h3>          
        </div>
        <div class="row dashboard-content">
          <div class="col-md-6 col-lg-4">
            <div class="card bg-info text-white no-border overflow-hidden" style="min-height: 250px; height: 265px;">
              <div class="card-block">
                <div class="row">
                  <div class="col-xs-6">
                    <div class="fa <?php echo $collum1->icon;?> fa-5x"></div>
                    <div class="h5 m-a-0"><?php echo $collum1->title;?></div>
                    <div class="h6 m-a-0">Total</div>
                  </div>
                  <div class="col-xs-6 text-right">
                    <div class="bg-white absolute shadow circle" style="width: 240px;height:240px; top:-100px;right:-80px;"></div>
                    <div class="h1 relative text-color" style="font-size:70px;"><?php echo $collum1->total;?></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4">
            <div class="card bg-info text-white no-border overflow-hidden" style="min-height: 250px; height: 265px;">
              <div class="card-block">
                <div class="row">
                  <div class="col-xs-6">
                    <div class="fa <?php echo $collum2->icon;?> fa-5x"></div>
                    <div class="h5 m-a-0"><?php echo $collum2->title;?></div>
                    <div class="h6 m-a-0">Total</div>
                  </div>
                  <div class="col-xs-6 text-right">
                    <div class="bg-white absolute shadow circle" style="width: 240px;height:240px; top:-100px;right:-80px;"></div>
                    <div class="h1 relative text-color" style="font-size:70px;"><?php echo $collum2->total;?></div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6 col-lg-4">
            <div class="card bg-info text-white no-border overflow-hidden" style="min-height: 250px; height: 265px;">
              <div class="card-block">
                <div class="row">
                  <div class="col-xs-6">
                    <div class="fa <?php echo $collum3->icon;?> fa-5x"></div>
                    <div class="h5 m-a-0"><?php echo $collum3->title;?></div>
                    <div class="h6 m-a-0">Total</div>
                  </div>
                  <div class="col-xs-6 text-right">
                    <div class="bg-white absolute shadow circle" style="width: 240px;height:240px; top:-100px;right:-80px;"></div>
                    <div class="h1 relative text-color" style="font-size:70px;"><?php echo $collum3->total;?></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /main area -->
<?php include("_footer.php");?>