                <?php foreach ($attachments as $attachment) { ?>  
                  <div class="form-group">
                    <label class="col-sm-2 control-label<?php echo (!$attachment["turnAvaliable"]?"":" required");?>"><?php echo $attachment["label"];?></label>
                    <div class="col-md-10 upload-block" id="<?php echo $attachment["block"];?>" data-type="<?php echo $attachment["type"];?>">
                      <div class="btn btn-block text-center fileinput-button m-b dashed">
                        <i class="fa fa-cloud-upload <?php echo $attachment["iconSize"];?>"></i>
                        <div>
                          Selecione o arquivo ou arraste para cá<br>
                          <small>
                          <?php if ($attachment["maxFiles"]>0) {
                            echo "Envie até ".$attachment["maxFiles"]." arquivo(s) | ";
                          }?>
                          <?php echo $attachment["fileTypesText"];?>
                          </small>
                        </div>
                        <div class="fileupload" data-url="<?php echo $attachment["url"];?>" data-maxFiles="<?php echo $attachment["maxFiles"];?>" data-parentBlock="<?php echo $attachment["block"];?>" data-fileTypes="<?php echo $attachment["fileTypes"];?>" data-template="<?php echo $attachment["templateName"];?>" data-turnAvaliable="<?php echo $attachment["turnAvaliable"];?>" data-preFiles="<?php echo $attachment["preFiles"];?>">
                          <input type="hidden" name="module" value="<?php echo $attachment["module"];?>">
                          <input type="hidden" name="relation" value="<?php echo $attachment["relation"];?>">
                          <input type="hidden" name="type" value="<?php echo $attachment["type"];?>">
                          <input type="hidden" name="block" value="<?php echo $attachment["block"];?>">
                          <input type="file" name="files[]"<?php if ($attachment["maxFiles"]>1 or $attachment["maxFiles"]==0) { ?> multiple<?php } ?>>
                        </div>
                      </div>
                      <div class="progress">
                        <div class="progress-bar progress-bar-success hidden" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                      <!-- The container for the uploaded files -->
                      <ul class="files sortable-list ui-sortable">
                        <?php if (!empty($attachment["list"])) {
                          foreach ($attachment["list"] as $attach) {
                        ?>
                          <li class="col-md-3" id="<?php echo $attachment["type"];?>-<?php echo $attach->id;?>" data-id="<?php echo $attach->id;?>" data-url="<?php echo $attachment["url"];?>">
                            <div class="thumbnail file text-center m-b">
                              <?php if ($attach->highlight=="0") { ?>
                                <i class="fa fa-star-o btn-highlight-attach" aria-hidden="true"></i>
                              <?php } else { ?>
                                <i class="fa fa-star btn-highlight-attach" aria-hidden="true"></i>
                              <?php } ?>
                              <?php echo $attach->element;?>
                            </div>
                            <div class="input-group m-b-0">
                              <input type="text" class="form-control br0 attach-desc" maxlength="255" value="<?php echo $attach->name;?>">
                              <span class="input-group-btn">
                                <button class="btn btn-danger btn-delete-attach" type="button">
                                  <i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-default btn-cancel hidden" type="button">
                                  <i class="fa fa-times-circle" aria-hidden="true"></i>
                                </button>
                                <button class="btn btn-success btn-save-attach hidden" type="button">
                                  <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                </button>
                              </span>
                            </div>
                          </li>
                        <?php } } ?>
                      </ul>
                    </div>
                  </div>
                <?php } ?>
