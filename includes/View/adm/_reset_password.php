<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title><?php echo $siteData["title"];?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/webfont.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/climacons-font.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>vendor/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/font-awesome.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/card.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/sli.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/animate.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/app.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/app.skins.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/custom.css">
  <!-- endbuild -->
</head>

<body class="page-loading">
  <!-- page loading spinner -->
  <div class="pageload">
    <div class="pageload-inner">
      <div class="sk-rotating-plane"></div>
    </div>
  </div>
  <!-- /page loading spinner -->
  <div class="app signin v2 usersession">
    <div class="session-wrapper">
      <div class="session-carousel slide" data-ride="carousel" data-interval="3000">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active" style="background-image:url(<?php echo $endereco_site;?>assets/adm/images/carrossel/home1.jpg);background-size:cover;background-repeat: no-repeat;background-position: 50% 50%;">
          </div>
        </div>
      </div>
      <div class="card bg-white no-border">
        <div class="card-block">
          <form role="form" class="form-layout resetPass" method="post" action="<?php echo $endereco_site;?>auth/savePassword/">
            <div class="text-center m-b">
              <h4 class="text-uppercase"><img src="<?php echo $endereco_site;?>assets/adm/images/logo.png" class="img-responsive"><br><br>Falta pouco!</h4>
              <p>Para redefinir sua senha, complete o formulário abaixo:</p>
            </div>
            <div class="form-inputs p-b">
              <label class="text-uppercase">Seu e-mail</label>
              <input type="email" class="form-control input-lg" name="username" placeholder="luis@dominio.com.br" required>
              <input type="hidden" name="hash" value="<?php echo $_GET['hash'];?>">
            </div>
            <div class="form-inputs p-b">
              <label class="text-uppercase">Sua nova senha</label>
              <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Digite uma nova senha" required>
            </div>
            <div class="form-group">
              <label class="text-uppercase">Confirme sua senha</label>
              <input type="password" name="password_confirmation" class="form-control" placeholder="Confirme a sua senha">
            </div>
            <button class="btn btn-primary btn-block btn-lg m-b" id="resetPass" type="submit">Redefinir</button>
            <div class="divider">
              <span>E DEPOIS</span>
            </div>
            <a class="btn btn-block no-bg btn-lg m-b" href="<?php echo $endereco_site;?>auth/login">Acesse sua conta</a>
          </form>
        </div>
      </div>
      <div class="push"></div>
    </div>
  </div>
  <script src="<?php echo $endereco_site;?>assets/adm/js/helpers/modernizr.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/fastclick/lib/fastclick.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="<?php echo $endereco_site;?>assets/adm/js/helpers/smartresize.js"></script>
  <script src="<?php echo $endereco_site;?>assets/adm/js/constants.js"></script>
  <script src="<?php echo $endereco_site;?>assets/adm/js/main.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/jquery-validation/src/localization/messages_pt_BR.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.min.js"></script>
  <script src="<?php echo $endereco_site;?>assets/adm/js/auth.js"></script>
</body>

</html>