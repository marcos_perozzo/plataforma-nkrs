    </div>
    <!-- /content panel -->
    <!-- bottom footer -->
    <footer class="content-footer">
      <nav class="footer-right">
        <ul class="nav">
          <li>
            <a href="javascript:;" class="scroll-up">
              <i class="fa fa-angle-up"></i>
            </a>
          </li>
        </ul>
      </nav>
      <nav class="footer-left hidden-xs">
        <div class="text-center footer-name text-white"><?php echo $siteData["nome_site"];?></div>
      </nav>
    </footer>
    <!-- /bottom footer -->
  </div>
  <script type="text/javascript">
    var baseURL="<?php echo $endereco_site;?>";
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/locale/pt-br.js"></script>
  <script src="<?php echo $endereco_site;?>assets/adm/js/helpers/modernizr.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/jquery/dist/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo $endereco_site;?>assets/adm/js/constants.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/summernote/dist/summernote.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/summernote/dist/lang/summernote-pt-BR.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/sweetalert/dist/sweetalert.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/jquery-mentions/jquery.mentions.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="<?php echo $endereco_site;?>assets/adm/js/helpers/smartresize.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/chosen_v1.4.0/chosen.jquery.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/jquery-validation/src/localization/messages_pt_BR.js"></script>
  <script src="<?php echo $endereco_site;?>assets/adm/js/main.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/blueimp-file-upload/js/jquery.iframe-transport.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/blueimp-file-upload/js/jquery.fileupload.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/chocolat/dist/js/jquery.chocolat.min.js"></script>
  <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/datatables/media/js/dataTables.bootstrap.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.10/handlebars.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/bootstrap-datepicker/dist/js/locales/bootstrap-datepicker.pt-BR.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.min.js"></script>
  <script src="<?php echo $endereco_site;?>vendor/jquery.ui/ui/sortable.js"></script>
  <script src="<?php echo $endereco_site;?>assets/adm/js/custom.js"></script>
  <script src="<?php echo $endereco_site;?>assets/adm/js/ajax_functions.js"></script>
  <?php
    foreach ($additionalScripts as $script) {
      echo "<script src='".$script."'></script>\n";
    }

    if (!empty($pageScript)) {
      echo $pageScript;
    }
  ?>
  <?php if (!empty($alertData)) { ?>
  <script type="text/javascript">
    $(document).ready(function() {
      customAlert={};
      customAlert.title='<?php echo $alertData["title"];?>';
      customAlert.message='<?php echo $alertData["message"];?>';
      customAlert.type='<?php echo $alertData["type"];?>';
      swal(customAlert.title, customAlert.message, customAlert.type);
    });
  </script>
  <?php } ?>
</body>
</html>