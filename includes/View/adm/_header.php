<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title><?php echo $siteData["title"];?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

  <link rel="stylesheet" href="<?php echo $endereco_site;?>vendor/chocolat/dist/css/chocolat.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>vendor/datatables/media/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/webfont.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>vendor/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>vendor/chosen_v1.4.0/chosen.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/card.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/sli.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/animate.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/app.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/app.skins.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>vendor/sweetalert/dist/sweetalert.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>vendor/summernote/dist/summernote.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>vendor/jquery-mentions/jquery.mentions.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>vendor/blueimp-file-upload/css/jquery.fileupload.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css">
  <link rel="stylesheet" href="<?php echo $endereco_site;?>assets/adm/css/custom.css">
</head>

<body class="page-loading">
  <!-- page loading spinner -->
  <div class="pageload">
    <div class="pageload-inner">
      <div class="sk-rotating-plane"></div>
    </div>
  </div>
  <!-- /page loading spinner -->
  <div class="app layout-fixed-header">
    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">
      <div class="brand m-b-md">
        <!-- toggle offscreen menu -->
        <div class="toggle-offscreen">
          <a href="javascript:;" class="visible-xs hamburger-icon" data-toggle="offscreen" data-move="ltr">
            <span></span>
            <span></span>
            <span></span>
          </a>
        </div>
        <!-- /toggle offscreen menu -->
        <!-- logo -->
        <a href="<?php echo $endereco_site;?>adm" class="brand-logo">
          <img src="<?php echo $endereco_site;?>assets/adm/images/logo.png" class="img-responsive">
        </a>
        <a href="#" class="small-menu-visible brand-logo">P</a>
        <!-- /logo -->
      </div>
      <!-- main navigation -->
      <nav role="navigation">
        <?php include("_menu.php");?>
      </nav>
      <!-- /main navigation -->
    </div>
    <!-- /sidebar panel -->
    <!-- content panel -->
    <div class="main-panel">
      <!-- top header -->
      <div class="header navbar">
        <div class="brand visible-xs">
          <!-- toggle offscreen menu -->
          <div class="toggle-offscreen">
            <a href="javascript:;" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <!-- /toggle offscreen menu -->
          <!-- logo -->
          <a class="brand-logo">
            <span><?php echo $siteData["title"];?></span>
          </a>
          <!-- /logo -->
        </div>
        <ul class="nav navbar-nav navbar-right hidden-xs">
          <?php if (!empty($notifications->unreaded) or !empty($notifications->readed)) { ?>
          <li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <i class="fa fa-bell<?php if (count($notifications->unreaded)>0) { ?> text-danger<?php } ?>"></i><?php if (count($notifications->unreaded)>0) { ?><span class="badge bg-danger pull-right"><?php echo count($notifications->unreaded);?></span><?php } ?>
            </a>
            <ul class="dropdown-menu notifications">
              <li class="notifications-header">
                <p class="text-muted small"><?php if (count($notifications->unreaded)==1) { echo "1 nova notificação"; } else { echo count($notifications->unreaded)." novas notificações"; }?> </p>
              </li>
              <li>
                <ul class="notifications-list">
                  <?php foreach ($notifications->unreaded as $notify) {
                    ?>
                    <li class="new">
                      <a href="<?php echo $endereco_site;?>adm/notifications/read/<?php echo $notify->id;?>">
                        <?php
                          if ($notify->type=="n" or $notify->type=="u") {
                            $classNotify="bg-primary";
                          } elseif ($notify->type=="d" or $notify->type=="r") {
                            $classNotify="bg-danger";
                          } elseif ($notify->type=="f") {
                            $classNotify="bg-warning";
                          } elseif ($notify->type=="a") {
                            $classNotify="bg-success";
                          }
                        ?>
                        <div class="notification-icon">
                            <div class="circle-icon <?php echo $classNotify;?> text-white">
                                <i class="fa <?php echo $notify->icon;?>" aria-hidden="true"></i>
                            </div>
                        </div>
                        <span class="notification-message col-md-8"><b><?php echo $notify->title;?></b> <?php echo $notify->content;?></span>
                        <span class="timef col-md-4"><?php echo convertDate($notify->created,"<b>%d/%m</b> %H:%M");?></span>
                      </a>
                    </li>
                    <?php
                  }
                  ?>
                  <?php foreach ($notifications->readed as $notify) { ?>
                    <li>
                      <a href="<?php echo $endereco_site;?>adm/notifications/read/<?php echo $notify->id;?>">
                        <div class="notification-icon">
                          <div class="circle-icon bg-default">
                              <i class="fa <?php echo $notify->icon;?>" aria-hidden="true"></i>
                          </div>
                        </div>
                        <span class="notification-message col-md-8 opct-50"><b><?php echo $notify->title;?></b> <?php echo $notify->content;?></span>
                        <span class="timef col-md-4"><?php echo convertDate($notify->created,"<b>%d/%m</b> %H:%M");?></span>
                      </a>
                    </li>
                    <?php
                  }
                  ?>
                </ul>
              </li>
            </ul>
          </li>
          <?php } ?>
          <li>
            <a href="javascript:;" class="ripple" data-toggle="dropdown">
              <img src="<?php echo $userData["avatar"];?>" class="header-avatar img-circle" alt="<?php echo $userData["name"];?>" title="<?php echo $userData["name"];?>">
              <span><?php echo $userData["name"];?></span>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="<?php echo $endereco_site;?>adm/users/myInfo">Editar meus dados</a>
              </li>
              <li>
                <a href="<?php echo $endereco_site;?>adm/auth/logout">Sair</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /top header -->
