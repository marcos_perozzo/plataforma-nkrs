<?php
/*================================================================================================*/
// Este arquivo é automaticamente chamado pelo ADM.
// Toda vez que quiser adicionar uma nova função comum, inclua ela aqui e lembre-se de comentar
// Funções que auxiliam o ADM. Aqui estarão funções comuns para qualquer Controller ou Model
/*================================================================================================*/

// Esta função prevê erros na função que será chamada e se houver problema já mostra uma mensagem amigável dela
function magicError($errno=false, $errstr=false, $errfile=false, $errline=false) {
	$erro=false;
    switch ($errno) {
		case E_USER_ERROR:
			$erro = "Erro fatal na linha ".$errline." do arquivo ".$errfile."<br />\n";
			$erro .= "Sua versão do PHP é " . PHP_VERSION . " (" . PHP_OS . ")\n";
			break;

		case E_USER_WARNING:
			$erro = "<b>ATENÇÃO</b> [".$errno."] ".$errstr."<br />\n";
			$erro .= "Linha ".$errline." do arquivo ".$errfile."\n";
			break;

		case E_USER_NOTICE:
			$erro = "<b>IMPORTANTE</b> [".$errno."] ".$errstr."<br />\n";
			$erro .= "Linha ".$errline." do arquivo ".$errfile."\n";
			break;

		case E_STRICT:
			$erro = "<b>ERRO</b> [".$errno."] ".$errstr."<br />\n";
			$erro .= "Linha ".$errline." do arquivo ".$errfile."\n";
			break;
    }
	if ($erro) {
		error_log($erro);
		$dadosErro["erro"]=array("titulo"=>"ERRO ".$errno,"conteudo"=>$erro,"linkADM"=>false);
		loadView("__erro",$dadosErro);
	}
    return true;
}

//Define que os erros comuns vão ser tratados pela função abaixo
set_error_handler("magicError");

//Função que pega o erro do try/catch e deixa ele amigável
function catchError($erro=false) {
	error_log($erro);
	$dadosErro["erro"]=array("titulo"=>"ERRO FATAL","conteudo"=>"Ocorreu um erro fatal: ".$erro,"linkADM"=>false);
	loadView("__erro",$dadosErro);
	exit;
}
//Define que os erros do método try/catch serão tratados pela função abaixo
set_exception_handler('catchError');

// Esta função carrega a View e adiciona as variáveis que irão compor o layout
function loadView($view=false,$variaveis = array()) {
	$endereco_site=ENDERECO_SITE;
	$endereco_fisico=ENDERECO_FISICO;

	// Cria variáveis para serem usadas na View baseado nos parametros ƒenviados para a função
	extract($variaveis);

	// Se vários layouts forem enviados, lê o array previamente enviado e varre eles e faz os includes
	if (is_array($view)) {
		foreach ($view as $include) {
			try {
				if (!@include_once("includes/View/".$include.".php")) {
					throw new Exception ("O arquivo includes/View/".$include.".php não existe. A view não foi carregada.");
				}
			}
			catch(Exception $e) {   
				$errno="404";
				$erro=$e->getMessage();
				error_log($erro);
				$dadosErro["erro"]=array("titulo"=>"ERRO ".$errno,"conteudo"=>$erro,"linkADM"=>false);
				loadView("__erro",$dadosErro);
				exit;
			}
		}
	} else {
		try {
			if (!@include_once("includes/View/".$view.".php")) {
				throw new Exception ("O arquivo includes/View/".$view.".php não existe. A view não foi carregada.");
			}
		}
		catch(Exception $e) {
			$errno="404";
			$erro=$e->getMessage();
			error_log($erro);
			$dadosErro["erro"]=array("titulo"=>"ERRO ".$errno,"conteudo"=>$erro,"linkADM"=>false);
			loadView("__erro",$dadosErro);
			exit;
		}
	}
}

// Função que elimina da URL alguns caracteres proibidos que estão definidos no arquivo config.php
function protegeURL($str) {
	$termo=mb_strtolower($str, 'UTF-8');
	
	$caracteresInvalidos = array("'", '"');
	$caracteresConvertidos = array("", "");

	$invalidos=array();
	foreach ($caracteresInvalidos as $cInv) {
		array_push($invalidos, utf8_encode($cInv));
	}
	return str_replace($invalidos, $caracteresConvertidos, $str);
}
// Função que verifica se a página está sendo acessada via POST request
function postRequest(){
	if ($_SERVER['REQUEST_METHOD']!="POST") {
		$errno="500";
		$erro="Você não tem permissão para acessar esta página com este tipo de requisição";
		$dadosErro["erro"]=array("titulo"=>"ERRO ".$errno,"conteudo"=>$erro,"linkADM"=>false);
		loadView("__erro",$dadosErro);
		exit;
	}
}
// Função que elimina da URL alguns caracteres proibidos que estão definidos no arquivo config.php
function limpaPost() {
	foreach ($_POST as $key => $value) {
		$valorLimpo=str_replace(array("'", '"'), array("&lsquo;", '&quot;'), $value);
		$_POST[$key]=$valorLimpo;
	}
}
// Função para salvar dados no banco Mysql
function dbSave($tabela, $arrayValores,$retornaID=false,$debug=false) {
	$arrayColunas = array_keys($arrayValores); // Pega as chaves do array
	// Separa as chaves do array e coloca elas separadas com vírgula para definir as colunas
	$colunas = array();
	foreach ($arrayColunas as $col) {
		array_push($colunas, $col);
	}
	$colunas=implode(",", array_map(function($string){
		return '`'.$string.'`';
	}, $colunas));

	// Separa as chaves do array e coloca elas separadas por dois pontos e vírgula para definir a variável PDO dos valores
	$valores = array();
	foreach ($arrayValores as $val) {
		array_push($valores, $val);
	}
	$valores=implode(",", array_map(function($string){
		return '"'.$string.'"';
	}, $valores));

	// Executa o comando no banco de dados
	global $db;
	$sql='INSERT INTO '.$tabela.' ('.$colunas.') VALUES ('.$valores.')';

	if ($debug) {
		echo "<h2>SQL que será executado</h2>";
		echo "<p>".$sql."</p>";
		die;
	}
	$mysql = $db->prepare($sql);
	$mysql->execute($arrayValores);
	$id_inserido=$db->lastInsertId();

	if ($retornaID) {
	 	return $id_inserido;
	} else {
		return true;
	}
}
// Função para editar dados no banco Mysql
function dbUpdate($tabela, $arrayValores, $debug=false) {
	$arrayColunas = array_keys($arrayValores); // Pega as chaves do array

	// Separa as chaves do array e coloca elas separadas por dois pontos e vírgula para definir a variável PDO dos valores
	$valores=array();
	foreach ($arrayColunas as $valColunas) {
		if ($valColunas!="id") {
			array_push($valores, "`".$valColunas."`=:".$valColunas);
		}
	}
	$valores=implode(", ", $valores);

	// Executa o comando no banco de dados
	global $db;
	$sql='UPDATE '.$tabela.' SET '.$valores.' WHERE id=:id';
	if ($debug) {
		echo "<h2>SQL que será executado</h2>";
		echo "<p>".$sql."</p>";
		die;
	}
	$mysql = $db->prepare($sql);
	$mysql->execute($arrayValores);

}
// Função para excluir dados no banco Mysql
function dbDelete($tabela, $id, $logica=false, $where=false) {
	// Executa o comando no banco de dados
	global $db;
	if ($logica) {
		if (!$id and $where) {
			$sql='UPDATE '.$tabela.' SET status="9" WHERE '.$where.';';
			$mysql = $db->prepare($sql);
			$mysql->execute();
		} else {
			$sql='UPDATE '.$tabela.' SET status="9" WHERE id=:id';
			$mysql = $db->prepare($sql);
			$mysql->execute(array("id"=>$id));
		}
	} else {
		if (!$id and $where) {
			$sql='DELETE FROM '.$tabela.' WHERE '.$where.';';
			$mysql = $db->prepare($sql);
			$mysql->execute();
		} else {
			$sql='DELETE FROM '.$tabela.' WHERE id=:id;';
			$mysql = $db->prepare($sql);
			$mysql->execute(array("id"=>$id));
		}
	}
}
// Função para verificar valores duplicados no Banco de Dados
function dbDuplicated($tabela, $arrayValores, $retorno, $msg="Item duplicado. A ação não foi executada.") {
	$arrayColunas = array_keys($arrayValores); // Pega as chaves do array

	// Separa as chaves do array e coloca elas separadas por dois pontos e vírgula para definir a variável PDO dos valores
	$valores=array();
	foreach ($arrayColunas as $valColunas) {
		if ($valColunas!="id") {
			array_push($valores, $valColunas."=:".$valColunas);
		} else {
			array_push($valores, $valColunas."<>:".$valColunas);
		}
	}
	$valores=implode(" AND ", $valores);

	// Executa o comando no banco de dados
	global $db;
	$mysql = $db->prepare('SELECT * FROM '.$tabela.' WHERE '.$valores.';');
	$mysql->execute($arrayValores);
	$data = $mysql->fetchAll();
	if (count($data)>=1) {
		if ($retorno=="json") {
			$json['status'] = 0;
			$json['mensagem'] = $msg;
			echo newJSON($json);
			exit;
		} elseif ($retorno=="html") {
			echo $msg;
			exit;
		} elseif ($retorno=="bool") {
			return true;
		} elseif ($retorno=="data") {
			return $data[0];
		} else {
			$dadosErro["erro"]=array("titulo"=>"ITEM DUPLICADO","conteudo"=>$msg,"linkADM"=>true);
			loadView("__erro",$dadosErro);
			exit;
		}
	} else {
		return false;
	}
}
// Função que cria uma sessão com o padrão definido no config.php, ou o nome definido na função, além das variaveis de $_SESSION
function newSession ($sessao=false, $arrayVariaveis, $adm=true) {
	if (!$sessao) {
		global $nome_sessao;
		$sessao=$nome_sessao;
	}
	if ($adm) {
		$sessao=$sessao."ADM";	
	}
	session_name($sessao);
	session_start();
	//Varr as variaveis definindo as sessions
	foreach ($arrayVariaveis as $key => $value) {
		$_SESSION[$key]=$value;
	}
	session_write_close();
}
// Função que abre uma sessão definida, ou a padrão definida no config.php se nenhuma for declarada
function openSession ($sessao=false,$adm=true) {
	if (!$sessao) {
		global $nome_sessao;
		$sessao=$nome_sessao;
	}
	if ($adm) {
		$sessao=$sessao."ADM";	
	}
	session_name($sessao);
	session_start();
	session_write_close();
}
// Função que re-declara o valor de uma SESSION já existente
function updateSession ($variavel,$novoValor,$sessao=false,$adm=true) {
	if (!$sessao) {
		global $nome_sessao;
		$sessao=$nome_sessao;
	}
	if ($adm) {
		$sessao=$sessao."ADM";	
	}
	session_name($sessao);
	session_start();
	$_SESSION[$variavel]=$novoValor;
	session_write_close();
}
// Função que destrói a sessão definida, ou a padrão definida no config.php se nenhuma for declarada
function closeSession ($sessao=false,$adm=true) {
	if (!$sessao) {
		global $nome_sessao;
		$sessao=$nome_sessao;
	}
	if ($adm) {
		$sessao=$sessao."ADM";	
	}
	session_name($sessao);
	session_start();
	session_destroy();
	unset($_SESSION);
	session_id(uniqid (rand()));
	session_start();
}
// Função que cria um JSON baseado no array enviado
function newJSON ($valores) {
	header('Content-Type: application/json');
	return json_encode($valores);
}
// Função que lê um JSON e cria um array associativo
function readJSON ($json,$converteURL=false) {
	if ($converteURL) {
		 $json=urldecode($json);
	}
	if (get_magic_quotes_gpc()) {
		$json=stripslashes($json);
		return json_decode($json, true);
	} else {
		return json_decode($json, true);
	}
}
// Função que cria um Cookie
function newCookie ($nomeCookie, $valor, $duracao=false, $diretorio=false, $adm=true) {
	if (!$duracao) {
		$duracao=time()+31556926;
	} else {
		$duracao=time()+$duracao;
	}
	if (!$diretorio) {
		global $diretorio_cookie;
		$diretorio=$diretorio_cookie;
	}
	if ($adm) {
		$diretorio=$diretorio."/adm";
	}
	global $servidor_cookie;
	setcookie($nomeCookie, $valor, $duracao, $diretorio, $servidor_cookie);
}
// Função que lê um Cookie e retorna o valor dele
function readCookie ($nomeCookie, $adm=true) {
	return $_COOKIE[$nomeCookie];
}
// Função que destroy um Cookie
function delCookie ($nomeCookie, $diretorio=false,$adm=true) {
	if (!$diretorio) {
		global $diretorio_cookie;
		$diretorio=$diretorio_cookie;
	}
	if ($adm) {
		$diretorio=$diretorio."/adm";
	}
	global $servidor_cookie;
	setcookie ($nomeCookie, "", time() - 3600, $diretorio, $servidor_cookie);
	unset ($_COOKIE[$nomeCookie]);
}
// Função que gera uma nova senha 
function geraSenha() {
	$vogais = 'aeiou';
	// A variável $consoante recebendo valor
	$consoante = 'bcdfghjklmnpqrstvwxyzbcdfghjklmnpqrstvwxyz';
	// A variável $numeros recebendo valor
	$numeros = '123456789';
	// A variável $simbolos recebendo valor
	$simbolos = '#.,_$@';
	// A variável $resultado vazia no momento
	$resultado = '';

	// strlen conta o nº de caracteres da variável $vogais
	$a = strlen($vogais)-1;
	// strlen conta o nº de caracteres da variável $consoante
	$b = strlen($consoante)-1;
	// strlen conta o nº de caracteres da variável $numeros
	$c = strlen($numeros)-1;
	// strlen conta o nº de caracteres da variável $simbolos
	$d = strlen($simbolos)-1;

	for($x=0;$x<=1;$x++) { // A função rand() tem objetivo de gerar um valor aleatório
		$aux1 = rand(0,$a);
		$aux2 = rand(0,$b);
		$aux3 = rand(0,$c);
		$aux4 = rand(0,$d);
		$aux5 = rand(0,$a);
		$aux6 = rand(0,$b);
		$aux7 = rand(0,$c);

		// A função substr() tem objetivo de retornar parte da string
		// Caso queira números com mais digitos mude de 1 para 2 e teste
		$str1 = substr($consoante,$aux1,1);
		$str2 = substr($vogais,$aux2,1);
		$str3 = substr($numeros,$aux3,1);
		$str4 = substr($simbolos,$aux4,1);
		$str5 = substr($consoante,$aux5,1);
		$str6 = substr($vogais,$aux6,1);
		$str7 = substr($numeros,$aux7,1);

		$resultado .= $str1.$str2.$str3.$str4.$str5.$str6.$str7;

		$resultado = trim($resultado);
	}
	return $resultado;
}
// Send the e-mails from the site
function sendEmail($to=false,$from=false,$data=array(),$introText=false,$subject=false,$replyTo=false,$debug=false) {
	//Get the config variables
	global $email_cfg_security;
	global $email_cfg_host;
	global $email_cfg_port;
	global $email_cfg_user;
	global $email_cfg_pass;
	global $email_cfg_debug;
	global $email_cfg_from;

	//Set the default sender with it is not set
	if (!$from) {
		$from=$email_cfg_from;
	}

	//Set the address to reply to if it is not set
	if (!$replyTo) {
		$replyTo=$from;
	}

	//Set the subject if it is not set
	if (!$subject) {
		$subject="Contato enviado através do site";
	}

	$bodyMsg="";

	foreach ($data as $line) {
		$bodyMsg.="<tr><td width=\"120\" style=\"text-align:right; font-size:11px; color:#666; padding:0 8px 0 0; font-weight:bold;\">".$line["label"]."</td><td style=\"font-size:11px; color:#666;\">".$line["value"]."</td></tr>\n";
	}

	//Set the header of the message
	$message='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no" />
<title>Comunicado Importante</title>
</head>
<body style="margin:0; padding:0;">
<table width="100%" border="0" cellspacing="30" cellpadding="0" bgcolor="white" style="-webkit-text-size-adjust:none; badmground-color:#FFFFFF; font-size:13px; line-height:16px; color:#666; text-align:left; font-family:Arial, Helvetica, sans-serif;">
    <tr>
        <td align="center">
            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="35"></td>
                </tr>
                <tr>
                    <td style="text-align:center;"><img src="'.ENDERECO_SITE.'assets/email/images/logo.png" alt="" border="0" style="vertical-align:bottom;" /></td>
                </tr>
                <tr>
                    <td height="5"></td>
                </tr>
                <tr>
                    <td><img src="'.ENDERECO_SITE.'assets/email/images/shadow.jpg" alt="" width="600" height="40" border="0" style="display:block;" /></td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:45px 45px 25px 45px; text-align:left;">';

	//Set the intro text of the message
	if (!empty($introText)) {
		$message.="<tr><td style=\"font-size:14px; color:#666; font-weight:bold;\">".$introText."</td></tr>\n";
	}

	//Set the content of the message
	$message.="<tr><td style=\"padding:20px 0 0 0;\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n".$bodyMsg."</table></td></tr>";

	//Set the footer of the message
	$message.='		</table>
                    </td>
                </tr>
                <tr>
                    <td><img src="'.ENDERECO_SITE.'assets/email/images/shadow.jpg" alt="" width="600" height="40" border="0" style="display:block;" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>';
	if ($debug===true) {
		dbg($to,false);
		dbg($subject,false);
		dbg($message);
	}

	//Include the SMTP class
	require_once ENDERECO_FISICO."/vendor/phpmailer/phpmailer/class.smtp.php";
	require_once ENDERECO_FISICO."/vendor/phpmailer/phpmailer/class.phpmailer.php";

	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail->SMTPDebug = $email_cfg_debug;
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
	//Tell PHPMailer to use SMTP
        $mail->isSMTP();
	if ($email_cfg_securtiy!="no-auth") {
		//Set the encryption system to use - ssl (deprecated) or tls
        	$mail->SMTPSecure = $email_cfg_security;
		//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
		//Username to use for SMTP authentication
		$mail->Username = $email_cfg_user;
		//Password to use for SMTP authentication
		$mail->Password = $email_cfg_pass;
	} else {
		//Set the encryption system to use - ssl (deprecated) or tls
                $mail->SMTPSecure = "tls";
                //Whether to use SMTP authentication
                $mail->SMTPAuth = false;
	}
        //Set the hostname of the mail server
        $mail->Host = $email_cfg_host;
        //Set the SMTP port number - likely to be 25, 465 or 587
        $mail->Port = $email_cfg_port;
	//Set who the message is to be sent from
	$mail->setFrom($from, NOME_SITE);
	//Set an alternative reply-to address
	$mail->addReplyTo($replyTo);
	//Set who the message is to be sent to
	if (is_array($to)) {
		$t=1;
		foreach ($to as $toValue) {
			if ($t==1) {
				$mail->addAddress($toValue);
			} else {
				$mail->AddCC($toValue);
			}
		}
	} else {
		$mail->addAddress($to);
	}
	//Set the subject line
	$mail->Subject = $subject;
	//Set the charset
	$mail->CharSet = 'utf-8';
	//Embbed the HTML body
	$mail->msgHTML($message);

	//send the message, check for errors
	if (!$mail->send()) {
		error_log($mail->ErrorInfo);
	    return false;
	} else {
	    return true;
	}
}
// Função para converter um valor em moeda
function convertMoney ($valor, $formatado=true) {
	if (empty($valor)) {
		return false;
		exit;
	}
	if (!$formatado) {
		$valor=str_replace(array("R$"," ",".",","), array("","","","."), trim($valor));
		return $valor;
	} else {
		setlocale(LC_MONETARY, 'pt_BR');
		return money_format('%!.2n', $valor);
	}
}
// Função para converter datas do formato do banco (americano) para o formato indicado
function convertDate ($data, $formato) {
	if (empty($data)) {
		$data=date("Y-m-d");
	}
	$data = str_replace("/", "-", $data);
	$formato=utf8_decode($formato);
	return utf8_encode(strftime($formato,strtotime($data)));
}
// Função para calcular diferença entre datas (A - Ano, M - Mês, D - Dia, H - Hora, MI - Minuto, Em branco - Segundos)
function diffDate($dataInicial, $dataFinal, $tipo='', $separadorData='-') {
	$d1 = explode($separadorData, $dataInicial);
	$d2 = explode($separadorData, $dataFinal);
	switch ($tipo) {
		case 'A':
		$X = 31536000;
		break;
		case 'M':
		$X = 2592000;
		break;
		case 'D':
		$X = 86400;
		break;
		case 'H':
		$X = 3600;
		break;
		case 'MI':
		$X = 60;
		break;
		default:
		$X = 1;
	}
	return floor(((mktime(0, 0, 0, $d2[1], $d2[2], $d2[0])-mktime(0, 0, 0, $d1[1], $d1[2], $d1[0]))/$X));
}
// Função que adiciona dias em uma data. O formato da data deve ser date("Ymd");  O formato de saída é a data em padrão americano, separado por hifens
function addDays($date,$days) {
	return date('Y-m-d', strtotime($date. ' + '.$days.' days'));
}
// Função que subtrai dias em uma data. O formato da data deve ser date("Ymd"); O formato de saída é a data em padrão americano, separado por hifens
function subDays($date,$days) {
     $thisyear = substr ( $date, 0, 4 );
     $thismonth = substr ( $date, 4, 2 );
     $thisday =  substr ( $date, 6, 2 );
     $nextdate = mktime ( 0, 0, 0, $thismonth, $thisday - $days, $thisyear );
     return strftime("%Y-%m-%d", $nextdate);
}
//Função que retorna o tamanho do arquivo
function tamanhoArquivo($arquivo) {
	global $endereco_fisico;
	$arquivo=$endereco_fisico.$arquivo;
	if (file_exists($arquivo)) {
		$tamanhoarquivo = filesize($arquivo);
		$bytes = array('KB', 'MB', 'GB', 'TB');
	
		if($tamanhoarquivo <= 999) {
			$tamanhoarquivo = 1;
		}
		
		for($i=-1; $tamanhoarquivo > 999; $i++) {
			$tamanhoarquivo = $tamanhoarquivo / 1024;
		}
		
		return round($tamanhoarquivo,2).$bytes[$i];
	} else {
		return "0KB";
	}
}
//Função que retorna o Thumb do Youtube
function youtubeThumb($url,$tamanho="p") {
	if ($tamanho=="p") {
		$tamanhoThumb="default";
	} elseif ($tamanho=="m") {
		$tamanhoThumb="hqdefault";
	} elseif ($tamanho=="g") {
		$tamanhoThumb="maxresdefault";
	}
	if(strlen($url)) {
		$pattern='#(\.be/|/embed/|/v/|/watch\?v=)([A-Za-z0-9_-]{5,11})#';
		preg_match($pattern, $url, $matches);
		$thumb="http://img.youtube.com/vi/".$matches[2]."/".$tamanhoThumb.".jpg";
		return $thumb;
	}
}
//Função que retorna o código do vídeo do Youtube
function youtubeCode($url) {
	if(strlen($url)) {
		$pattern='#(\.be/|/embed/|/v/|/watch\?v=)([A-Za-z0-9_-]{5,11})#';
		preg_match($pattern, $url, $matches);
		return $matches[2];
	} else {
		return false;
	}
}
//Função que limita o número de caracteres de uma string
function limitaCaracteres($texto,$maxchar,$end='') {
	$text=strip_tags($texto);
	if (strlen($text)>$maxchar) {
		$words=explode(" ",$text);
		$output = '';
		$i=0;
		while(1){
			$length = (strlen($output)+strlen($words[$i]));
			if($length>$maxchar){
				break;
			}else{
				$output = $output." ".$words[$i];
				++$i;
			};
		};
		return $output.$end;
	} else {
		return strip_tags($texto);
	}
}
//Função que formata uma string para um formato de URL
function formaURL($string){
   $slug=preg_replace( array( '/([`^~\'"])/', '/([-]{2,}|[-+]+|[\s]+)/', '/(,-)/' ), array( null, '-', ', ' ), iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
   return substr(strtolower($slug),0,80);
}
//Função que retorna os dados de um item Multimedia (Imagens, vídeos anexos)
function loadMedia($type,$module,$relation,$block=false,$showFirst=false) {
	if (!empty($block) and $block!=false) {
		$blockSQL=' AND block="'.$block.'"';
	} 
	if ($showFirst) {	
		//Retorna apenas o primeiro item procurado, ou false se não encontrar nada
		global $db;
		$sql='SELECT * FROM attachments WHERE type="'.$type.'" AND module="'.$module.'" AND relation="'.$relation.'" '.$blockSQL.' ORDER BY item_order ASC, created DESC LIMIT 1;';
		$mysql = $db->prepare($sql);
		$mysql->execute();
		$results = $mysql->fetchAll(PDO::FETCH_OBJ);
		if (count($results)>0) {
			$extension=getExtension($results[0]->file);
			if ($type=="image" or $extension=="jpg" or $extension=="jpeg" or $extension=="gif" or $extension=="png" or $extension=="bmp") {
				$imagePath=showImage($results[0]->id,300,280,false,"FFFFFF");
				$results[0]->element="<img src='".$imagePath."' class='img-responsive'>";
				$results[0]->attachType="image";
			} else {
				$results[0]->element="<i class='fa fa-file fa-4x'></i>";
				$results[0]->attachType="file";
			}
			return $results[0];
		} else {
			return false;	
		}
	} else {
		//Retorna todos os itens procurados, ou false se não encontrar nada
		global $db;
		$mysql = $db->prepare('SELECT * FROM attachments WHERE type="'.$type.'" AND module="'.$module.'" AND relation="'.$relation.'" '.$blockSQL.' ORDER BY item_order ASC, created DESC;');
		$mysql->execute();
		$results = $mysql->fetchAll(PDO::FETCH_OBJ);
		if (count($results)>0) {
			$a=0;
			foreach ($results as $attach) {
				$extension=getExtension($attach->file);
				if ($type=="image" or $extension=="jpg" or $extension=="jpeg" or $extension=="gif" or $extension=="png" or $extension=="bmp") {
					$imagePath=showImage($results[$a]->id,300,280,false,"FFFFFF");
					$results[$a]->element="<img src='".$imagePath."' class='img-responsive'>";
					$results[$a]->attachType="image";
				} else {
					$results[$a]->element="<i class='fa fa-file fa-4x'></i>";
					$results[$a]->attachType="file";
				}
				$a++;
			}
			return $results;
		} else {
			return false;	
		}
	}
}
//Função que ordena um array com a ordem dentro de outro array
function ordenaPor($array,$campo,$ordem="ASC") {
	$cod = "return strnatcmp(\$a['$campo'], \$b['$campo']);";
	if ($ordem=="ASC") {
		usort($array, create_function('$a,$b', $cod));
	} elseif($ordem=="DESC") {
		usort($array, create_function('$b,$a', $cod));
	}
	return $array;
}
//Função que imprime o caminho da imagem com o CROP dela, se não houver crop imprime ele sem os comandos.
function showImage($idImagem,$larguraPadrao="640",$alturaPadrao="480",$qualidadePadrao=false,$preencher=false,$idConteudo=false,$idModulo=false,$imgRetorno=true,$block=false) {
	$endereco_site=ENDERECO_SITE;
	
	if (!$idImagem and $idConteudo and $idModulo) {
		$dadosImagem=loadMedia("image",$idModulo,$idConteudo,$block,true);
		$idImagem=$dadosImagem->id;
	}
	
	if ($qualidadePadrao) {
		$qualidadeJPEG=$qualidadePadrao;
	} else {
		$qualidadeJPEG=85;
	}

	if (!empty($preencher) and $preencher!="transparent") {
		$preencherURL="&far=1&bg=".$preencher;
	} elseif ($preencher=="transparent") {
		$preencherURL="&far=1&f=png";
	} else {
		$preencherURL="&zc=1";
	}

	//Retorna todos os itens procurados, ou false se não encontrar nada
	global $db;
	$mysqlImagem = $db->prepare('SELECT * FROM attachments WHERE id="'.$idImagem.'";');
	$mysqlImagem->execute();
	$resultados = $mysqlImagem->fetchAll();
	if (empty($resultados)) {
		if ($imgRetorno) {
			return $endereco_site."vendor/phpthumb/phpThumb.php?src=../../images/site/sem_imagem.jpg&w=".$larguraPadrao."&h=".$alturaPadrao."&q=".$qualidadeJPEG."&zc=1".$preencherURL;			
		} else {
			return false;
		}
		exit;
	} else {
		$dadosImagem=$resultados[0];
	}

	//Detecta se a imagem é um PNG
	$nomeArquivo=explode(".", $dadosImagem["file"]);
	if ($nomeArquivo[1]=="png" and $preencher!="transparent") {
		$fileType="&f=png";
	}

	return $endereco_site."vendor/phpthumb/phpThumb.php?src=../../uploads/".$dadosImagem["type"]."/".$dadosImagem["file"]."&w=".$larguraPadrao."&h=".$alturaPadrao."&q=".$qualidadeJPEG.$preencherURL.$fileType;
	exit;
}
//Função que trabalha em conjunto com a função padrão do PHP array_walk_recursive
function abre_array($item, $key, &$output) {
    if ($key!="id" and $key!="visivel" and $key!="modificacao" and $key!="arquivo" and $key!="foto" and $key!="link" and $item!="javascript:void(0);") {
	    $output.="$item ";
    }
}
//Função que imprime na tela um array ou string, morrendo ou não
function dbg($valor,$die=true) {
	echo "<pre>";
	print_r($valor);
	if ($die) {
		die;
	}
}
function uploadImage($fileVariable,$targetPath,$outputName=false,$maxWidth=800,$maxHeight=600) {
	$tempFile = $fileVariable['tmp_name'];
	
	$extension = getExtension($fileVariable['name']);

	if (!$extension) {
		return false;
	}

	if ($outputName===false) {
		$outputName = uniqid (rand ()).".png";
	} else {
		$outputName=$outputName.".png";
	}

	$targetFile =  $targetPath ."/". $outputName;

	move_uploaded_file($tempFile,$targetFile);
	
	$img = false;
	
	if ($extension == 'jpg' || $extension == 'jpeg') {
		$img = imagecreatefromjpeg($targetFile);
	} elseif ($extension == 'png') {
		$img = imagecreatefrompng($targetFile);
	} elseif ($extension == 'gif') {
		$img = imagecreatefromgif($targetFile);
	}
	
	if ($img) {
		$width  = imagesx($img);
		$height = imagesy($img);
		$scale  = min($maxWidth/$width, $maxHeight/$height);
	
		if ($scale < 1) {
			$new_width = floor($scale*$width);
			$new_height = floor($scale*$height);
	
			$tmp_img = imagecreatetruecolor($new_width, $new_height);
	
			imagecopyresampled($tmp_img, $img, 0, 0, 0, 0,$new_width, $new_height, $width, $height);
			imagedestroy($img);
			$img = $tmp_img;
			
			imagepng($img, $targetFile, 0);
		}
	}
	return $outputName;
}

function uploadFile($fileVariable,$targetPath,$outputName=false) {
	$tempFile = $fileVariable['tmp_name'];
	
	$extension = getExtension($fileVariable['name']);

	if (!$extension) {
		return false;
	}

	if ($outputName===false) {
		$outputName = uniqid (rand ()).".".$extension;
	} else {
		$outputName=$outputName.".".$extension;
	}

	$targetFile =  $targetPath ."/". $outputName;

	move_uploaded_file($tempFile,$targetFile);

	return $outputName;
}

function getExtension($file) {
	$size = strlen($file);
	if( $file[($size)-3] == '.' )	{
		$extension = substr($file,-2);
	} elseif( $file[($size)-4] == '.' )	{
		$extension = substr($file,-3);
	} elseif( $file[($size)-5] == '.' )	{
		$extension = substr($file,-4);
	} else {
		$extension = false;
	}
	return strtolower($extension);
}

function executeCURL($token,$url,$vars=false) {
	if (empty($url)) {
		return false;
	}

	$headers = array();
	$headers[] = 'Cache-Control: no-cache';

	if ($token!==false) {
		$headers[] = 'Authorization: Bearer '.$token;
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	
	if (is_array($vars)) {
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);  //Post Fields
	}

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_HEADER, false);

	$server_output = curl_exec($ch);
	curl_close ($ch);

	return readJSON($server_output);
}

function addScheme($url, $scheme = 'http://') {
  return parse_url($url, PHP_URL_SCHEME) === null ? $scheme . $url : $url;
}

function tagToImg($content = false, $contentId = false, $thumbWidth = false, $thumbHeight = false ){

	$geralModel = new geral_Model();

	$galleryEditor = file_get_contents(ENDERECO_FISICO.'/includes/View/layout/_galleryEditor.php');
	$imageEditor = file_get_contents(ENDERECO_FISICO.'/includes/View/layout/_imageEditor.php');
	  
	preg_match_all('(#image-[0-9]+)', $content, $imgMatches, PREG_PATTERN_ORDER );
					
	$imgMatches= reset($imgMatches);            
	
	foreach ($imgMatches as $key => $tagImg) {
		$imgId = explode('-', $tagImg)[1];                
		$imgData = $geralModel->listaSQL('select * from attachments where id='.$imgId, true);
		
		if(!empty($imgData)){
			$imgUrl = ENDERECO_SITE.'uploads/image/'.$imgData->file;
			$imgTemplate = str_replace('#image', $imgUrl, $imageEditor);
			$content = str_replace($tagImg, $imgTemplate, $content);
		} else {
			$content = str_replace($tagImg, '', $content);
		}
	}
		
	preg_match_all('(#gallery-[a-zA-Z]+)', $content, $galleryMatches, PREG_PATTERN_ORDER);

	$galleryMatches= reset($galleryMatches);            

	foreach ($galleryMatches as $key => $tagGallery) {
		$galleryBlockName = explode('-', $tagGallery)[1];

		$galleryData = $geralModel->listaSQL("select * from attachments where block='".$galleryBlockName."' and relation=".$contentId);

		$imgRepeaterStart = explode('#foreach',$galleryEditor);
		$imgRepeaterEnd = explode('#endforeach', $imgRepeaterStart[1]); // [1]

		$imgsBlock = '';
		foreach ($galleryData as $key => $imgData) {
			$imgUrl = ENDERECO_SITE.'uploads/image/'.$imgData->file;
			$thumbUrl = showImage($imgData->id, $thumbWidth, $thumbHeight);
			$imgsBlock .= str_replace(['#image','#thumb','#subtitle'], [$imgUrl, $thumbUrl, $imgData->name], $imgRepeaterEnd[0]);
		}
	
		if(!empty($imgsBlock)){
			$galleryTemplate = $imgRepeaterStart[0].$imgsBlock.$imgRepeaterEnd[1];
			$content = str_replace($tagGallery, $galleryTemplate, $content);
		} else {
			$content = str_replace($tagGallery, '', $content);
		} 

	}
		
	return $content;
}

?>