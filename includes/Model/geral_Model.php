<?php
class geral_Model {
	function listaItens($tabela,$apartir,$limite=false,$categoria=false,$status=false,$order=false) {
		if (!$limite) {
			$modelSite=new ADMsite_Model();
			$dadosSite=$modelSite->dadosSite();
			$limite=$dadosSite["num_por_pagina"];
		}
		if ($categoria) {
			$categoriaWhere="WHERE categoria='".$categoria."'";
		} else {
			$categoriaWhere=false;
		}

		if($status=="no") {
			$statusWhere=false;
		} elseif ($status!==false) {
			if ($categoriaWhere) {
				$statusWhere=" AND status='".$status."'";
			} else {
				$statusWhere="WHERE status='".$status."'";
			}
		} else {
			if ($categoriaWhere) {
				$statusWhere=" AND status<>'9'";
			} else {
				$statusWhere="WHERE status<>'9'";
			}
		}
		if ($order) {
			$orderWhere="ORDER BY ".$order;
		}

		// Executa o comando no banco de dados
		global $db;
		$querySQL='SELECT * FROM '.$tabela.' '.$categoriaWhere.' '.$statusWhere.' '.$orderWhere.' LIMIT '.$apartir.', '.$limite.';';
		$mysql = $db->prepare($querySQL);
		$mysql->execute();
		$resultados = $mysql->fetchAll(PDO::FETCH_OBJ);
		if (!empty($resultados) and count($resultados)>0) {
			return $resultados;
		} else {
			return false;
		}
	}

	function dataItem($table,$data,$verifyDeleted=false) {
		$arrayColunas = array_keys($data);

		$valores=array();
		foreach ($arrayColunas as $valColunas) {
			array_push($valores, $valColunas."=:".$valColunas);
		}
		$valores=implode(" AND ", $valores);

		if ($verifyDeleted) {
			$statusWhere=" AND status<>'9'";
		}

		global $db;
		$mysql = $db->prepare('SELECT * FROM '.$table.' WHERE '.$valores.$statusWhere);
		$mysql->execute($data);
		$retorno = $mysql->fetchAll(PDO::FETCH_OBJ);
		if (count($retorno)>0) {
			return $retorno[0];
		} else {
			return false;
		}
	}

	function listaSQL($sql,$unico=false) {
		global $db;

		$mysql = $db->prepare($sql);
		$mysql->execute();
		$resultados = $mysql->fetchAll(PDO::FETCH_OBJ);
		if (!empty($resultados)) {
			if ($unico) {
				return $resultados[0];
			} else {
				return $resultados;
			}
		} else {
			return null;
		}
	}
	function executeSQL($sql) {
		global $db;

		$mysql = $db->prepare($sql);
		$mysql->execute();
		return true;
	}
}
?>
