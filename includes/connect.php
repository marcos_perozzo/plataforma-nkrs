<?php
/*=====================Conexão com o banco de dados MYSQL=====================*/
try {
	global $db_host;
	global $db_name;
	global $db_user;
	global $db_pass;
	$db = new PDO("mysql:host=".$db_host.";dbname=".$db_name, $db_user, $db_pass);
    $db->query("SET NAMES 'utf8'");
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e) {
	$data["erro"]["titulo"]="ERRO 500 - Conexão ao banco de dados";
	$data["erro"]["contedo"]=$e->getMessage();
	echo "<h1>".$data["erro"]["titulo"]."</h1>";
	echo "<p>".$data["erro"]["contedo"]."</p>";
	exit;
}
?>