<?php
/**
 * Class ST_Gift
 *
* @package Storetrooper
**/
class ST_Gift
{

    public function get($params = array())
    {
        $gift = $_SESSION[Storetrooper::$session_id]['gift'];

        $gift = (!empty($gift)) ? (object)$gift : FALSE;

        return $gift;
    }

    /**
     * Add a product to the gift
     * @param  array     $params
     */
    public function add($params = array())
    {
        $default = array(
            'from'      => NULL,
            'to'        => NULL,
            'message'   => NULL
        );
        $params = array_intersect_key($params, $default);

        $_SESSION[Storetrooper::$session_id]['gift'] = $params;
    }

    public function remove()
    {
        $_SESSION[Storetrooper::$session_id]['gift'] = array();
    }

}