<?php
/**
 * Class ST_Cart
 *
* @package Storetrooper
**/
class ST_Juno
{
    /**
     * Add a product to the cart
     * @param  array     $params
     * @return object
     */
    public function pay($params = array())
    {

        // tratar parametros
        // $params

        $ch = curl_init("http://www.juno.com.br/transfers");
        $fp = fopen("pagina_exemplo.txt", "w");

        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        //Tratar retorno
        return $retorno;
    }

    /**
     * Saves the cart to go to checkout method
     * @param  array     $data
     * @return object
     */
    public function save($data = array(), $site = false)
    {
        // Get the cart
        $cart = $this->get();
        if (!empty($data) && $data){
            $data = !is_object($data) ? (object) $data : $data;
            // Update the quantity
            foreach ($cart as $id => $item) {
                $cart[$id]->quantity = $data->quantity[$id];
                $cart[$id]->total = $cart[$id]->price * $data->quantity[$id];
                if($site)
                    $cart[$id]->image = str_replace($site, '', $cart[$id]->image);
            }
        }
        // Update session
        $_SESSION[Storetrooper::$session_id]['cart'] = $cart;
        ST_Curl::post('cart/save', array(
            'id_cart' => $_SESSION[Storetrooper::$session_id]['id_cart'],
            'products' => $cart
        ), FALSE);
        // Calculate the cart
        $this->_calc_cart();
        return $this;
    }

    public function get()
    {
        return $_SESSION[Storetrooper::$session_id]['cart'];
    }

    public function get_total()
    {
        return $this->get_resume()->items;
    }

    public function get_resume()
    {
        return (object) $_SESSION[Storetrooper::$session_id]['resume'];
    }

    public function reset()
    {
        $_SESSION[Storetrooper::$session_id]['cart'] = array();
        $_SESSION[Storetrooper::$session_id]['resume'] = array(
            'coupon'    => 0,
            'freight'   => 0,
            'gift'      => 0,
            'items'     => 0,
            'subtotal'  => 0,
            'total'     => 0
        );
        $this->save();
    }

    public function remove($key)
    {
        if (isset($_SESSION[Storetrooper::$session_id]['cart'][$key])){
            $_SESSION[Storetrooper::$session_id]['resume']['items'] -= 1;
            unset($_SESSION[Storetrooper::$session_id]['cart'][$key]);
            $this->_calc_cart();
            $this->save();
            return $this->get_total();
        } else
            return FALSE;
    }

    private function _calc_cart()
    {
        $cart = $this->get();

        $subtotal = 0;
        foreach ($cart as $id => $item) {
            $subtotal += $item->total;
        }

        $_SESSION[Storetrooper::$session_id]['resume']['subtotal']  = $subtotal;
        $_SESSION[Storetrooper::$session_id]['resume']['total']     = $subtotal;
    }

}