<?php
/**
 * Class ST_Categories
 *
* @package Storetrooper
**/
class ST_Categories
{
    /**
     * Get a json object with the store categories
     * @param  array      $params
     * @return json
     */
    public function get($params = array())
    {
        $options = array(
            'slug'      => FALSE,
            'recursive' => FALSE,
            'search'    => FALSE
        );
        $params = array_merge($options, $params);

        return ST_Curl::get('categories', $params, FALSE);
    }
}