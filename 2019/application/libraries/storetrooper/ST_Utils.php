<?php
/**
 * Class ST_Categories
 *
* @package Storetrooper
**/
class ST_Utils
{
    /**
     * Returns a JSON object with an addres based on the zipcode
     * @param  string       $zipcode
     * @param  bool         $output
     */
    public function get_address($zipcode, $output = FALSE)
    {
        $resultado = @file_get_contents('http://republicavirtual.com.br/web_cep.php?cep='.urlencode($zipcode).'&formato=query_string');
        if(!$resultado){
            $resultado = "&resultado=0&resultado_txt=erro+ao+buscar+cep";
        }
        $resultado = utf8_encode(rawurldecode($resultado));
        parse_str($resultado, $retorno);

        if (!$output)
            return json_encode($retorno);

        $this->output($retorno);
     }

    /**
     * Returns a JSON object with a list of states from Brazil
     * @param  bool         $output
     */
    public function get_states($output = FALSE)
    {
        $data = ST_Curl::get('utils/states');

        if (!$output)
            return $data;

        $this->output($data);
     }

    /**
     * Returns a JSON object with a list of cities from a State
     * @param  bool         $output
     */
    public function get_cities($state, $output = FALSE)
    {
        $data = ST_Curl::get('utils/cities', array('state' => $state));

        if (!$output)
            return $data;

        $this->output($data);
     }

    protected function output($data)
    {
        header('Content-Type: application/json');
        die(json_encode($data));
    }

    public function add_newsletter($data)
    {
        return ST_Curl::post('utils/newsletter/add', $data);
    }

    public function update_newsletter($data)
    {
        return ST_Curl::post('utils/newsletter/edit', $data, FALSE);
    }

    public function get_newsletter($email)
    {
        return ST_Curl::get('utils/newsletter/get', array('email' => $email));
    }

    public function get_coupon($coupon)
    {
        $coupon = ST_Curl::get('utils/coupon/get', array('coupon' => $coupon));

        if (!empty($coupon)){
            $_SESSION[Storetrooper::$session_id]['resume']['coupon'] = $coupon;
        }

        return $coupon;
    }

}