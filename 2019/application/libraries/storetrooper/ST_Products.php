<?php
/**
 * Class ST_Products
 *
* @package Storetrooper
**/
class ST_Products
{

    /**
     * Get a json object with the store products
     * @param  array      $params
     * @return json
     */
    public function get($params = array())
    {
        $options = array(
            'limit'      => 12,
            'offset'     => 0,
            'filter'     => array(),
            'search'     => FALSE,
            'order'      => FALSE,
            'getFilters' => FALSE
        );
        $params = array_merge($options, $params);
        return ST_Curl::get('products', $params, FALSE, (isset($params['getFilters']) && $params['getFilters']) ? FALSE : TRUE);
    }

    /**
     * Get a json object with the store products
     * @param  array      $params
     * @return json
     */
    public function get_variation($params = array())
    {
        $options = array(
            'id_product'    => FALSE,
            'id_variation'  => FALSE,
        );
        $params = array_merge($options, $params);
        return ST_Curl::get('products/get_variation', $params, FALSE, FALSE);
    }

    /*
     * Get a json object or array with the requested data
     * @param  array      $params
     * @return json
     */
    public function save_review($params = array())
    {
        return ST_Curl::post('products/save_review', $params);
    }
}