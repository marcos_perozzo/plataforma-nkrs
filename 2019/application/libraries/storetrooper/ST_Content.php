<?php
/**
 * Class ST_Content
 *
* @package Storetrooper
**/
class ST_Content
{
    /*
     * Get a json object or array with the requested data
     * @param  array      $params
     * @return json
     */
    public function get($params = array())
    {
        $options = array(
            'limit'     => 12,
            'offset'    => 0,
            'slug'      => FALSE
        );
        $params = array_merge($options, $params);

        return ST_Curl::get('content/get', $params);
    }

}