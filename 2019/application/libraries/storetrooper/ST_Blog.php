<?php
/**
 * Class ST_Blog
 *
* @package Storetrooper
**/
class ST_Blog
{
    /*
     * Get a json object or array with the requested data
     * @param  array      $params
     * @return json
     */
    public function get($params = array())
    {
        $options = array(
            'limit'     => 12,
            'offset'    => 0,
            'slug'      => FALSE,
            'tag'       => FALSE,
            'category'  => FALSE,
            'total'     => FALSE
        );
        $params = array_merge($options, $params);

        return ST_Curl::get('blog', $params, FALSE);
    }

    /*
     * Get a json object or array with the requested data
     * @param  array      $params
     * @return json
     */
    public function get_categories($params = array())
    {
        return ST_Curl::get('blog/categories', $params, FALSE);
    }

}