<?php
/**
 * Class ST_Store
 *
* @package Storetrooper
**/
class ST_Store
{

    public function get()
    {
        return ST_Curl::get('store');
    }

    public function get_installments($params = array())
    {
        $options = array(
            'price'      => 0,
        );
        $params = array_merge($options, $params);
        return ST_Curl::get('store/installments', $params, FALSE, FALSE);
    }

}