<?php
/**
 * Class ST_Addresses
 *
* @package Storetrooper
**/
class ST_Addresses
{
    /**
     * Insert an address related to a client
     * @param  array     $data
     * @return object
     */
    public function add($data)
    {
        if (!isset($data['client'])){
            $client = Storetrooper::client()->get();
            if ($client){
                $data['client'] = $client->id;
            }
        }
        return ST_Curl::post('addresses/add', $data, FALSE);
    }

    public function edit($data)
    {
        $client = Storetrooper::client()->get();
        if ($client){
            $data['client'] = $client->id;
        }
        return ST_Curl::post('addresses/edit', $data, FALSE);
    }

    /**
     * Get a json object with the client addresses
     * @param  array      $params
     * @return object
     */
    public function get($params = array())
    {
        $mandatory = array(
            'client'    => Storetrooper::client()->get()->id,
        );
        $params = array_merge($params, $mandatory);

        return ST_Curl::get('addresses/get', $params, FALSE);
    }


    public function delete($id)
    {
        $params = array(
            'id'        => $id,
            'client'    => Storetrooper::client()->get()->id
        );
        return ST_Curl::post('addresses/delete', $params, FALSE);
    }

}