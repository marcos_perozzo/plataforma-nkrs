<?php
/**
 * Class ST_Client
 *
* @package Storetrooper
**/
class ST_Client
{
    public function get($full = FALSE)
    {
        if ($full)
            return ST_Curl::get('client/get', array('id' => $_SESSION[Storetrooper::$session_id]['client']['id']));
        else
            return is_null($_SESSION[Storetrooper::$session_id]['client']) ? FALSE : (object) $_SESSION[Storetrooper::$session_id]['client'];
    }

    public function add($data)
    {
        if (isset($data['address'])){
            $address = $data['address'];
            unset($data['address']);
        }
        if (isset($data['newsletter'])){
            $newsletter = TRUE;
            unset($data['newsletter']);
        }

        $client = ST_Curl::post('client/add', $data, FALSE);

        if (!empty($client)){

            $this->_login($client);

            if (isset($address)){
                $address['client']  = $client->id;
                $address['billing'] = 1;
                $address['default'] = 1;
                Storetrooper::addresses()->add($address);
            }

            if (isset($newsletter)){
                $newsletter = array(
                    'name'   => $client->name.' '.$client->last_name,
                    'email'  => $client->email,
                    'status' => 1
                );
                Storetrooper::utils()->add_newsletter($newsletter);
            }
        }

        return $client;
    }

    public function edit($data)
    {
        if (isset($data['newsletter'])){
            $newsletter = TRUE;
            unset($data['newsletter']);
        }else{
            $newsletter = FALSE;
        }

        $data['id'] = $_SESSION[Storetrooper::$session_id]['client']['id'];

        $client = ST_Curl::post('client/edit', $data, FALSE);
        if (!empty($client)){
            // Edit client session info
            $this->_login($client);

            $newsletter = array(
                'name'   => $client->name.' '.$client->last_name,
                'email'  => $client->email,
                'status' => $newsletter ? 1 : 0
            );
            Storetrooper::utils()->update_newsletter($newsletter);
        }

        return $client;
    }

    public function edit_password($data)
    {
        $data['id'] = $_SESSION[Storetrooper::$session_id]['client']['id'];
        return ST_Curl::post('client/edit-password', $data, FALSE);
    }

    public function retrieve_password($email)
    {
        $data = array(
            'email' => $email
        );
        $response = ST_Curl::get('client/retrieve-password', $data, FALSE);

        return $response;
    }

    public function new_password($data)
    {
        $client = ST_Curl::post('client/new-password', $data, FALSE);

        $this->_login($client);

        return $client;
    }

    public function check_hash($hash)
    {
        $data = array(
            'hash' => $hash
        );
        $response = ST_Curl::get('client/check-hash', $data, FALSE);

        return $response;
    }

    public function login($data)
    {
        $session = $_SESSION[Storetrooper::$session_id];
        if (isset($session['id_cart']) && $session['id_cart'])
            $data['id_cart'] = $session['id_cart'];
        $client = ST_Curl::post('client/login', $data, FALSE);

        $this->_login($client);

        return $client;
    }

    public function facebook_login($data)
    {
        $session = $_SESSION[Storetrooper::$session_id];
        if (isset($session['id_cart']) && $session['id_cart'])
            $data['id_cart'] = $session['id_cart'];
        $client = ST_Curl::post('client/facebook-login', $data, FALSE);

        if($client && !empty($data['picture'])){
            $client->picture = $data['picture'];
        }

        if ($client){
            $this->_login($client);
        }

        return $client;
    }

    public function logout()
    {
        Storetrooper::clear_session('client');
    }

    private function _login($client)
    {
        $_SESSION[Storetrooper::$session_id]['client'] = array(
            'id'        => $client->id,
            'name'      => $client->name,
            'email'     => $client->email,
            'phone'     => $client->phone,
            'picture'   => isset($client->picture) ? $client->picture : FALSE,
            'is_valid'  => (boolean) $client->cpf,
        );
    }

}