<?php
/**
 * Class ST_Order
 *
* @package Storetrooper
**/
class ST_Orders
{

    public function add($data)
    {
        $gift = $_SESSION[Storetrooper::$session_id]['gift'];
        $id_cart = $_SESSION[Storetrooper::$session_id]['id_cart'];

        if ($gift){
            $data['gift'] = $gift;
        }
        if ($id_cart){
            $data['id_cart'] = $id_cart;
        }

        return ST_Curl::post('orders/add', $data, FALSE);
    }

    public function get($params = array())
    {
        $params['client'] = $_SESSION[Storetrooper::$session_id]['client']['id'];

        return ST_Curl::get('orders/get', $params, FALSE);
    }

}