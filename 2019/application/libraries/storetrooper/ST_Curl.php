<?php
/**
 * Class ST_Curl
 *
 * @package Storetrooper
 */
class ST_Curl
{

    private static $curl = FALSE;

    /**
     * Send a CURL to the REST API to the specified method with given params
     * @param  string     $method
     * @param  array      $params
     * @return json
     */
    public static function get($method, $params = array(), $safe = TRUE, $only_data = TRUE)
    {
        //Open the CURL if it's not opened yet
        self::open_curl();
        //Generate the URL
        $url = Storetrooper::api_url() . $method . '?' . http_build_query($params);
        // Get the options
        $options = self::get_options($url);
        // Set the options
        curl_setopt_array(self::$curl, $options);
        // Execute CURL
        return self::exec_curl($safe, $only_data);
    }

    public static function post($method, $params = array(), $safe = TRUE, $debug = FALSE)
    {
        //Open the CURL if it's not opened yet
        self::open_curl();
        //Generate the URL
        $url = Storetrooper::api_url() . $method;
        // Get the options
        $options = self::get_options($url) + array(
            CURLOPT_POST        => 1,
            CURLOPT_POSTFIELDS  => http_build_query($params)
        );
        // Set the options
        curl_setopt_array(self::$curl, $options);
        
        if($debug){
            var_dump($url, $response, $options);
            exit();
        }        
        
        // Execute CURL
        return self::exec_curl($safe);
    }

    public static function delete($method, $params = array(), $safe = TRUE)
    {
        //Open the CURL if it's not opened yet
        self::open_curl();
        //Generate the URL
        $url = Storetrooper::api_url() . $method;
        // Get the options
        $options = self::get_options($url) + array(
            CURLOPT_CUSTOMREQUEST   => "DELETE",
            CURLOPT_POSTFIELDS      => http_build_query($params)
        );
        // Set the options
        curl_setopt_array(self::$curl, $options);
        // Execute CURL
        return self::exec_curl($safe);
    }

    public static function get_options($url)
    {
        // Get Trooper Key
        $trooper = Storetrooper::get_trooper();
        // Set headers
        $headers = array(
            'Authorization: key=' . Storetrooper::key(),
            'Content-Type: application/x-www-form-urlencoded'
        );
        if (!is_null($trooper)){
            $headers[] = 'trooper: ' . $trooper;
        }
        $options = array(
            CURLOPT_URL             => $url,
            CURLOPT_HTTPHEADER      => $headers,
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_SSL_VERIFYPEER  => 0,
            CURLOPT_IPRESOLVE       => CURL_IPRESOLVE_V4,
            CURLOPT_ENCODING        =>  ''
        );
        return $options;
    }

    public static function open_curl()
    {
        if (self::$curl)
            return;
        self::$curl = curl_init();
    }

    public static function close_curl()
    {
        // Close request to clear up some resources
        if (self::$curl)
            curl_close(self::$curl);
    }

    public static function exec_curl($safe = TRUE, $only_data = TRUE)
    {
        // Saves the response to $data
        $response = curl_exec(self::$curl);
        if ($response !== FALSE){
            $data = json_decode($response);
        }else{
            echo '<pre>';die(var_dump(curl_error(self::$curl)));
        }
        // Check Safe return
        if (!$safe && is_object($data) && (!isset($data->status) || !$data->status)){
            die(json_encode(array(
                'status'    => FALSE,
                'title'     => 'OPS',
                'message'   => isset($data->message) ? $data->message : 'Desculpe, ocorreu um problema inesperado. Contate o administrador ou tente novamente mais tarde.',
                'trace'     => $data
            )));
        }
        if (!isset($data->status) && (json_last_error() != JSON_ERROR_NONE)){
            echo '<pre>';die(var_dump($response));
        }
        // Return the Reponse
        if (isset($data) && is_object($data) && $data->status){
            if (isset($data->trooper) && $data->trooper){
                Storetrooper::set_trooper($data->trooper);
            }
            if (isset($data->total)){
                unset($data->status);
                return $data;
            }
            return property_exists($data, 'data') && $only_data ? $data->data : $data;
        }
        return array();
    }

}