<?php
/**
* @author   Diogo Taparello <diogotaparello@gmail.com>
* @author   Fabio Bachi <fabio.bachi@gmail.com>
* @author   Ralf da Rocha <ralf.am.rocha@gmail.com>
* @copyright 2016
* @package Storetrooper
**/

require_once Storetrooper::path() . 'ST_Curl.php';

class Storetrooper
{
    /**
     * @var Authorization key from Storetrooper
     */
    protected static $key;

    /**
     * @var REST API URL from Storetrooper
     */
    protected static $api_url;

    /**
     * @var string Path to the library base directory
     */
    protected static $path = null;

    /**
     * Session name
     */
    public static $session_id = '_storetrooper';

    public function __construct($key = FALSE, $api_url = FALSE)
    {
        $key        || die('Uma chave de autorização setar você deve.');
        $api_url    || die('Uma URL de API informar você deve.');

        // Set API authorization fields
        self::$key = $key;
        self::$api_url = $api_url;

        // Create a new Storetrooper session if it didnt't exist
        if (!isset($_SESSION[Storetrooper::$session_id]))
            $this->clear_session();
    }

    /**
     * Check if property is inacessible and instantiate the related object
     * @author Ralf da Rocha [ralf@ezoom.com.br]
     * @date   2016-11-02
     * @param  string     $name
     * @return object
     */
    public function __get($name)
    {
        if (!property_exists($this,$name)){
            $className = 'ST_'.ucfirst($name);
            require_once Storetrooper::path() . $className . '.php';
            $this->{$name} = new $className;
            return $this->{$name};
        }
    }
    /**
     * Check if property is inacessible and instantiate the related object
     * @author Ralf da Rocha [ralf@ezoom.com.br]
     * @date   2016-11-02
     * @param  string     $name
     * @return object
     */
    public static function __callStatic($name, $arguments)
    {
        $className = 'ST_'.ucfirst($name);
        require_once Storetrooper::path() . $className . '.php';
        return new $className;
    }

    /**
     * Returns the Authorization Key
     * @return string
     */
    static function key()
    {
        return self::$key;
    }

    /**
     * Returns the REST API URL
     * @return string
     */
    static function api_url()
    {
        return self::$api_url;
    }

    /**
     * Returns the path to the library
     * @return string
     */
    static function path()
    {
        if (self::$path === null) {
            self::$path = dirname(__FILE__) . '/storetrooper/';
        }

        return self::$path;
    }

    /**
     * Set the trooper key
     */
    static function set_trooper($trooper)
    {
        $_SESSION[Storetrooper::$session_id]['trooper'] = $trooper;
    }

    /**
     * Get the trooper key
     * @return string
     */
    static function get_trooper()
    {
        if (isset($_SESSION[Storetrooper::$session_id]['trooper'])){
            return $_SESSION[Storetrooper::$session_id]['trooper'];
        }else{
            return null;
        }
    }

    /**
     * Clear the ST session by setting the basic structure for the SDK.
     */
    static function clear_session($key = FALSE)
    {
        if ($key){
            $_SESSION[Storetrooper::$session_id][$key] = $key === 'client' || $key === 'trooper' ? null : array();
        }else{
            $_SESSION[Storetrooper::$session_id] = array(
                'id_cart'    => null,
                'cart'      => array(),
                'client'    => null,
                'trooper'   => null,
                'gift'      => array(),
                'resume'    => array(
                    'coupon'    => 0,
                    'freight'   => 0,
                    'items'     => 0,
                    'subtotal'  => 0,
                    'total'     => 0
                ),
            );
        }
    }

    public function __destruct() {
        ST_Curl::close_curl();
    }

}