<?php
/**
* @author   Diogo Taparello <diogotaparello@gmail.com>
* @author   Fabio Bachi <fabio.bachi@gmail.com>
* @author   Ralf da Rocha <ralf.am.rocha@gmail.com>
* @copyright 2016
* @package Storetrooper
**/

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Storetrooper.php';

class Storetrooper_ci extends Storetrooper
{
    /**
     * @var Codeigniter instance object
     */
    private $ci;

    public function __construct($key = FALSE, $api_url = FALSE)
    {
        $this->ci =& get_instance();
        $this->ci->config->load('storetrooper');

        $key        = $this->ci->config->item('storetrooper_key');
        $api_url    = $this->ci->config->item('storetrooper_api_url');

        parent::__construct($key,$api_url);
    }

}