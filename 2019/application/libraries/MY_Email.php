<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Email extends CI_Email
{

    private $ci;

    public function __construct($params = array())
    {
        parent::__construct($params);

        $this->ci =& get_instance();
        $config = $this->ci->config->item('config_email');

    }

    public function sendEmail ($params = array())
    {

        extract($params);

        if (!is_array($from))
            die('O parâmetro $from deve ser um array com as "name" e "email".');

        $this->clear();

        $this->from($this->smtp_user, 'Site Utimil - ' . $from['name']);
        $this->to($to);
        $this->reply_to($from['email']);

        $this->subject($subject);
        $this->message($message);

        return $this->send();

    }

}