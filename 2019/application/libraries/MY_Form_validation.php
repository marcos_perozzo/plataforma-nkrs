<?php if (!defined('BASEPATH')) exit('No direct script access allowed.');

class MY_Form_validation extends CI_Form_validation {

	public function __construct() {
		parent::__construct();
	}

    function error_array($keys = FALSE)
    {
        return ($keys) ? array_keys($this->_error_array) : $this->_error_array;
    }

    /**
     *
     * valid_cpf
     *
     * Verifica CPF é válido
     * @access  public
     * @param   string
     * @return  bool
     */
    function valid_cpf($cpf)
    {
        $CI =& get_instance();

        $CI->form_validation->set_message('valid_cpf', 'O %s informado não é válido.');

        $cpf = preg_replace('/[^0-9]/','',$cpf);

        if(strlen($cpf) != 11 || preg_match('/^([0-9])\1+$/', $cpf))
        {
            return false;
        }

        // 9 primeiros digitos do cpf
        $digit = substr($cpf, 0, 9);

        // calculo dos 2 digitos verificadores
        for($j=10; $j <= 11; $j++)
        {
            $sum = 0;
            for($i=0; $i< $j-1; $i++)
            {
                $sum += ($j-$i) * ((int) $digit[$i]);
            }

            $summod11 = $sum % 11;
            $digit[$j-1] = $summod11 < 2 ? 0 : 11 - $summod11;
        }

        return $digit[9] == ((int)$cpf[9]) && $digit[10] == ((int)$cpf[10]);
    }

    function valid_cnpj($cnpj)
    {
        $CI =& get_instance();

        $CI->form_validation->set_message('valid_cnpj', 'O %s informado não é válido.');

        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
        // Valida tamanho
        if (strlen($cnpj) != 14)
            return false;
        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
            return false;
        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
        {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }

    function valid_recaptcha($value)
    {
        $CI =& get_instance();

        $CI->form_validation->set_message('valid_recaptcha', 'O %s informado não é válido.');

        try {

            $url = 'https://www.google.com/recaptcha/api/siteverify';

            $data = ['secret'   => '6LecV5IUAAAAAOfHlnoW8-pK4TTdh2e0Lb4TD9KZ',
                     'response' => $value,
                     'remoteip' => $_SERVER['REMOTE_ADDR']];

            $options = [
                'http' => [
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                ],
                'ssl' => [
                    'verify_peer'       => FALSE,
                    'verify_peer_name'  => FALSE,
                ],
            ];

            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            $retorno = json_decode($result)->success;

            return $retorno;
        }
        catch (Exception $e) {
            return null;
        }
    }

}