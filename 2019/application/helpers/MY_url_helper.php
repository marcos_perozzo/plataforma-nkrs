<?php defined('BASEPATH') OR exit('No direct script access allowed');

function base_img($file = '', $module = 'comum')
{
    $CI =& get_instance();
    return $CI->config->base_url('application/modules/'.$module."/assets/img/{$file}");
}

function base_svg($file = '', $module = 'comum')
{
    $CI =& get_instance();
    if (!preg_match("/\.svg/i", $file))
        $file .= '.svg';
    return $CI->config->base_url('application/modules/'.$module."/assets/svg/{$file}");
}

function base_css($file = '', $module = 'comum')
{
    $CI =& get_instance();
    if (!preg_match("/\.css/i", $file))
        $file .= '.css';
    return $CI->config->base_url('application/modules/'.$module."/assets/css/{$file}");
}

function base_js($file = '', $module = 'comum')
{
    $CI =& get_instance();
    if ($file != '' && !preg_match("/\.js/i", $file))
        $file .= '.js';
    return $CI->config->base_url('application/modules/'.$module."/assets/js/{$file}");
}

function base_plugin($file = '', $module = 'comum')
{
    $CI =& get_instance();
    return $CI->config->base_url('application/modules/'.$module."/assets/plugins/{$file}");
}

if (!function_exists('load_svg')) {
    function load_svg($file = '', $module = false)
    {
        $CI =& get_instance();
        $module = ($module) ? CI::$APP->router->fetch_module() : 'comum';

        return file_get_contents(APPPATH."modules/{$module}/assets/svg/{$file}");
    }
}

if (!function_exists('fix_image_resize')) {
    function fix_image_resize($src = '')
    {
        return str_replace('/../adsites/userfiles', '/adsites/userfiles', $src);
    }
}

if (!function_exists('preloader')) {
    function preloader($class = FALSE)
    {
        $CI =& get_instance();
        return $CI->load->view('comum/preloader',array('class' => $class),TRUE);
    }
}

if ( ! function_exists('get_youtube_id')){

    function get_youtube_id($value)
    {
        preg_match("/(?<=(v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\/)[^&\n]+|(?<=embed\/)[^\"&\n]+|(?<=(?:v|i)=)[^&\n]+|(?<=youtu.be\/)[^&\n]+/", $value, $matches);
        return $matches[0];
    }
}

if ( ! function_exists('get_vimeo_id')){

    function get_vimeo_id($value)
    {
        preg_match("/(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $value, $matches);
        return $matches[5];
    }
}

if (! function_exists('slug')) {
    function slug($str = null, $table = null, $id = null, $separator = '-', $lowercase = true)
    {
        $CI = &get_instance();
        $CI->load->helper('text');
        $str = convert_accented_characters($str);
        $str = url_title($str, $separator, $lowercase);
        if ($table) {
            $unique = false;
            $count = 1;
            $slug = $str;
            while (!$unique) {
                $CI->db->select('COUNT(*) AS total')->from($table)->where('slug', $slug);
                if ($id) {
                    $CI->db->where('id !=', $id);
                }
                $query = $CI->db->get();
                $query = $query->row();
                if ((int) $query->total == 0) {
                    $unique = true;
                    $str = $slug;
                } else {
                    $slug = $str.'-'.$count++;
                }
            }
        }

        return $str;
    }
}