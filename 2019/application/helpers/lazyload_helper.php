<?php (defined('BASEPATH')) or exit('No direct script access allowed');

if (! function_exists('lazyload')) {
    /**
     * @author Ralf da Rocha [ralf@ezoom.com.br]
     * @date   2016-09-05
     * @param  array      $params
     * @return [string]
     */
    function lazyload($params = array())
    {
        $options = array(
            'src' => FALSE,
            'view' => FALSE,
            'tag' => 'div'
        );
        $params = array_merge($options, $params);
        if (!$params['src'])
            return '';

        $UA =& load_class('User_agent', 'libraries');

        $view = $params['view'];
        $tag = $params['tag'];
        $src = $params['src'];
        unset($params['src'], $params['view'], $params['tag']);

        $container = '<'.$tag;

        if ($UA->is_robot()){
            $img = '<img src="'.$src.'"'.(isset($params['alt']) ? ' alt="'.$params['alt'].'"' : '').' />';
            $view = $img .$view;
            unset($params['alt']);
        } else {
            $params['data-src'] = $src;
        }
        foreach ($params as $key => $value) {
            $container .= ' '.$key.'="'.$value.'"';
        }

        $container .= '>'.($view ? $view : '').'</'.$tag.'>';

        return $container;
    }
}