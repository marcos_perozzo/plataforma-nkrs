<?php
if ( ! function_exists('number_to_mysql_decimal')) {

    function number_to_mysql_decimal($value = null)
    {
        if (is_null($value)) {
            return $value;
        }
        $source = array('.', ',');
        $replace = array('', '.');
        $value = str_replace($source, $replace, $value); //remove os pontos e substitui a virgula pelo ponto
        return $value; //retorna o valor formatado para gravar no banco
    }

}

if ( ! function_exists('mysql_decimal_to_number')) {

    function mysql_decimal_to_number($value){
        return number_format($value, 2, ',', '.');
    }
}


if ( ! function_exists('mysql_date')) {

    function mysql_date($date){
        return preg_replace('#(\d{2})/(\d{2})/(\d{4})\s?(.*)?#', '$3-$2-$1 $4', $date);
    }

}

if ( ! function_exists('format_date')) {

    function format_date($date){
        return preg_replace('#(\d{4})/(\d{2})/(\d{2})\s?(.*)?#', '$3/$2/$1 $4', $date);
    }

}

if( ! function_exists('format_mask')) {
    function format_mask($val, $mask){
        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++){
            if($mask[$i] == '#'){
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }else{
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
}


