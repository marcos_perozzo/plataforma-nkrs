<div class="content-top">
    <button class="common-back"><?php echo load_svg('back-arrow.svg'); ?></button>
    <h1 class="account-title">Adicionar novo Endereço</h1>
</div>
<form action="<?php echo site_url('minha-conta/meus-enderecos/add'); ?>" method="POST" id="form-address" class="common-form" novalidate>
    <input type="hidden" name="origin" value="cart">
    <div class="address-box">
        <div class="row">
            <div class="form-group">
                <input type="text" name="address[label]" id="address-label" required placeholder="Descrição">
            </div>
            <div class="form-group">
                <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="address[zipcode]" id="address-zipcode" class="mask-zipcode" required placeholder="CEP">
            </div>
        </div>
        <div class="row">
            <div class="form-group address-group">
                <input type="text" name="address[street]" id="address-street" required placeholder="Endereço">
            </div>
            <div class="form-group number-group">
                <input type="number" name="address[number]" id="address-number" placeholder="Número" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <input type="text" name="address[complement]" id="address-complement" placeholder="Complemento">
            </div>
            <div class="form-group">
                <input type="text" name="address[suburb]" id="address-suburb" required placeholder="Bairro">
            </div>
        </div>
        <div class="row">
            <div class="form-group relative">
                <?php $this->load->view('comum/preloader'); ?>
                <div class="select-wrapper">
                    <select name="address[state]" id="address-state" data-value="" required>
                        <option value="">ESTADO</option>
                    </select>
                </div>
            </div>
            <div class="form-group relative">
                <?php $this->load->view('comum/preloader'); ?>
                <div class="select-wrapper">
                    <select name="address[city]" id="address-city" data-value="" required class="search-select">
                        <option value="">CIDADE</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group final">
            <button type="submit" id="save-address" class="common-button">
                <?php $this->load->view('comum/preloader'); ?>
                <?php echo load_svg('save.svg'); ?>
                <span>Salvar</span>
            </button>
        </div>
    </div>
</form>