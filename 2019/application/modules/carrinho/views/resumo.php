<?php
    if (!isset($coupon) && isset($resume)){
        $coupon = $resume->coupon;
    }
    $coupon_value = empty($coupon) ? 0 : $coupon->value/100;
    $coupon_discount = !empty($resume) ? $coupon_value * $resume->subtotal : 0;
    $transfer_price = 0;
    if (isset($resume->total) && isset($hasPromo) && !$hasPromo)
        $transfer_price = (($resume->total - $coupon_discount) * DEBT_PRICE_DISCOUNT) / 100;
 ?>
<div id="cart-prices" data-has-promo="<?php echo (isset($resume->total) && isset($hasPromo) && !$hasPromo) ? '0' : '1' ; ?>">
    <div class="price-subtotal total-dup">
        <label class="label">Subtotal</label>
        <div class="data">R$ <?php echo !empty($resume) ? mysql_decimal_to_number($resume->subtotal) : '0,00'; ?></div>
    </div>
    <div class="price-coupon total-dup" data-type="percent" data-value="<?php echo isset($coupon) ? $coupon_value : '0'; ?>">
        <label class="label">Cupom de desconto</label>
        <div class="data">-R$ <?php echo isset($coupon) ? mysql_decimal_to_number($coupon_discount) : '0,00'; ?></div>
    </div>
    <div class="price-transfer-discount total-dup" data-type="percent" data-value="<?php echo $transfer_price; ?>">
        <label class="label">Transferência Bancária</label>
        <div class="data">-R$ <?php echo mysql_decimal_to_number($transfer_price); ?> <small>(-<?php echo DEBT_PRICE_DISCOUNT; ?>%)</small></div>
    </div>
    <?php if ($client){ ?>
    <div class="price-freight total-dup">
        <label class="label">Frete</label>
        <?php
            $chosenShipping = 0;
            foreach ($shipping as $key => $val) {
                if($resume->shipping == $val->id){
                    $chosenShipping = $val->price;
                    ?>
                    <div class="data">R$ <?php echo isset($val) ? mysql_decimal_to_number($val->price) : '0,00'; ?></div>
                 <?php
                }
            }
            if(isset($resume->shipping) && ($resume->shipping === null || $resume->shipping === FALSE)){ ?>
                <div class="data">Retirada na loja</div>
            <?php } ?>
            <?php if(isset($resume->shipping) && !$resume->shipping){ ?>
                <div class="data">-</div>
            <?php } ?>
    </div>
    <?php } ?>
    <div class="price-total total-dup">
        <label class="label">Valor Total</label>
        <div class="data"
             data-full-price="R$ <?php echo !empty($resume) ? mysql_decimal_to_number($resume->total - $coupon_discount + $chosenShipping) : ''; ?>"
             data-transfer-price="R$ <?php echo !empty($resume) ? mysql_decimal_to_number($resume->total - $coupon_discount - $transfer_price + $chosenShipping) : ''; ?>">
            R$ <?php echo !empty($resume) ? mysql_decimal_to_number($resume->total - $coupon_discount + $chosenShipping) : '0,00'; ?>
            <?php /*
                <span>em até 10x de 489,59 sem juros</span>
                <small>ou</small> R$4.295,90 <small>á vista</small>
            */ ?>
        </div>
    </div>
</div>
<?php if (isset($isCheckoutResume) && $isCheckoutResume){ ?>
<script type="text/javascript">
    fbq('track', 'InitiateCheckout', {
        'value': '<?php echo $resume->total - $coupon_discount + $chosenShipping; ?>',
        'currency': 'BRL',
        'num_items': <?php echo $total_cart; ?>
    });

    ga('ec:setAction','checkout', {
        'step': 1,
        'revenue': '<?php echo $resume->total - $coupon_discount + $chosenShipping; ?>'
    });
    ga("send", "event", "Start Checkout", "click", "startCheckout");
</script>
<?php } ?>