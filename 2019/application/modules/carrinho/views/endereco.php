<?php foreach ($addresses as $address){ ?>
<div class="address-content" data-id="<?php echo $address->id; ?>">
    <div class="loader"><?php $this->load->view('comum/preloader'); ?></div>
    <div class="address-info common-radio">
        <input type="radio" name="address" id="radio-address-<?php echo $address->id ?>" value="<?php echo $address->id; ?>" required data-zipcode="<?php echo $address->zipcode; ?>" />
        <div class="radio"></div>
        <label for="radio-address-<?php echo $address->id ?>">
            <h4 class="title"><?php echo $address->label; ?></h4>
            <div class="common-text">
                <p>
                    <?php echo $address->street.', '.$address->number.($address->complement ? ', '.$address->complement : '').
                    ', Bairro: '.$address->suburb.' - '.$address->zipcode.', '.$address->city.' - '.$address->uf; ?>
                </p>
            </div>
        </label>
    </div>
</div>
<?php } ?>