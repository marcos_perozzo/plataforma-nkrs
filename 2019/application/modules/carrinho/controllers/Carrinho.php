<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrinho extends MY_Controller
{

    public function index()
    {

        $resume = array();
        $shipping = array();
        if($this->client) {
            $coupon = $_SESSION[Storetrooper::$session_id]['resume']['coupon'];
            $addresses = $this->st->addresses->get();

            $resume = $this->st->cart->get_resume();
            $address = $this->st->addresses->get(array('client' => $this->client->id, 'id' => isset($resume->address) ? $resume->address : null));

            if(isset($resume->shipping) && ($resume->shipping === null || $resume->shipping === FALSE))
                $shipping = $this->st->shipping->get(array(
                    'zipcode' => $address->zipcode,
                    'price' => $resume->total
                ));

        } else {
            $coupon = FALSE;
            $addresses = array();
        }

        $this->template
             ->add_css('css/carrinho')
             ->add_js('js/carrinho')
             ->add_js('js/address', 'comum')
             ->set('title', 'Utimil - Carrinho')
             ->set('cart', $this->st->cart->get())
             ->set('coupon', $coupon && $coupon->status && !is_null($coupon) ? $coupon : FALSE)
             ->set('addresses', $addresses)
             ->set('resume', $resume)
             ->set('shipping', $shipping)
             ->set('info_store', $this->st->content->get(array(
                'content' => 'informacoes-retirada-loja',
                'limit' => 1
             )))
             ->build('carrinho');
    }

    public function add()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $data = $this->input->post();
        $item = $this->st->products->get( array('id' => $data['product']) );
        if ($item->promo)
            $data['promo'] = TRUE;
        var_dump($data);
        exit();
        $product = $this->st->cart->add($data);

        $json = array(
            'status'    => TRUE,
            'product'   => $product,
            'total'     => $this->st->cart->get_total()
        );

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($json));
    }

    public function remove()
    {

        if (!$this->input->is_ajax_request())
            show_404();

        $total = $this->st->cart->remove($this->input->post('id'));

        $json = array(
            'success' => TRUE,
            'total' => $total
        );

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($json));
    }

    public function check_coupon()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $coupon = $this->st->utils->get_coupon($this->input->post('coupon'));

        if (isset($coupon) && !empty($coupon) && $coupon->status && $coupon->limit_use >= 1){
            $json = array(
                'title' => 'Cupom Válido',
                'message' => 'O desconto de seu cupom será aplicado.',
                'class' => 'success',
                'status' => TRUE,
                'type'  => $coupon->type, //percent ou value
                'value' => $coupon->percent, //valor a ser descontado ou porcentagem
            );
        }else{
            $json = array(
                'title' => 'Cupom Inválido',
                'message' => 'Digite um código de cupom válido.',
                'class' => 'error',
                'status' => FALSE
            );
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($json));
    }


    public function calculate_freight()
    {

        if (!$this->input->is_ajax_request())
            show_404();

        $zipcode = $this->input->post('zipcode');
        $product = $this->input->post('product');
        $quantity = $this->input->post('quantity');
        $view = $this->input->post('view');
        $price = $this->input->post('price');

        if (!$price){
            if ($product){
                $productDetails = $this->st->products->get( array('id' => $product, 'details' => TRUE) );
                if ($productDetails && !empty($productDetails)){
                    $price = (float) $productDetails->price;
                }
            } else {
                $price = $this->st->cart->get_resume();
                $price = $price->subtotal;
            }
        }

        $shipping = $this->st->shipping->get(array(
            'zipcode' => $zipcode,
            'simulate' => $product,
            'quantity' => $quantity,
            'price' => $price
        ));

        $json = array('status' => TRUE, 'view' => $this->load->view($view, array('options' => $shipping), TRUE));

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($json));
    }

    public function save_cart()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $cart = $this->st->cart->get();

        if (empty($cart)) {

            $response = array(
                'status'    => FALSE,
                'class'     => 'error',
                'title'     => 'Ocorreu um erro!',
                'message'   => 'Seu carrinho está vazio.',
                'redirect'  => site_url('produtos')
            );

        }else{

            $data = $this->input->post();

            if(!$this->client) {
                $this->session->set_userdata('st_autocheckout', TRUE);
                $response = array(
                    'status'    => FALSE,
                    'class'     => 'alert',
                    'title'     => 'Atenção!',
                    'message'   => 'Você precisa efetuar login para prosseguir!',
                    'redirect'  => site_url('login')
                );
            } else if (!$this->client->is_valid){
                $this->session->set_userdata('st_autocheckout', TRUE);
                $response = array(
                    'status'    => FALSE,
                    'class'     => 'alert',
                    'title'     => 'Atenção!',
                    'message'   => 'Seu cadastro em nosso site não está completo. Complete seu cadastro para prosseguir!',
                    'redirect'  => site_url('minha-conta/meus-dados')
                );
            } else {

                // Validação
                $this->load->library('form_validation');
                if(!$this->input->post('pickup_at_store')) {
                    $this->form_validation->set_rules('id_address', 'Endereço', 'trim|required');
                    $this->form_validation->set_rules('id_shipping', 'Frete', 'trim|required');
                } else {
                    $this->form_validation->set_rules('pickup_at_store', 'Endereço', 'trim|required');
                }

                if ($this->form_validation->run()) {

                    $response = array(
                        'status'    => TRUE,
                        'redirect'  => site_url('carrinho/checkout')
                    );
                } else {
                    $response = array(
                        'status'    => FALSE,
                        'class'     => 'alert',
                        'title'     => 'Atenção!',
                        'message'   => 'Você deve informar a forma envio do pedido!',
                    );
                }

            }

            $_SESSION[Storetrooper::$session_id]['resume']['pickup_at_store'] = $data['pickup_at_store'];
            $_SESSION[Storetrooper::$session_id]['resume']['shipping'] = $data['id_shipping'];
            $_SESSION[Storetrooper::$session_id]['resume']['address'] = $data['id_address'];
            $store = $this->st->store->get();
            $this->st->cart->save($data, $store->base_path);
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

    public function get_total_payment()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $total = $this->input->post('total');

        if(!$total)
            show_404();

        $response = array(
            'status'=> TRUE,
            'view'  => $this->load->view('cart-total', array('total' => $total), TRUE)
        );

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

}