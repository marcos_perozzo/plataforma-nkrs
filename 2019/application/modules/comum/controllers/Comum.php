<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Comum extends MY_Controller
{
    public function index()
    {
        $slugContent = $this->uri->segment(1);
        $content = $this->st->content->get(array('content' => $slugContent ));
        if($content){
            $this->template
                ->set('title', 'NKRS - '.$content->title)
                ->add_css('css/common_pages')
                ->add_js('js/common_pages')
                ->set('dataContent', $content)
                ->build('comum_content');
        } else {
            set_status_header(404);
            $this->template->set('title', 'NKRS - Página não encontrada')
                        ->add_css('css/error_404', 'comum')
                        ->build('comum/error_404');
        }
    }

    public function get_cities()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $this->st->utils->get_cities($this->input->post('id'), TRUE);
    }

    public function get_states()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $this->st->utils->get_states(TRUE);
    }

    public function get_cep()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $this->st->utils->get_address($this->input->post('cep'), TRUE);
    }
}