/**
 * @author: Henrique Orlandin
 * @copyright: StoreTrooper - 2017
 */


var app;
var overing;

function App(){ return this };

App.prototype.init = function() {
    var self = this;

    this.initMenuMobile();
    this.initFixTransitions();
    this.initMask();
    this.initArrowsListener();
    this.history = new History();
    this.initNotification();
    this.initFreight();
    this.configSelects();
    this.setForm($('.ajax-form'));

    $("body").removeClass("preload");
    $('.lazyload:not(.ondemand)').lazyload();
};

App.prototype.initFixTransitions = function() {
    $('.animation-element').on('mouseover',function(){
        var element = $(this);
        $('body').addClass('animating');
        requestAnimationFrame(function(){
            element.addClass('doing');
        }.bind(this));
    }).on('mouseleave',function() {
        var element = $(this);
        $('body').addClass('animating');
        requestAnimationFrame(function(){
            element.removeClass('doing');
        }.bind(this));
    });
    $(window).on('resize',function(){
        $('body').removeClass('animating');
    });
};

App.prototype.initMask = function() {

    $('body').on('keypress', '.mask-pagination', function(evt) {
        if(evt.key == 0)
            evt.preventDefault();

        if ((evt.which < 48 || evt.which > 57) && evt.which != 13)
            evt.preventDefault();

        if(evt.which != 13 && ($(this).val() + evt.key) > parseInt($(this).attr('max'))) {
            $(this).val($(this).attr('max'));
            evt.preventDefault();
        }
    });

    $('body').on('keypress', '.mask-quantity', function(evt) {
        if(evt.key == 0)
            evt.preventDefault();
        if ((evt.which < 48 || evt.which > 57))
            evt.preventDefault();
    });

    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };
    $('.mask-phone').mask(SPMaskBehavior,{onKeyPress: function(val, e, field, options) {field.mask(SPMaskBehavior.apply({}, arguments), options);}});
    $('.mask-cpf').mask('999.999.999-99');
    $('.mask-cnpj').mask('99.999.999/9999-99');
    $('.mask-zipcode').mask('99999-999');
    $('.mask-date').mask("99/99/9999");

};

App.prototype.initArrowsListener = function(first_argument) {
    if($('.common-arrows').length) {
        setInterval(function(){
            $('.slick-slider').each(function(index, el) {
                var arrow_selector = $(el).parent().find('.common-arrows');
                if($(el).find('.slick-active').length < $(el).find('.slick-slide').length && arrow_selector.length && arrow_selector.css('display') == 'none') {
                    $(el).parent().find('.common-arrows').fadeIn();
                } else if($(el).find('.slick-active').length == $(el).find('.slick-slide').length && arrow_selector.length && arrow_selector.css('display') != 'none') {
                    $(el).parent().find('.common-arrows').fadeOut();
                }
            });
        },1000);
    }
};

App.prototype.initNotification = function(){
    var $notification = $('#cart-notification');
    $('#close-notification').on('click', function(){
        $notification.removeClass('open');
    });
    $notification.on('mouseenter', function(){
        clearTimeout(this.notificationTimer);
    }.bind(this)).on('mouseleave', function(){
        this.notificationTimer = setTimeout(function(){
            $notification.removeClass('open');
        }, 5000);
    }.bind(this)).find('.product-image').lazyload({
        autoLoad: false
    });
    this.notificationTimer = '';
};

App.prototype.initMenuMobile = function(){
    $('#header').on('click','.toggle-menu', function(){
        $('body').addClass('animating');
        requestAnimationFrame(function(){
            $('body').toggleClass('menu-open');
            $('#menu-mobile').toggleClass('open');
            if($('body').hasClass('submenu-open') && !$('body').hasClass('menu-open')){
                setTimeout(function() {$('body').toggleClass('submenu-open');},200);
            }
        }.bind(this));
    }).on('click','.toggle-whats', function(e){
        $(this).closest('.whats').toggleClass('open');
    }).on('click','.toggle-search', function(e){
        $('body').addClass('animating');
        requestAnimationFrame(function(){
            $(this).closest('.search').toggleClass('open');
            setTimeout(function(){ $('#search').focus();  },100);
        }.bind(this));
    }).on('click','.toggle-user', function(e){
        $('body').addClass('animating');
        requestAnimationFrame(function(){
            $(this).closest('.access').toggleClass('open');
        }.bind(this));
    });

    $('#navigation').on('click','.menu-data button.link', function(e) {
        $('body').addClass('animating');
        requestAnimationFrame(function(){
            $('body').toggleClass('submenu-open');
            $('#navigation .submenu').removeClass('current');
            $(this).closest('.menu-data').find('.submenu').addClass('current');
            setTimeout(function(){
                $('#navigation').stop().animate({ scrollTop: 0 }, 400);
            },300);
        }.bind(this));
    }).on('click', '.submenu-back', function(e) {
        $('body').addClass('animating');
        requestAnimationFrame(function(){
            $('body').toggleClass('submenu-open');
            $('#navigation .submenu').removeClass('current');
        }.bind(this));
    }).on('mouseover',function() {
        $('body').addClass('menu-fix');
    }).on('mouseleave',function() {
        setTimeout(function() { $('body').removeClass('menu-fix'); },600);
    });

    $(document).on('click',function(e){
        if(!$(e.target).closest('.right .whats').length && $('#header .right .whats').hasClass('open'))
            $('#header .right .whats').toggleClass('open');
        if(!$(e.target).closest('.right .search').length && $('#header .right .search').hasClass('open')) {
            requestAnimationFrame(function(){
                $('#header .right .search').toggleClass('open');
            }.bind(this));
        }
        if(!$(e.target).closest('.right .access').length && $('#header .right .access').hasClass('open')) {
            requestAnimationFrame(function(){
                $('#header .right .access').toggleClass('open');
            }.bind(this));
        }
        if(!$(e.target).closest('.toggle-menu').length && !$(e.target).closest('#navigation').length && $('body').hasClass('menu-open')){
            requestAnimationFrame(function(){
                $('body').removeClass('menu-open submenu-open');
                $('#menu-mobile').removeClass('open submenu-open');
            }.bind(this));
        }
    });
};

App.prototype.initFreight = function(){

    if (!$('#zipcode').length || !$('#calculate').length)
        return;

    var self = this;

    $('#zipcode').on('keydown', function(e){
        if (e.keyCode == 13){
            e.preventDefault();
            $('#calculate').click();
        }
    }).mask("99999-999");

    $('#calculate').on('click', function(e){
        e.preventDefault();
        if ($(this).hasClass('loading'))
            return;
        var v = $('#zipcode').val();
        if (v.length == 9){
            var xhr = $(this).data('xhr'),
                quantity = $(this).closest('form').find('input[name="quantity"]');
            if (xhr !== undefined){
                xhr.abort();
            }
            $(this).addClass('sending');
            self.calculateShipping({
                zipcode: v,
                product: $(this).data('product') ? $(this).data('product') : null,
                quantity: quantity.length ? quantity.val() : 1,
                view: $(this).data('checkout') ? 'carrinho/shipping' : 'produtos/shipping',
                bt: this
            });
        }else{
            // app.dialog.open({
            //     class: 'error',
            //     title: 'CEP Inválido',
            //     message: 'Digite um CEP válido.',
            //     callback: function(){
            //         $('#zipcode').focus();
            //     }
            // });
        }
    });
};

App.prototype.calculateShipping = function(params){

    var postData = {zipcode: params.zipcode, quantity: params.quantity, view: params.view};

    if (params.product)
        postData.product = params.product;

    if (typeof carrinho !== 'undefined' || typeof checkout !== 'undefined')
        postData.price = typeof carrinho !== 'undefined' ? carrinho.calculate(false) : checkout.calculate(false);

    if($.trim($('.shipping-result').html()) == '') {
        $('.shipping-result').stop().slideUp();
    }

    $(params.bt).data('xhr', $.post(base_url + 'carrinho/calculate-freight', postData, function(data){
        if(!data.status) {
            // app.dialog.open({
            //     'class': data.class ? data.class : 'error',
            //     'title': data.title ? data.title : 'Ops!!',
            //     'message': data.message,
            //     'callback': function(){
            //         if (data.redirect !== undefined && data.redirect)
            //             window.location.href = data.redirect;
            //     }
            // });
        } else {
            $('.shipping-result').html(data.view).stop().slideDown(function(){
                if (typeof carrinho !== 'undefined'){
                    carrinho.calc_height();
                    if(typeof params.element != 'undefined')
                       params.element.closest('.address-content').removeClass('loading');
                }
            });
            try{
                checkout.calculate();
            }catch(e){}
        }
        $(params.bt).removeData('xhr').removeClass('sending');
    }.bind(params.bt),'json').fail(function() {
        $(params.bt).removeData('xhr').removeClass('sending');
    }.bind(params.bt)));
};

App.prototype.showNotification = function(product){
    var $notification = $('#cart-notification'),
        open = $notification.hasClass('open'),
        title = product.title + (product.variation ? ' <small>('+product.variation.title+')</small>' : '');

    clearTimeout(this.notificationTimer);
    $notification.find('.product-image').data('src',base_url + 'image/resize?w=108&h=102&src='+product.image.replace('/../adsites', '/adsites')).lazyload('reload');
    if (open){
        $notification.find('.product-preview').stop().animate({top: 15, opacity: 0}, 300, function(){
            $notification.find('.product-title').html(title);
            $notification.find('.product-price').html(app.formatReal(product.price));
            $notification.find('.product-quantity').html(product.quantity);
            $(this).stop().animate({top: 0, opacity: 1}, 300);
        });
    }else{
        $notification.find('.product-title').html(title);
        $notification.find('.product-price').html(app.formatReal(product.price));
        $notification.find('.product-quantity').html(product.quantity);
        $notification.addClass('open');
    }
    this.notificationTimer = setTimeout(function(){
        $notification.removeClass('open');
    }, 5000);
};

App.prototype.setForm = function($form, callback){

    var self = this;
    $form.each(function() {

        var $form = $(this);

        $form.on('submit', function(e){
            e.preventDefault();
        }).validate({
            errorPlacement: function(error, el) {
                if(el[0].tagName.toLowerCase() == 'select')
                    $(el).next().addClass('error');
                return false
            },
            submitHandler: function(form){
                self.form_submit($form, callback);
            }
        });

    });
};

App.prototype.form_submit = function($form, callback) {
    if ($form.hasClass('sending'))
        return;
    $form.addClass('sending');

    $.ajax({
        url: $form.attr('action'),
        dataType: 'json',
        type: 'POST',
        data: $form.serialize(),
        success: function (data){
            $form.removeClass('sending');

            if($('.g-recaptcha').length)
                grecaptcha.reset();

            if (data.status && data.track_push){
                fbq('track', data.track_push.fb.action, data.track_push.fb.body);
            }

            if (data.reset) {
                $form[0].reset();
                $('select').each(function(i, el){
                    try{
                        $(el).val('').trigger('change');
                    }catch (e){
                    }
                });
            } else if (data.fail !== undefined){
                $.each(data.fail,function(i, name){
                    $form.find('[name="'+name+'"]').addClass('error');
                });
            }
            if ($form.hasClass('no-message')){
                if (!data.status){
                    // app.dialog.open({
                    //     'class': data.class ? data.class : 'error',
                    //     'title': data.title ? data.title : 'Ops!',
                    //     'message': data.message,
                    //     'callback': function(){
                    //         if (data.redirect !== undefined && data.redirect)
                    //             window.location.href = data.redirect;
                    //     }

                    // });
                }else if (data.redirect !== undefined && data.redirect){
                    $form.addClass('sending');
                    window.location.href = data.redirect;
                }
            }else{
                // app.dialog.open({
                //     'class': data.class ? data.class : 'error',
                //     'title': data.title ? data.title : 'Ops!',
                //     'message': data.message,
                //     'callback': function(){
                //         if (data.redirect !== undefined && data.redirect){
                //             $form.addClass('sending');
                //             window.location.href = data.redirect;
                //         }
                //     }
                // });
            }
            if (typeof callback == 'function') {
                callback(data);
            }
        },
        error: function(){
            $form.removeClass('sending');
            // app.dialog.open({
            //     'class': 'error',
            //     'title': 'Ocorreu um erro',
            //     'message': 'Desculpe, ocorreu um problema inesperado. Tente Novamente.'
            // });
        }
    });
};

App.prototype.configSelects = function($selector){

    $selector = $selector ? $selector : $('select');
    $selector.each(function(index, el) {

        if(!is_mobile) {
            var options = {
                language: "pt-BR",
                minimumResultsForSearch: $(el).hasClass('search-select') ? 0 : -1,
                allowClear: false,
                placeholder: $(el).data('placeholder')
            }
            $(el).select2(options).on('change', function(){
                if ($(this).val() != ''){
                    $(this).removeClass('error');
                }
            });
        }

        if($(el).attr('required')) {
            $(el).on('change', function(){
                if($.trim(this.value) != ''){
                    $(this).removeClass('error').next().removeClass('error');
                }
            });
        }
    });

};

App.prototype.getMoney = function(str){
    return isNaN(str) == false ? parseFloat(str) : parseFloat($.trim(str.replace(/Por/i,"").replace("R$","").trim().replace(".","").replace(",",".")));
};

/**
 * @param  {[type]} n [numero a converter]
 * @param  {[type]} c [numero de casas decimais]
 * @param  {[type]} d [separador decimal]
 * @param  {[type]} t [separador milha]
 */
App.prototype.formatReal = function(n, c, d, t){
    c = isNaN(c = Math.abs(c)) ? 2 : c;
    d = d == undefined ? "," : d;
    t = t == undefined ? "." : t;
    var s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return 'R$' + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}


App.prototype.query_builder = function ($selector) {
    var query = '';
    var fields = Array();
    var data = $selector.serialize().split('&');
    $.each(data,function(i,el){
        var field = el.split('=');
        if (field[1] != ''){
            field[0] = field[0].replace('%5B%5D','');
            if (fields[field[0]] === undefined)
                fields[field[0]] = [];
            fields[field[0]].push(field[1]);
        }
    });
    for (var i in fields){
        query += (query != '' ? '&' : '') + i + '=' + fields[i].join(',');
    }
    return query;
}

App.prototype.productsHover = function () {
    $('.product-data').on('mouseover',function(){
        if(self.overing && self.overing != $(this))
            self.overing.css({'z-index':'2'});

        $(this).css({'z-index':'3'});
        self.overing = $(this);
    });
    $('.product-data').on('mouseleave',function(){
        var $current = $(this);
        setTimeout(function() {
            $current.css({'z-index':'1'});
            self.overing = false;
        },300);
    });
}


App.prototype.initColorbox = function($selector, iframe)
{
    if ($selector === undefined){
        var hammertime = new Hammer(document.getElementsByTagName("BODY")[0]);
        hammertime.on('swipeleft', function(ev) {
            if ($('#cboxOverlay').is(':visible'))
                $.colorbox.next();
        });
        hammertime.on('swiperight', function(ev) {
            if ($('#cboxOverlay').is(':visible'))
                $.colorbox.prev();
        });
        $selector = $('body');
    }
    // Colorbox
    $selector.find('.colorbox').each(function(i,el){
        if($(el).hasClass('iframe')) {
            var color_height = '85%';
            if(is_mobile) {
                if(window.innerWidth >= 768)
                    color_height = '500px';
                else
                    color_height = '300px';
            }
            $(el).colorbox({
                current: false,
                returnFocus: false,
                iframe: true,
                width: (is_mobile ? '90%' : '1250px'),
                height: color_height,
                onLoad: function(){
                    $('#cboxWrapper,#colorbox,#cboxContent,#cboxLoadedContent,.cboxPhoto').css({
                        'height':$('#colorbox .cboxPhoto').height(),
                        'width':$('#colorbox .cboxPhoto').width(),
                        'max-width': '100%',
                    });
                    $('#colorbox').removeClass('responsive iframe');
                },
                onComplete: function() {
                    $('#colorbox').addClass('responsive iframe');
                },
                onClosed: function() {
                   $('#colorbox').removeClass('responsive iframe');
                }
            });
        } else {
            $(el).colorbox({
                current: false,
                maxWidth: '90%',
                maxHeight: '85%',
                returnFocus: false,
                onLoad: function(){
                    $('#cboxWrapper,#colorbox,#cboxContent,#cboxLoadedContent,.cboxPhoto').css({
                        'height':$('#colorbox .cboxPhoto').height(),
                        'width':$('#colorbox .cboxPhoto').width(),
                        'max-width': '100%',
                    });
                    $('#colorbox').removeClass('responsive image');
                },
                onComplete: function() {
                    $('#colorbox').addClass('responsive image');
                },
                onClosed: function() {
                   $('#colorbox').removeClass('responsive image');
                }
            });
        }
        if(!$('#colorbox .loader-container').length)
            $('#colorbox').find('#cboxLoadingGraphic').append('<div class="loader-container"></div>');
    });

};

$(document).ready(function (){
    app = new App();
    app.init();
});


//valida o CPF digitado
function validate_cpf(cpf) {
    var num, key, sum, i, result, equal;
    equal = 1;
    cpf = cpf.replace(/[^\d]+/g,'');
    if (cpf.length < 11) {
        return false;
    }
    for (i = 0; i < cpf.length - 1; i++) {
        if (cpf.charAt(i) != cpf.charAt(i + 1)) {
            equal = 0;
            break;
        }
    }
    if (!equal) {
        num = cpf.substring(0,9);
        key = cpf.substring(9);
        sum = 0;
        for (i = 10; i > 1; i--)
            sum += num.charAt(10 - i) * i;
        result = sum % 11 < 2 ? 0 : 11 - sum % 11;
        if (result != key.charAt(0))
            return false;
        num = cpf.substring(0,10);
        sum = 0;
        for (i = 11; i > 1; i--)
            sum += num.charAt(11 - i) * i;
        result = sum % 11 < 2 ? 0 : 11 - sum % 11;
        if (result != key.charAt(1))
            return false;
        return true;
    }
    else
        return false;
}

//valida o CNPJ digitado
function validate_cnpj(value) {

    var cnpj = value.replace(/[^\d]+/g,'');

    if(cnpj == '') return false;

    if (cnpj.length != 14)
        return false;

    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
        return false;

    // Valida DVs
    var size = cnpj.length - 2,
        numbers = cnpj.substring(0,size),
        keys = cnpj.substring(size),
        sum = 0,
        pos = size - 7;
    for (var i = size; i >= 1; i--) {
      sum += numbers.charAt(size - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    var result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    if (result != keys.charAt(0))
        return false;

    size = size + 1;
    numbers = cnpj.substring(0,size);
    sum = 0;
    pos = size - 7;
    for (i = size; i >= 1; i--) {
      sum += numbers.charAt(size - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    if (result != keys.charAt(1))
          return false;

    return true;

}

function recaptcha_callback() {

    var $form = $('.g-recaptcha').closest('form');

    if($form.valid() && grecaptcha.getResponse() != ""){
        app.form_submit($form);
    }
    $('.g-recaptcha').on('click',function(){
        if(grecaptcha.getResponse() == "")
            return false;

        if($form.valid()){
            app.form_submit($form);
        }
    });

};