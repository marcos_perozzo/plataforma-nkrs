/**
 * @author: Fabio Bachi
 * @copyright: StoreTrooper - 2016
 */

"use strict";

function Common_pages(){ this.init(); };

Common_pages.prototype.init = function() {
    app.initColorbox();
};

$(document).ready(function (){
    new Common_pages();
});