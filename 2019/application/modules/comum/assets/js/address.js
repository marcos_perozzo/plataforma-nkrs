/**
 * @author: Ralf da Rocha
 * @copyright: StoreTrooper - 2016
 */

"use strict";

function Address($context) {
    this.init($context);
};

Address.prototype.init = function($context){

    if (this.initialized)
        return;
    this.initialized = true;
    this.$context = $context;

    this.initZipcode();
    this.initStateCities();
};

Address.prototype.initZipcode = function(){
    var self = this;
    this.$context.on('keyup', '.mask-zipcode', function(){
        var v = $(this).val().replace(/\D/g, ''),
            $zip = $(this);
        if (v.length==8 && v != $zip.data('old')){
            $zip.data('old',v);
            $zip.closest('.form-group').addClass('loading');
            var tab = $zip.closest('.address-box');
            $.post(base_url+'comum/get-cep',{cep: v}, function(data){
                if(data.resultado == 1 || data.resultado == 2){
                    var stateId = '';
                    var opt_selector = tab.find('[name*="state"] option');
                    opt_selector.each(function(i,el) {
                        if($(el).text().substr(0,2) == data.uf){
                            stateId = $(el).attr('value');
                            return false;
                        }
                    });
                    self.autoCity = data.cidade;
                    tab.find('[name*="street"]').val(data.tipo_logradouro+" "+data.logradouro).trigger('blur');
                    tab.find('[name*="state"]').val(stateId).trigger('change');
                    tab.find('[name*="suburb"]').val(data.bairro).trigger('blur');
                    tab.find('[name*="number"]').focus();
                }
                $zip.closest('.form-group').removeClass('loading');
            });
        }
    });
};

Address.prototype.initStateCities = function(){
    var self = this,
        $states = this.$context.find('select[name*="state"]'),
        original_city = this.$context.find('select[name*="city"]').data('value');

    $states.closest('.form-group').addClass('loading');
    $.ajax({
        type : "POST",
        url : base_url + 'comum/get-states',
        dataType : "json",
        success: function(data) {
            var states = '',
                original = $states.data('value');
            for (var i = 0; i < data.length; i++)
                states += '<option value="'+data[i].id+'" '+(original == data[i].uf ? 'selected="selected"' : '')+'>'+data[i].uf+' - '+data[i].name+'</option>';
            if (states != ''){
                $states.each(function(){

                    $(this).append(states);
                    if(!is_mobile) {
                        $(this).select2('destroy');
                        app.configSelects($(this));
                    }

                });
                if (original && original_city){
                    self.autoCity = original_city;
                    $states.trigger('change');
                }
            }
        },
        complete: function () {
            $states.closest('.form-group').removeClass('loading');
        }
    });

    this.$context.on('change', 'select[name*="state"]', function(){
        var $state = $(this),
            $city = $state.closest('.address-box').find('select[name*="city"]'),
            v = $(this).val(),
            selected = false;

        $city.closest('.form-group').addClass('loading');

        $.ajax({
            type : "POST",
            url : base_url + 'comum/get_cities',
            data : { id: v },
            dataType : "json",
            success: function(data) {

                $('#select2-drop-mask').hide();
                if(!is_mobile)
                    $city.select2('destroy');

                $city.children('option:gt(0)').remove();
                var citys = '';
                for (var i = 0; i < data.length; i++) {
                    citys += '<option value="'+data[i].id+'"'+(self.autoCity && self.autoCity == data[i].name ? ' selected="selected"' : '')+'>'+data[i].name+'</option>';
                }
                if (citys != ''){
                    $city.append(citys);
                }
                self.autoCity = false;
                if(!is_mobile) {
                    app.configSelects($city);
                }

            },
            complete: function () {
                $city.closest('.form-group').removeClass('loading');
            }
        });
    });

};