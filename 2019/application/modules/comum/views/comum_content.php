<main id="common-pages">

    <section class="common-header">
        <!-- título e breadcrumbs -->
        <div class="page-data">
            <div class="text">
                <h1 class="title"><?php echo $dataContent->title; ?></h1>
                <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url(); ?>">Página inicial</a></li>
                    <li class="breadcrumb-data"><strong><?php echo $dataContent->title; ?></strong></li>
                </ul>
            </div>
        </div>

    </section>
    <section class="content">
        <div class="common-text">
            <?php echo $dataContent->description; ?>
        </div>
        <div class="common-text">
            <?php if(isset($dataContent->link)){?>
                <a href="<?php echo $dataContent->link; ?>" class="iframe colorbox"><img src="<?php echo $dataContent->image->file; ?>" /></a>
                <br /><br />
                <a href="<?php echo $dataContent->link; ?>" download>Download do Catálogo</a>
                
            <?php }?>
        </div>
    </section>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>