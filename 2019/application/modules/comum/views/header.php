<header class="container mt-4 mb-4">
    <div class="row d-flex align-items-center">
        <div class="col-sm-4">
            <a href="<?= site_url(); ?>" title="Para a homepage" class="header__logo"></a>
        </div>
        <div class="col-sm-4">
            <form class="header__search" method="post" action="<?= site_url(); ?>search" data-search>
                <input type="search" class="form-control" name="search" placeholder="Buscar"/>
                <button class="header__search__button" type="submit"><i class="fas fa-search"></i></button>
            </form>
        </div>
        <div class="col-sm-4">
            <?php if(isset($_SESSION['nkrs']['client'])) { ?>
                <div class="header__auth">
                    <i class="fas fa-user"></i> 
                    <span>Olá, <?=$_SESSION['nkrs']['client']['name']?>!</span>
                    <a href="<?= site_url(); ?>painel" class="btn btn-primary ml-2" title="Acessar sua conta">Minha conta</a>
                    <a href="<?= site_url(); ?>carrinho" class="cart ml-3" title="Carrinho de compras">
                        <i class="fas fa-shopping-cart cart__icon"></i>
                        <span class="badge badge-primary cart__badge" data-badge>3</span>
                    </a>
                </div>
            <?php } else { ?>
                <a href="<?= site_url(); ?>auth/login" class="header__auth" title="Login">
                    <i class="fas fa-user"></i> 
                    <span>Área do Piloto</span>
                </a>
            <?php } ?>
        </div>
    </div>
</header>
<nav class="navbar navbar-expand-lg">
    <button class="navbar__trigger" type="button" data-navbar>
        <i class="fas fa-bars"></i>
    </button>
    <div class="container collapse navbar-collapse">
        <ul class="navbar-nav">
            <li class="nav-item 
                <?php strpos($_module,'home') !== false ? 'active' : ''?>">
                <a href="<?= site_url(); ?>" class="nav-link">Home</a>
            </li>
            <li class="nav-item <?=strpos($_module,'sobre') !== false ? 'active' : ''?>">
                <a href="<?= site_url(); ?>sobre" class="nav-link">Sobre o projeto</a>
            </li>
            <li class="nav-item <?=strpos($_module,'apoiar') !== false ? 'active' : ''?>">
                <a href="<?= site_url(); ?>apoiar" class="nav-link">Quero apoiar o projeto</a>
            </li>
            <li class="nav-item <?=strpos($_module,'inscricoes') !== false ? 'active' : ''?>">
                <a href="<?= site_url(); ?>inscricoes" class="nav-link">Inscrições</a>
            </li>
            <li class="nav-item <?=strpos($_module,'blog') !== false ? 'active' : ''?>">
                <a href="<?= site_url(); ?>blog" class="nav-link">Blog</a>
            </li>
            <li class="nav-item <?=strpos($_module,'loja') !== false ? 'active' : ''?>">
                <a href="<?= site_url(); ?>loja" class="nav-link">Loja</a>
            </li>
        </ul>
    </div>
</nav>