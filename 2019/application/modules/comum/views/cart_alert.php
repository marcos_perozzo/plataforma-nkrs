<aside id="cart-notification">
    <div class="common-limiter">
        <div class="product-side">
            <p class="title">Adicionado em seu carrinho:</p>
            <div class="product-preview">
                <div class="product-image lazyload"></div>
                <div class="product-info">
                    <div class="product-title"></div>
                    <div class="product-price"></div>
                </div>
            </div>
        </div>
        <div class="actions-side">
            <button type="button" class="common-button" id="close-notification"><span>Continuar Comprando</span></button>
            <a href="<?php echo site_url('carrinho'); ?>" class="common-button"><span>Meu Carrinho</span></a>
        </div>
    </div>
</aside>