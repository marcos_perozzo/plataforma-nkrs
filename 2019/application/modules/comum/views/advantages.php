<ul class="advantage-list">
    <li class="advantage-data">
        <span class="icon"><img src="<?php echo site_url('image/resize?w=80&h=80&src='.base_img('produto-nacional.png')); ?>" alt="Visa"></span>
        <span class="desc">Produto 100% nacional</span>
    </li>
    <li class="advantage-data">
        <span class="icon"><?php echo load_svg('credit-card.svg'); ?></span>
        <span class="desc">Parcelamento em até 6x sem juros</span>
    </li>
    <li class="advantage-data shipping">
        <span class="icon"><?php echo load_svg('delivery-truck.svg'); ?></span>
        <span class="desc">Frete Grátis - acima de R$ 350,00 - Sul e Sudeste</span>
    </li>
    <!-- <li class="advantage-data">
        <span class="icon"><?php echo load_svg('secure-shield.svg'); ?></span>
        <span class="desc">Compra 100% Segura</span>
    </li>
    <li class="advantage-data">
        <span class="icon"><?php echo load_svg('barcode.svg'); ?></span>
        <span class="desc">Descontos no Boleto</span>
    </li> -->
</ul>