<div class="loader-container <?php echo isset($class) ? $class : ''; ?>">
    <div class="rectangle-1"></div>
    <div class="rectangle-2"></div>
    <div class="rectangle-3"></div>
    <div class="rectangle-4"></div>
    <div class="rectangle-5"></div>
    <div class="rectangle-6"></div>
    <div class="rectangle-5"></div>
    <div class="rectangle-4"></div>
    <div class="rectangle-3"></div>
    <div class="rectangle-2"></div>
    <div class="rectangle-1"></div>
</div>