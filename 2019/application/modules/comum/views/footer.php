<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="footer-content">
                    <a class="footer-content__logo" href="<?php echo site_url();  ?>">
                        <img class="img-fluid" src="<?php echo base_img('logo-2019-branco.png');  ?>" alt="Logo Moderniza"/>
                    </a>
                    <p class="footer-content__text">O NKRS - Novo Kart RS, tem por finalidade promover a integração e competição entre os amantes do Kartismo amador da Serra Gaúcha, além da formação de pilotos e equipe para competições de Endurance.</p>
                </div>
                <div class="footer-content footer-content--newsletter">
                    <p class="footer-content__text footer-content__text--alt">Assine nossa newsletter e fique dentro das nossas novidades.</p>
                    <form class="form-inline" action="<?php echo site_url('common/newsletter');  ?>" method="POST" data-validate>
                        <div class="form-group">
                            <input type="email" name="email" id="newsEmail" class="form-control" placeholder="Seu e-mail" required />
                        </div>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </form>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="footer-content">
                    <h6 class="footer-content__title">
                        <a href="<?php echo site_url(); ?>" title="Home">Home</a>
                    </h6>
                    <h6 class="footer-content__title">
                        <a href="<?php echo site_url('sobre'); ?>" title="Sobre o Projeto">Sobre o Projeto</a>
                    </h6>
                    <h6 class="footer-content__title">
                        <a href="<?php echo site_url('quero-apoiar'); ?>" title="Quero apoiar o projeto">Quero apoiar o projeto</a>
                    </h6>
                    <h6 class="footer-content__title">
                        <a href="<?php echo site_url('inscricoes'); ?>" title="Inscrições">Inscrições</a>
                    </h6>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="footer-content">
                    <h6 class="footer-content__title">
                        <a href="<?php echo site_url('loja'); ?>" title="Home">Loja</a>
                    </h6>
                    <ul class="footer-content__list">
                        <li class="footer-content__item">
                            <a href="<?php echo site_url('loja'); ?>" title="Merchandising">Merchandising</a>
                        </li>
                        <li class="footer-content__item">
                            <a href="<?php echo site_url('loja'); ?>" title="Macacões">Macacões</a>
                        </li>
                        <li class="footer-content__item">
                            <a href="<?php echo site_url('loja'); ?>" title="Luvas">Luvas</a>
                        </li>
                        <li class="footer-content__item">
                            <a href="<?php echo site_url('loja'); ?>" title="Sapatilhas">Sapatilhas</a>
                        </li>
                        <li class="footer-content__item">
                            <a href="<?php echo site_url('loja'); ?>" title="Proteções">Proteções</a>
                        </li>
                        <li class="footer-content__item">
                            <a href="<?php echo site_url('loja'); ?>" title="Underware">Underware</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="footer-content">
                    <h6 class="footer-content__title">
                        <a href="<?php echo site_url(); ?>blog" title="Conteúdo Moderniza" target="_blank">Blog</a>
                    </h6>
                    <h6 class="footer-content__title">
                        <a href="<?php echo site_url(); ?>contato" title="Contato">Contato</a>
                    </h6>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="footer-bottom">
            <div class="footer-bottom__left">
                <span>Copyright 2019 - Novo Kart RS - NKRS Competições. Todos os direitos reservados.</span>
            </div>
            <div class="footer-bottom__right">
                <span class="footer-bottom__phone"><i class="fas fa-phone"></i> (54) 9.9943.9698</span>
                <a href="https://api.whatsapp.com/send?phone=555499439698" target="_blank"><i class="fab fa-whatsapp"></i></a>
                <a href="https://www.facebook.com/novokartrs/" target="_blank"><i class="fab fa-facebook-f"></i></a>
            </div>
        </div>
    </div>
</footer>
