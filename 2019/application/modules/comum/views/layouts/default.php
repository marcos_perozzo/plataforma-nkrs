<!DOCTYPE html>
<html lang="pt-br" <?php echo $_is_mobile ? 'class="mobile"' : 'class="desktop"'; ?>>
    <head>
        <base href="<?php echo base_url(); ?>" >
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="cleartype" content="on">
        <meta name="theme-color" content="#222222" />
        <meta name="msapplication-TileColor" content="#222222">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="format-detection" content="telephone=no">

        <title><?php echo $title; ?></title>
        <?php echo $metadata; ?>
        <meta name="author" content="AdSites">
        <meta name="robots" content="index, follow">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_img('icon/favicon.ico'); ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_img('icon/apple-touch-icon.png'); ?>">
        <link rel="mask-icon" href="<?php echo base_img('icon/safari-pinned-tab.svg'); ?>" color="#d52027">

        <?php echo $head_styles; ?>
        <?php echo $head_scripts; ?>
    </head>
    <body class="preload">
        <section id="wrapper">
            <?php echo $header; ?>
            <section id="content">
                <?php echo $breadcrumb, $content, $footer; ?>
            </section>
            <?php echo $cart_alert; ?>
        </section>

        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>',
                base_img = '<?php echo base_img(); ?>',
                base_js = '<?php echo base_js(); ?>',
                segments = '<?php echo trim($this->uri->uri_string(),"/"); ?>'.split('/'),
                is_mobile = <?php echo $_is_mobile ? 'true' : 'false'; ?>;
        </script>
        <?php echo $body_scripts; ?>
        <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId: '',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v2.11'
            });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', ''); 
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
    </body>
</html>
