<section id="search-page">
    <div class="common-top">
        <div class="content">
            <div class="call">Busca</div>
            <h1 class="title">Sua busca retornou</h1>
            <div class="results"><?php echo $total ?> resultado<?php echo $total != 1 ? 's' : ''; ?></div>
        </div>
    </div>
    <div class="content">
        <?php   if (!empty($products)){ ?>
        <div class="subtitle">Produtos</div>
        <ul class="products-list">
            <?php foreach($products as $product) { ?>
                <li class="product-data">
                    <a href="<?php echo site_url('produtos/'.$product->slug); ?>" class="content">
                        <?php
                        $options = array(
                            'src'           => site_url('image/resize_canvas?w=248&h=248&bg=1&src='.fix_image_resize($product->image)),
                            'alt'           => $product->title,
                            'class'         => 'lazyload',
                            'data-viewport' => 1,
                            'tag'           => 'figure'
                        );
                        echo lazyload($options);
                        ?>
                        <h3 class="title"><?php echo $product->title; ?> </h3>
                        <?php if(($product->has_variation == '0' && $product->inventory >= 1)) { ?>
                            <div class="price">
                                <strong class="current">R$ <?php echo mysql_decimal_to_number($product->price); ?></strong>
                            </div>
                            <?php if($product->original_price != $product->price){
                                    $off = round(100 - ((float)$product->price * 100 / (float)$product->original_price));
                                ?>
                                <div class="offer">
                                    <?php echo load_svg('arrow-down.svg'); ?>
                                    <span class="save">Promo <?php echo $off; ?>%</span>
                                </div>
                            <?php } ?>
                            <div class="btn-cart">
                                <strong class="current">Ver Detalhes</strong>
                            </div>
                        <?php } else if ($product->has_variation == '1'){ ?>
                            <div class="btn-cart">
                                <strong class="current">Veja mais</strong>
                            </div>
                        <?php } else { ?>
                            <div class="no-stock">
                                <p>Esgotado</p>
                                <button data-href="<?php echo site_url('contato'); ?>" class="common-button"><span>Solicitar Orçamento</span></button>
                            </div>
                        <?php } ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
        <?php   }
                echo $pagination;
        ?>
    </div>
</section>