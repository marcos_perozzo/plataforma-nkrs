<?php
class Busca extends MY_Controller
{
    function __construct(){
        parent::__construct();
    }

    public function index()
    {

        $max = 12;
        $search = $this->input->get('search');
        $page = $this->input->get('page');
        $start = $page ? ((int) $page - 1) * $max : 0;

        $products_request = array(
            'limit' => $max,
            'offset' => $start,
            'search' => $search,
            'order' => 'lang.title ASC'
        );
        $products = $this->st->products->get($products_request);

        $products_request['count'] = TRUE;
        $totalP = $this->st->products->get($products_request);

        $pagination = $this->_initPagination(array(
            'url' => site_url('busca'),
            'total' => $totalP,
            'max' => $max
        ));

        $this->template
             ->add_css('css/busca')
             ->set('products', $products)
             ->set('total', $totalP)
             ->set('pagination', $pagination)
             ->set('title', 'Utimil - Realizar Busca')
             ->build('busca');
    }
}