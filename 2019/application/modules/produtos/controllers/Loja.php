<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loja extends MY_Controller
{

    public function index($c1 = FALSE)
    {
        $categories = array();
        $category_father = null;

        list($category, $route) = $this->_get_current_category($c1, $c2, $c3);
        $search = $this->input->get('q');
        $filter_array = $this->input->get();
        if(isset($filter_array['pg'])) {
            $pg = $filter_array['pg'];
            unset($filter_array['pg']);
        } else {
            $pg = 1;
        }

        $limit = 12;
        $offset = ($pg - 1) * $limit;
        $order_by = 'main.price DESC';

        if ($category){

            if(isset($c2)){
                $category_father = $this->st->categories->get(array('slug' => $c1));
            }

            //separa os dados do get que possuirem vírgulas
            if(isset($filter_array['order'])){
                $order_by = $filter_array['order'];
                unset($filter_array['order']);
            }
            foreach ($filter_array as $key => $value) {

                if($key != 'de' && $key != 'ate')
                    $filter_array[$key] = explode(',',$value);
                else
                    $filter_array[$key] = $value;
            }

            $title = $this->site_title.' - '.$category->title;
            $categories[$c1] = $this->categories->{$c1};
            $products = $this->st->products->get(array(
                'category' => $category->slug,
                'search' => $search,
                'filters' => $filter_array,
                'offset' => $offset,
                'limit' => $limit,
                'order' => $order_by,
                'getFilters' => TRUE
            ));
            $total = $this->st->products->get(array('category' => $category->slug, 'count' => TRUE, 'search' => $search, 'filters' => $filter_array));
        }else{
            $title = $this->site_title;
            $products = $this->st->products->get(array(
                'products' => TRUE,
                'search' => $search,
                'offset' => $offset,
                'limit' => $limit,
                'order' => $order_by,
                'getFilters' => TRUE
            ));
            $total = $this->st->products->get(array('products' => TRUE, 'count' => TRUE, 'search' => $search));
        }


        $segment = 2;
        $c1 ? $segment++ : $segment;
        $c2 ? $segment++ : $segment;

        $base_url = 'produtos'.($c1 ? '/'.$c1 : '').($c2 ? '/'.$c2 : '');
        $pagination = $this->_initPagination(array(
            'segment'   => $segment,
            'total'     => $total,
            'url'       => $base_url,
            'max'       => $limit
        ));

        $filters = array();

        //carregar somente na requisição ajax
        if($this->input->is_ajax_request()) {
            $products = $products->data;
            $this->load->view('ajax-content', array(
                '_is_mobile' => $this->isMobile,
                'pagination' => $pagination,
                'total'      => $total,
                'products'   => $products,
            ));
        } else {
            $this->load->helper('url');
            $get_filters = $this->st->products->get( array('lang' => 1, 'getFilters' => TRUE, 'category' => ($category ? $category->slug : '') ));
            if( isset($get_filters->filters) ){
                foreach ($get_filters->filters as $key => $filter) {
                    $values = array();
                    foreach ($filter->values as $k => $v) {
                        $values[] = $v->value;
                    }
                    $filters[] = array(
                        'id' => $filter->id,
                        'title' => $filter->title,
                        'values' => $values,
                    );
                }
            } 
            $products = $products->data;            

            $this->template
                 ->add_css('css/produtos')
                 ->add_js('js/produtos')
                 ->set('route', $route)
                 ->set('products', $products)
                 ->set('title', 'Utimil - '.$title)
                 ->set('title', $title)
                 ->set('search', $search)
                 ->set('filters', $filters)
                 ->set('order_by', $order_by)
                 ->set('pagination', $pagination)
                 ->set('category', $category)
                 ->set('category_father', $category_father)
                 ->set('filter_array', $filter_array)
                 ->set('total', $total)
                 ->build('produtos');
        }

    }

    public function detalhes($product = FALSE)
    {
        if (!$product)
            show_404();

        $categories = explode('/', $product->slug);
        array_pop($categories);

        list($category, $route) = $this->_get_current_category(
            $categories[0],
            isset($categories[1]) ? $categories[1] : FALSE,
            isset($categories[2]) ? $categories[2] : FALSE
        );

        $product->related = $this->st->products->get(array(
            'related' => $product->id,
            'category' => implode('/',$categories)
        ));

        $measure = $this->st->content->get(array(
            'content' => 'measure'
        ));
        if ($measure && !empty($measure)){
            $measure = isset($measure[0]->image->file) ? $measure[0]->image->file : FALSE;
        }

        $this->load->helper('text');
        $title = $product->title;
        $image = isset($product->gallery[0]->path) ? $product->gallery[0]->path : null;

        if (!empty($image)){
            $this->template
                 ->add_metadata('image', site_url('image/resize_canvas?w=600&h=315&src='.$image))
                 ->add_metadata('og:image', site_url('image/resize_canvas?w=600&h=315&src='.$image),'property')
                 ->add_metadata('twitter:image', site_url('image/resize_canvas?w=600&h=315&src='.$image));
        }else{
            $this->template
                 ->add_metadata('image', site_url('image/resize_canvas?w=600&h=315&src=application/modules/comum/assets/img/logo-color.png'))
                 ->add_metadata('og:image', site_url('image/resize_canvas?w=600&h=315&bg=2&src=application/modules/comum/assets/img/logo-color.png'),'property')
                 ->add_metadata('twitter:image', site_url('image/resize_canvas?w=600&h=315&src=application/modules/comum/assets/img/logo-color.png'));
        }

        $description = !empty($product->description) ? $product->description : null;

        if($description) {
            $this->template
                 ->add_metadata('description', html_entity_decode(character_limiter(strip_tags($description), 200)))
                 ->add_metadata('og:description', html_entity_decode(character_limiter(strip_tags($description), 200)), 'property')
                 ->add_metadata('twitter:description', html_entity_decode(character_limiter(strip_tags($description), 200)));
        }

        $gallery = array();
        foreach ($product->gallery as $gal) {
            $gallery[] = $gal->path;
        }

        $json_ld = array(
            "@context" => "http://schema.org/",
            "@type" => "Product",
            "name" => $title,
            "image" => $gallery,
            "description" => $description,
            "mpn" => $product->ncm,
            "offers" => array(
                "@type" => "Offer",
                "priceCurrency" => "BRL",
                "price" => $product->price,
                "priceValidUntil" => date('Y-m-d'),
                "itemCondition" => "http://schema.org/NewCondition",
                "availability" => "http://schema.org/InStock",
                "seller" => array(
                    "@type" => "Organization",
                    "name" => "Utimil"
                )
            )
        );

        //simulação de parcelamento do produto
        $price = (float) $product->price;
        $installments = array();
        for ($i=1; $i <= 12; $i++) {
            $installments[$i] = (object) array(
                'installments' => $i,
                'price'        => number_format($price/$i,2,',','.'),
                'rate'         => 'sem juros',
                'total'        => number_format($price,2,',','.'),
            );
        }

        $category_father = $this->st->categories->get(array('slug' => $categories[0]));

        $this->load->library('user_agent');

        $back_url = $this->agent->referrer() && strpos($this->agent->referrer(), site_url('produtos/'.$category->slug)) !== -1 ? TRUE : FALSE;

        $this->template
             ->add_metadata('og:locale', 'pt_BR', 'property')
             ->add_metadata('og:url', site_url($this->uri->uri_string()), 'property')
             ->add_metadata('og:title', $title, 'property')
             ->add_metadata('og:site_name', $this->site_title, 'property')
             ->add_metadata('og:type', 'product.item', 'property')
             ->add_metadata('og:image:width', '600', 'property')
             ->add_metadata('og:image:height', '315', 'property')

             //->add_metadata('twitter:card', 'summary')
             //->add_metadata('twitter:site', '@hotmusic')
             //->add_metadata('twitter:creator', '@hotmusic')
             //->add_metadata('twitter:title', $title)

             // ->add_metadata('product:price:currency', $product->price,'property')
             // ->add_metadata('fb:app_id', '302184056577324')
             // ->add_metadata('product:retailer_item_id', 'Sample Retailer Product Item ID','property')
             // ->add_metadata('product:price:amount', 'Sample Amount','property')
             // ->add_metadata('product:shipping_cost:amount', 'Sample Shipping Cost','property')
             // ->add_metadata('product:shipping_cost:currency', 'Sample Shipping Cost: ','property')
             // ->add_metadata('product:shipping_weight:value', 'Sample Shipping Weight: Value','property')
             // ->add_metadata('product:shipping_weight:units', 'Sample Shipping Weight: Units','property')
             // ->add_metadata('product:sale_price:amount', 'Sample Sale Price: ','property')
             // ->add_metadata('product:sale_price:currency', 'Sample Sale Price: ','property')
             // ->add_metadata('product:sale_price_dates:start', 'Sample Sale Price Dates: Start','property')
             // ->add_metadata('product:sale_price_dates:end', 'Sample Sale Price Dates: End','property')
             // ->add_metadata('product:availability', 'Sample Availability','property')
             // ->add_metadata('product:condition', 'Sample Condition','property')

             //->add_metadata('canonical', site_url($this->langLinks['imoveis'].$this->uri->segment(2).'/detalhes/'.$slug), 'link')

             ->add_css('css/detalhes')
             ->add_js('js/detalhes')
             ->add_json_ld($json_ld)
             ->set_partial('cart_alert', 'cart_alert', 'comum')
             ->set('title', $title.' - '.$this->site_title)
             ->set('product', $product)
             ->set('product_details', TRUE)
             ->set('back_url', $back_url)
             ->set('category', $category)
             ->set('category_father', $category_father)
             ->set('installments', $installments)
             ->set('debt_account', $this->st->content->get(array(
                'content' => 'debito-em-conta',
                'limit' => 1
             )))
             ->build('detalhes');
    }

    public function valid_slug($slug = FALSE)
    {
        $newSlug = implode('/', $slug);
        $item = $this->st->products->get( array('slug' => $newSlug, 'details' => TRUE) );
        if ((isset($item) && !empty($item)) || end($slug) == 'produto')
            $this->detalhes($item);
        else
            $this->index(
                isset($slug[0]) ? $slug[0] : FALSE,
                isset($slug[1]) ? $slug[1] : FALSE,
                isset($slug[2]) ? $slug[2] : FALSE
            );

    }

    private function _get_current_category($c1 = FALSE, $c2 = FALSE, $c3 = FALSE)
    {
        if(!isset($this->categories)) {
            $this->categories = $this->st->categories->get(array(
                'recursive' => TRUE,
                'search' => array('id_parent' => ''),
                'key_slug' => TRUE
            ));
        }
        $route = array('produtos' => 'Produtos');
        $category = FALSE;
        if ($c3){
            if (!isset($this->categories->{$c1}->children->{$c1.'/'.$c2}->children->{$c1.'/'.$c2.'/'.$c3}))
                show_404();
            $route['produtos/'.$c1] = $this->categories->{$c1}->title;
            $route['produtos/'.$c1.'/'.$c2] = $this->categories->{$c1}->children->{$c1.'/'.$c2}->title;
            $route['produtos/'.$c1.'/'.$c2.'/'.$c3] = $this->categories->{$c1}->children->{$c1.'/'.$c2}->children->{$c1.'/'.$c2.'/'.$c3}->title;
            $category = $this->categories->{$c1}->children->{$c1.'/'.$c2}->children->{$c1.'/'.$c2.'/'.$c3};
        }else if ($c2){
            if (!isset($this->categories->{$c1}->children->{$c1.'/'.$c2}))
                show_404();
            $route['produtos/'.$c1] = $this->categories->{$c1}->title;
            $route['produtos/'.$c1.'/'.$c2] = $this->categories->{$c1}->children->{$c1.'/'.$c2}->title;
            $category = $this->categories->{$c1}->children->{$c1.'/'.$c2};
        }else if ($c1){
            if (!isset($this->categories->{$c1}))
                show_404();
            $route['produtos/'.$c1] = $this->categories->{$c1}->title;
            $category = $this->categories->{$c1};
        }
        return array($category, $route);
    }

    public function rating()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $post = $this->input->post();

        if(!$this->client){
            $response = array(
                'status'    => FALSE,
                'class'     => 'alert',
                'title'     => 'Atenção!',
                'message'   => 'Você precisa efetuar login para prosseguir!',
                'redirect'  => site_url('login')
            );
        }else{
            // Validação
            $this->load->library('form_validation');
            $this->form_validation->set_rules('rating', 'Pontuação', 'trim|required');
            $this->form_validation->set_rules('product', 'Produto', 'trim|required');

            if ($this->form_validation->run()) {
                $client = $this->st->client->get(TRUE);

                $data = array(
                    'id_product' => $post['product'],
                    'id_client' => $client->id,
                    'name' => $client->name.' '.$client->last_name,
                    'comment' => $post['comment'],
                    'rating' => $post['rating'],
                    'status' => 0,
                );

                $this->st->products->save_review($data);

                $response = array(
                    'status'=> TRUE,
                    'class' => 'success',
                    'title' => 'Sucesso!',
                    'message' => 'Sua avaliação foi enviada com sucesso!',
                    'reset' => TRUE
                );
            } else {
                $error = $this->form_validation->error_array();
                $response = array(
                    'status'=> FALSE,
                    'class' => 'error',
                    'title' => 'Ocorreu um erro!',
                    'message' => (count($error) > 1 ? 'Preencha os campos corretamente!' : array_shift($error)),
                    'fail' => $this->form_validation->error_array(TRUE)
                );
            }
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

    public function variation()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        $post = $this->input->post();

        $product = $this->st->products->get_variation(array('id_product' => $post['id_product'] ,'id_variation' => $post['id_variation']));
        if(!empty($post['id_variation']) && !empty($post['id_product'])) {
            $response = array(
                'status' => TRUE,
                'price'  => mysql_decimal_to_number($product->data->price),
                'original_price'  => mysql_decimal_to_number($product->data->original_price),
            );
        } else {
            $response = array(
                'status'=> FALSE,
                'class' => 'error',
                'title' => 'Ocorreu um erro!',
                'message' => 'O variação não foi encontrada!',
            );
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

    public function _remap($method, $params = array())
    {
        if (method_exists($this, $method))
            return call_user_func_array(array($this, $method), $params);
        else{
            $segments = $params;
            array_unshift($segments, $method);
            $segments = array_map(array($this, 'fix_slug'), $segments);
            $count = count($segments);
            if ($count <= 1){
                call_user_func_array(array($this, 'index'), $segments);
            }else if ($count >= 2){
                call_user_func_array(array($this, 'valid_slug'), array($segments));
            }else{
                $this->show_404();
            }
        }
    }

}
