/**
 * @author: Ralf da Rocha
 * @copyright: StoreTrooper - 2016
 */

"use strict";

var produtos;

function Produtos(){
    this.request = false;
    return this;
};

Produtos.prototype.init = function() {

    var self = this;
    this.filters_mobile();

    //controle do Ver Mais nos filtros
    $('#products').on('click', '#filter-form .see-more', function(){
        $(this).closest('.filter-dup').addClass('open');

    });
    $('#filter-form .filter-dup').each(function(i, el) {
        if($(el).find('.common-checkbox').length <= 3)
            $(el).addClass('open');
    });

    //envia dados dos filtros, ordenação e paginação para a função ajax
    $('#products').on('change', '#order', function(e){
        self.submit();
    });
    $('#products').on('submit', '#filter-form', function(e){
        e.preventDefault();
        $('body').removeClass('filter-open');
        self.submit();
    });
    $('#products').on('submit', '#pagination-form', function(e){
        e.preventDefault();
        self.submit($('#pagination-form').find('input[name="pg"]').val());
    });

    //prevent paginação
    $('#products').on('click', '.common-pagination .pagination-data a',function(e) {
        e.preventDefault();
        self.submit(false, $(this).attr('href'));
    });

    //prevent paginação
    $('#products').on('click', '.product-data a',function(e) {
        if($(e.target).closest('.common-button').length) {
            e.preventDefault();
            window.location.href = $(this).find('.common-button').data('href');
        }
    });

};

//monta a url a ser enviada com todos os filtros
Produtos.prototype.submit = function(pg, url) {
    var self = this;
    pg = typeof pg == 'undefined' ? false : pg;
    url = typeof url == 'undefined' ? false : url;

    if(!url) {
        url = app.query_builder($('#order-form, #filter-form'));
        url = $('#products').data('url') +  (url ? '?' + url : '');
        pg = pg ? 'pg='+pg : '';
        url = url + (url.match(/\?/i) ? '&' : '?') + pg;
    }
    self.navigateTo($.trim(url), $('#ajax-content')[0]);
};

//busca a listagem de produtos e a paginação atualizada
Produtos.prototype.navigateTo = function(url, ctx){
    var self = this;

    // Cancela requisição em andamento
    if (this.request !== false)
        this.request.abort();

    var $scroll = $('html,body');
    $scroll.animate({scrollTop: 0 },800).promise().then(function(){
        $(window).off('scroll.stopScroll mousedown.stopScroll wheel.stopScroll DOMMouseScroll.stopScroll mousewheel.stopScroll keyup.stopScroll touchmove.stopScroll');
    });

    this.request = $.ajax({
        method: 'GET',
        url: url,
        dataType: 'html',
        context: ctx,
        complete: function(data){
            self.request = false;
            if (data.status == 200){
                // atualiza html
                $(this).html(data.responseText);
                // muda histórico
                app.history.pushHistory({
                    title: document.title,
                    url: url.replace(base_url,'')
                });
                // carrega as novas imagens
                $('.lazyload').lazyload();
            }
            $(this).removeClass('loading');
        },
        beforeSend: function(){
            $(this).addClass('loading');
        }
    });
};

Produtos.prototype.filters_mobile = function() {

    // toggle da ordenação
    $('#products').on('click', '#btn-order', function () {
        $('.order-list').toggleClass('open');
    });

    // esconde a ordenação quando toca fora
    $(document).on('click',function(e){
        if(!$(e.target).closest('#btn-order').length && $('#products .order-list').hasClass('open'))
            $('#products .order-list').toggleClass('open');
    });

    // trigger da ordenação no mesmo select do desktop
    $('#products').on('click', '.order-data', function () {
        $('#order').val($(this).data('value')).trigger('change');
    });

    // toggle da filtros
    $('#products').on('click', '#btn-filters', function () {
        $('section.filter').height(window.innerHeight).css({ top: $('#wrapper').scrollTop() });
        $('body').addClass('filter-open');
    });

    //accordeon filtros mobile
    $('#filter-form .filter-title').on('click',function(){
        if(!$(this).closest('.filter-dup').hasClass('mob-open')) {
            $('#filter-form .filter-dup.mob-open').removeClass('mob-open').find('.filters-list').stop().slideUp();
            $(this).closest('.filter-dup').addClass('mob-open').find('.filters-list').stop().slideDown();
        } else {
            $(this).closest('.filter-dup').removeClass('mob-open').find('.filters-list').stop().slideUp();
        }
    });

    //fecha os filtros no mobile
    $('#back-filter').on('click',function(){
        $('body').removeClass('filter-open');
    });

    //recalcula a altura dos filtros no mobile
    $(window).on('resize',function(){
        if($('body').hasClass('filter-open')) {
            $('section.filter').height(window.innerHeight);
        }
    });

};

$(document).ready(function (){
    produtos = new Produtos();
    produtos.init();
});