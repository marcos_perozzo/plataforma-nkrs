/**
 * @author: Ralf da Rocha
 * @copyright: StoreTrooper - 2016
 */

"use strict";

var detalhes;

function Detalhes(){ return this; };

Detalhes.prototype.init = function() {
    this.buy();
    this.gallery();
    this.rating();
    this.variation();

    app.initColorbox();

    if($('#show-reviews').length) {
        $('#show-reviews').on('click', function() {
            $('html,body').stop().animate({ scrollTop: $('#reviews').offset().top - $('#header').height() },800);
        });
    }

    //parcelas
    $('#btn-installments').on('click',function(){
        $('body').addClass('installments-open');
    });
    $('#payment-conditions').find('.common-button, .overlay').on('click',function(){
        $('body').removeClass('installments-open');
    });

    //debito em conta
    $('#btn-debt-account').on('click',function(){
        $('body').addClass('debt-account-open');
    });
    $('#debt-account-modal').find('.common-button, .overlay').on('click',function(){
        $('body').removeClass('debt-account-open');
    });

    $('.history-back').on('click', function(e){
        e.preventDefault();
        window.history.back();
    });

};

Detalhes.prototype.buy = function() {
    // Comprar
    var $form = $('#cart-add'),
        $selects = $form.find('select');

    $form.validate({
        errorPlacement: function(error, el) {
            if(el[0].tagName.toLowerCase() == 'select')
                $(el).next().addClass('error');
            return false;
        },
        submitHandler: function(form){
            if ($form.hasClass('sending'))
                return;
            $form.removeClass('error success').addClass('sending');
            $.ajax({
                url: $form.attr('action'),
                dataType: 'json',
                type: 'POST',
                data: $form.serialize(),
                success: function (data){
                    $form.removeClass('sending');
                    if (data.status){

                        //adicionar o produto no contador do cabeçalho
                        var $header_cart = $('#cart-quantity');
                        $header_cart.closest('.cart-link').addClass('active');
                        $header_cart.text(data.total);

                        app.showNotification(data.product);

                        fbq('track', 'AddToCart', {
                            'value': parseFloat(data.product.price),
                            'currency':'BRL',
                            'content_name': data.product.title
                        });

                        ga('ec:addProduct', {
                            'id': data.product.id,
                            'name': data.product.title,
                            'price': parseFloat(data.product.price),
                            'quantity': data.quantity
                        });
                        ga("ec:setAction", "add");
                        ga("send", "event", "Detail View", "click", "addToCart");

                    }else{
                        app.dialog.open({
                            class: 'alert',
                            title: 'Atenção',
                            message: data.message
                        });
                    }
                },
                error: function(){
                    $form.removeClass('sending');
                }
            });
        }
    });
    $('#quantity').rules( "add", {
        min: 1
    });
};

Detalhes.prototype.gallery = function() {

    // galeria principal
    var main_gallery = 1;
    $('.main-gallery .gallery-list').slick({
        slidesToShow: main_gallery,
        slidesToScroll: main_gallery,
        arrows: false,
        fade: true,
        asNavFor: '.small-gallery .gallery-list',
        responsive: [
            {
                breakpoint: 960,
                settings: {
                    dots: true,
                    appendDots: $('.main-gallery .common-dots'),
                }
            },
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        for (var i = 0; i < main_gallery; i++) {
            var selector = $('.main-gallery .gallery-data:eq('+(nextSlide+i)+') .lazyload');
            if(!selector.hasClass('loaded'))
                selector.lazyload('load');
        }
    });
    $('.main-gallery .gallery-data .lazyload').lazyload();
    $('.main-gallery .gallery-data.slick-active .lazyload').lazyload('load');

    //navegação do menu principal
    var small_gallery = 6;
    $('.small-gallery .gallery-list').slick({
        slidesToShow: small_gallery,
        slidesToScroll: 1,
        asNavFor: '.main-gallery .gallery-list',
        dots: false,
        speed: 300,
        arrows: true,
        focusOnSelect: true,
        infinite: false,
        prevArrow: $('.small-gallery .prev'),
        nextArrow: $('.small-gallery .next'),
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: small_gallery-1,
                    slidesToScroll: small_gallery-1
                }
            },
            {
                breakpoint: 1220,
                settings: {
                    slidesToShow: small_gallery-2,
                    slidesToScroll: small_gallery-2
                }
            },
            {
                breakpoint: 1050,
                dots: true,
                settings: {
                    slidesToShow: small_gallery-3,
                    slidesToScroll: small_gallery-3
                }
            },
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        for (var i = 0; i < small_gallery; i++) {
            var selector = $('.small-gallery .gallery-data:eq('+(nextSlide+i)+') .lazyload');
            if(!selector.hasClass('loaded'))
                selector.lazyload('load');
        }
    });
    $('.small-gallery .gallery-data .lazyload').lazyload();
    $('.small-gallery .gallery-data.slick-active .lazyload').lazyload('load');

    // galeria de vídeos
    var video_gallery = 1;
    $('.video-gallery .gallery-list').slick({
        slidesToShow: video_gallery,
        slidesToScroll: video_gallery,
        arrows: true,
        infinite: false,
        prevArrow: $('.video-gallery .prev'),
        nextArrow: $('.video-gallery .next'),
        responsive: [
            {
                breakpoint: 1150,
                settings: {
                    dots: true,
                    appendDots: $('.video-gallery .common-dots'),
                }
            },
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        for (var i = 0; i < video_gallery; i++) {
            var selector = $('.video-gallery .gallery-data:eq('+(nextSlide+i)+') .lazyload');
            if(!selector.hasClass('loaded')) {
                selector.lazyload('load');
            }
        }
    });
    $('.video-gallery .gallery-data .lazyload').lazyload();
    $('.video-gallery .gallery-data.slick-active .lazyload').lazyload('load');

    // galeria de produtos relacionados
    var products_gallery = 5;
    $('.related-gallery .products-list').slick({
        slidesToShow: products_gallery,
        slidesToScroll: 1,
        dots: false,
        speed: 300,
        arrows: true,
        focusOnSelect: true,
        infinite: false,
        prevArrow: $('.related-gallery .prev'),
        nextArrow: $('.related-gallery .next'),
        responsive: [
            {
                breakpoint: 1800,
                settings: {
                    slidesToShow: products_gallery-1,
                    slidesToScroll: products_gallery-1
                }
            },
            {
                breakpoint: 1550,
                settings: {
                    slidesToShow: products_gallery-2,
                    slidesToScroll: products_gallery-2
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: products_gallery-1,
                    slidesToScroll: products_gallery-1
                }
            },
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: products_gallery-2,
                    slidesToScroll: products_gallery-2
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: products_gallery-3,
                    slidesToScroll: products_gallery-3
                }
            },
            {
                breakpoint: 960,
                settings: {
                    dots: true,
                    appendDots: $('.related-gallery .common-dots'),
                    slidesToShow: products_gallery-3,
                    slidesToScroll: products_gallery-3
                }
            },
            {
                breakpoint: 650,
                settings: {
                    dots: true,
                    appendDots: $('.related-gallery .common-dots'),
                    slidesToShow: products_gallery-4,
                    slidesToScroll: products_gallery-4
                }
            },
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        for (var i = 0; i < products_gallery; i++) {
            var selector = $('.related-gallery .product-data:eq('+(nextSlide+i)+') .lazyload');
            if(!selector.hasClass('loaded'))
                selector.lazyload('load');
        }
    });
    $('.related-gallery .product-data .lazyload').lazyload();
    $('.related-gallery .product-data.slick-active .lazyload').lazyload('load');

    // galeria de reviews
    $(window).on('resize',function(){
        if(window.innerWidth <= 768 && $('#reviews .slick-list').length === 0) {
            $('#reviews .review-list').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 300,
                arrows: false,
                infinite: false,
                dots: true,
                appendDots: $('#reviews .common-dots')
            }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
                var selector = $('#reviews .review-data:eq('+nextSlide+') .lazyload');
                if(!selector.hasClass('loaded'))
                    selector.lazyload('load');
            });
            $('#reviews .review-data .lazyload').lazyload();
            $('#reviews .review-data.slick-active .lazyload').lazyload('load');
        } else if($('#reviews .slick-list').length) {
            $('#reviews .review-list').slick('unslick');
        }
    }).resize();

};

Detalhes.prototype.rating = function() {

    $('#rating-form input[name="rating"]').on('change', function(){
        var score = $(this).val();
        $('#rating-form .star').removeClass('full');
        for (var i = 0; i < score; i++) {
            $('#rating-form .star:eq('+i+')').addClass('full');
        }
    });

};

Detalhes.prototype.variation = function() {

    $('#variation').on('change', function(e) {
        $('#variation').closest('.form-group').addClass('loading');
        $.ajax({
            url: base_url + 'produtos/variation',
            dataType: 'json',
            type: 'POST',
            data: { id_variation: $('#variation').val(), id_product: $('#variation').data('product') },
            success: function (data){
                if (data.status){
                    if(data.original_price > data.price){
                        $('#product-price-old').html(data.original_price);
                    }
                    $('#product-price').html(data.price);
                }else{
                    app.dialog.open({
                        class: 'alert',
                        title: 'Atenção',
                        message: data.message
                    });
                }
            },
            complete: function(){
                $('#variation').closest('.form-group').removeClass('loading');
            }
        });
    });

};

$(document).ready(function (){
    detalhes = new Detalhes();
    detalhes.init();
});