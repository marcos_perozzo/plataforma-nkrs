<?php if($total > 0) {
    $this->load->view('listagem', array('products' => $products));
    echo $pagination;
} else { ?>
<div class="common-empty">Nenhum produto foi encontrado.</div>
<?php } ?>
<div class="loader"><?php $this->load->view('comum/preloader'); ?></div>