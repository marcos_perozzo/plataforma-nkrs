<main id="product-details">

    <section class="common-header desktop-only">
        <div class="page-data">
            <div class="icon desktop-only"><img src="<?php echo site_url('image/resize?w=35&h=35&src='.fix_image_resize($category_father->image)); ?>" alt="<?php echo $product->title; ?>"></div>
            <div class="text">
                <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url('produtos/'.$category_father->slug) ?>"><?php echo $category_father->title; ?></a></li>
                    <li class="breadcrumb-data"><a href="<?php echo site_url('produtos/'.$category->slug) ?>"><?php echo $category->title; ?></a></li>
                    <li class="breadcrumb-data"><strong><?php echo $product->title; ?></strong></li>
                </ul>
            </div>
        </div>
    </section>

    <article>
        <div class="product-info">
            <section class="gallery left">

                <?php if($product->original_price != $product->price){
                    $off = round(100 - ((float)$product->price * 100 / (float)$product->original_price));
                ?>
                    <div class="offer">
                        <?php echo load_svg('arrow-down.svg'); ?>
                        <span class="save"><?php echo $off; ?>%<br/>off</span>
                    </div>
                <?php } ?>

                <div class="main-gallery">
                    <ul class="gallery-list">
                        <?php foreach($product->gallery as $gallery){ ?>
                            <li class="gallery-data">
                                <?php
                                echo lazyload(array(
                                    'src'   => site_url('image/resize?w=760&h=460&src='.fix_image_resize($gallery->path)),
                                    'alt'   => $gallery->subtitle,
                                    'title' => $gallery->subtitle,
                                    'rel'   => 'gallery',
                                    'class' => 'lazyload image colorbox',
                                    'tag'   => 'a',
                                    'href'  => site_url('image/resize?w=1250&h=900&src='.fix_image_resize($gallery->path)),
                                    'data-autoload' => false,
                                )); ?>
                                <div class="subtitle"><?php echo $gallery->subtitle;?></div>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="common-dots mobile-only"></div>
                </div>

                <div class="small-gallery desktop-only">
                    <ul class="gallery-list">
                        <?php foreach($product->gallery as $gallery){ ?>
                            <li class="gallery-data">
                                <?php
                                echo lazyload(array(
                                    'src'   => site_url('image/resize?w=80&h=80&src='.fix_image_resize($gallery->path)),
                                    'alt'   => $gallery->subtitle,
                                    'title' => $gallery->subtitle,
                                    'class' => 'lazyload image',
                                    'tag'   => 'figure',
                                    'data-autoload' => false,
                                )); ?>
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="common-arrows">
                        <button class="arrow prev"><i class="fa fa-angle-left"></i></button>
                        <button class="arrow next"><i class="fa fa-angle-right"></i></button>
                    </div>
                </div>

            </section>

            <section class="sell-data right">

                <h1 class="title"><?php echo $product->title; ?></h1>
                <?php if(!empty($product->code)){ ?>
                    <p class="code">Ref.: <?php echo $product->code; ?></p>
                <?php }
                
                if($product->inventory > 0) { ?>

                    <div class="price">
                        <?php if($product->original_price != $product->price){ ?>
                            <small class="old">R$ <span id="product-price-old"><?php echo mysql_decimal_to_number($product->original_price); ?></span></small>
                        <?php } ?>
                        <strong class="current">R$ <span id="product-price"><?php echo mysql_decimal_to_number($product->price); ?></span></strong>
                    </div>

                    <div class="payment">
                        <?php echo $this->load->view('_parcelas', array(
                            'productPrice' => $product->price,
                            'showMore' => TRUE
                        )); ?>
                    </div>

                    <form action="<?php echo site_url('carrinho/add'); ?>" method="POST" id="cart-add" class="common-form">

                        <?php if(!empty($product->variations)){ ?>
                            <ul class="variations-list">
                                <li class="variation-data">
                                    <label for="variation">Opcional</label>
                                    <div class="form-group">
                                        <div class="select-wrapper">
                                            <?php $this->load->view('comum/preloader'); ?>
                                            <select name="variation" id="variation" data-product="<?php echo $product->id; ?>" placeholder="Selecione" required>
                                                <option value="">Selecione</option>
                                                <?php
                                                    foreach($product->variations as $variation){
                                                        if($variation->quantity >= 1){ ?>
                                                            <option value="<?php echo $variation->id; ?>"><?php echo $variation->title; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <div class="transport-box">
                            <div class="form-group quantity-box">
                                <label for="quantity">Quant.</label>
                                <input type="number" name="quantity" value="1" id="quantity" required class="mask-quantity">
                            </div>
                            <div class="form-group zipcode-box">
                                <label for="zipcode">Calcular frete e prazo:</label>
                                <input type="<?php echo $_is_mobile ? 'tel' : 'text'; ?>" name="zipcode" placeholder="CEP" id="zipcode">
                                <button type="button" id="calculate" data-product="<?php echo $product->id; ?>" class="common-button">
                                    <span>Calcular</span>
                                    <?php $this->load->view('comum/preloader'); ?>
                                </button>
                            </div>
                            <div class="shipping-result"></div>
                        </div>

                        <input type="hidden" name="product" value="<?php echo $product->id; ?>">
                        <button type="submit" id="add-to-cart" class="common-button">
                            <span>Adicionar ao Carrinho</span>
                            <?php $this->load->view('comum/preloader'); ?>
                        </button>

                    </form>

                <?php } else { ?>
                <div class="no-stock">
                    <p>Produto Esgotado</p>
                    <a href="<?php echo site_url('contato'); ?>" class="common-button"><span>Solicitar Orçamento</span></a>
                </div>
                <?php } ?>

            </section>
        </div>

        <section class="product-content">

            <?php if(!empty($product->videos) || !empty($product->description)){ ?>
                <div class="left description">
                    <h2 class="title">ESPECIFICAÇÕES</h2>
                    <?php if(!empty($product->videos)){ ?>
                        <h3 class="title">Vídeo</h3>
                        <div class="video-gallery">
                            <ul class="gallery-list">
                                <?php foreach ($product->videos as $key => $value) { ?>
                                <li class="gallery-data">
                                    <?php
                                    echo lazyload(array(
                                        'src'   => site_url('image/resize?w=960&h=350&src=https://img.youtube.com/vi/'.get_youtube_id($value->link).'/0.jpg'),
                                        'alt'   => $product->title,
                                        'rel'   => 'videos',
                                        'class' => 'videos-play lazyload iframe colorbox',
                                        'tag'   => 'a',
                                        'href'  => '//www.youtube.com/embed/'.get_youtube_id($value->link),
                                        'data-autoload' => false,
                                    ));
                                    ?>
                                </li>
                                <?php } ?>
                            </ul>
                            <div class="common-arrows desktop-only">
                                <button class="arrow prev"><i class="fa fa-angle-left"></i></button>
                                <button class="arrow next"><i class="fa fa-angle-right"></i></button>
                            </div>
                            <div class="common-dots"></div>
                        </div>
                    <?php } ?>

                    <div class="common-text">
                        <?php echo $product->description; ?>
                    </div>

                </div>
            <?php } ?>

            <div class="right characteristcs">
                <h2 class="title">Características da Embalagem</h2>
                <div class="common-text">
                    <ul class="product-characteristics">
                        <li>Comprimento <strong><?php echo number_format((float) $product->length); ?> cm</strong></li>
                        <li>Largura <strong><?php echo number_format((float) $product->width); ?> cm</strong></li>
                        <li>Altura <strong><?php echo number_format((float) $product->height); ?> cm</strong></li>
                        <li>Peso <strong><?php echo (float) $product->weight; ?> kg</strong></li>
                        <?php foreach ($product->characteristics as $characteristic) { ?>
                            <li><?php echo $characteristic->title.' '.$characteristic->value; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>

        </section>
    </article>

    <?php if(!empty($product->related)){ ?>
        <aside id="related-products" class="common-products">
            <h2 class="">Você também pode gostar de:</h2>
            <div class="related-gallery">
                <?php $this->load->view('listagem', array('products' => $product->related)); ?>
                <div class="common-arrows desktop-only">
                    <button class="arrow prev"><i class="fa fa-angle-left"></i></button>
                    <button class="arrow next"><i class="fa fa-angle-right"></i></button>
                </div>
                <div class="common-dots mobile-only"></div>
            </div>
        </aside>
    <?php } ?>

    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>
