/**
 * @author: Ralf da Rocha
 * @copyright: StoreTrooper - 2016
 */

"use strict";

function Meus_enderecos(){ this.init(); };

Meus_enderecos.prototype.init = function() {

    var self = this;
    this.delete_address();
    this.address_type();
    this.navigation();
    this.calc_height();

    $(window).on('resize', function() {
        if($('main .address-wrapper').height() != $('main .change-content:not(.hide)').height()) {
            self.calc_height();
        }
    });

};

Meus_enderecos.prototype.navigation = function(){
    var self = this;
console.log('11');
    $('body').on('click','#add-address',function(event) {
        self.load_form();
    });
    $('body').on('click','main .change-address',function(event) {
        self.load_form($(this).closest('.address-content').data('id'));
    });
    $('body').on('click','#new-address .common-back',function(event) {
        $('#new-address').addClass('hide');
        setTimeout(function(){
            $('#new-address').html('');
        },300);
        $('#address-container').removeClass('hide');
        self.calc_height();
    });
};

Meus_enderecos.prototype.calc_height = function(){
    setTimeout(function(){
        $('main .address-wrapper').stop().animate({ height: $('main .change-content:not(.hide)').height() },500);
    },300);
};

Meus_enderecos.prototype.load_form = function(id){
    var self = this;
    var xhr = false;
    var $content = $(this).closest('.address-wrapper');
    $content.addClass('loading');
    if (xhr)
        xhr.abort();
    xhr = $.ajax({
        method: 'GET',
        url: base_url + 'minha-conta/meus-enderecos/form' + (typeof id != 'undefined' ? '/'+id : ''),
        dataType: 'json',
        success: function(data){
            xhr = false;
            if(typeof data.title != 'undefined' || typeof data.message != 'undefined') {
                app.dialog.open({
                    'class': data.class ? data.class : 'error',
                    'title': data.title ? data.title : 'Ops!',
                    'message': data.message,
                    'callback': function(){
                        if (data.redirect !== undefined && data.redirect)
                            window.location.href = data.redirect;
                    }
                });
            } else if(typeof data.view != 'undefined') {
                $('#new-address').html(data.view);
                $('#new-address').removeClass('hide');
                $('#address-container').addClass('hide');

                app.setForm($('#form-address'), function(data){
                    if (data.status){
                        var $view = $(data.view),
                            $box = $('#address-container .address-content[data-id="'+data.address.id+'"]');
                        if ($box.length > 0){
                            $box.replaceWith($view);
                        }else{
                            $view.appendTo('#address-container');
                        }
                        $('#new-address .common-back').trigger('click');
                    }
                });
                app.initMask();
                app.configSelects();
                self.calc_height();
                new Address($('#form-address'));
            }
            $content.removeClass('loading');
        }
    });
};

Meus_enderecos.prototype.address_type = function(){
    var xhr = false;
    $('#address-container').on('change', 'input[type="radio"]', function(e){
        e.preventDefault();
        var $content = $(this).closest('.address-content');
        $content.addClass('loading');
        if (xhr)
            xhr.abort();
        xhr = $.post(base_url+'minha-conta/meus-enderecos/update',{name: $(this).attr('name'), id: $content.data('id')}, function(data){
            $content.removeClass('loading');
            xhr = false;
            app.dialog.open({
                class: data.class,
                title: data.title,
                message: data.message
            });
        });
    });
};

Meus_enderecos.prototype.delete_address = function(){
    var self = this;
    $('#address-container').on('click', '.delete-address', function(e){
        e.preventDefault();
        var $content = $(this).closest('.address-content');
        app.dialog.open({
            class: 'alert',
            cancel: true,
            button: 'Excluir',
            title: 'Deseja excluir o endereço?',
            message: 'Você tem certeza que deseja excluir o endereço selecionado? Esta opção não poderá ser revertida.',
            callback: function(){
                $content.addClass('loading');
                $.post(base_url+'minha-conta/meus-enderecos/delete',{id: $content.data('id')}, function(data){
                    $content.slideUp(function() { $content.remove(); });
                    self.calc_height();
                    app.dialog.open({
                        class: data.class,
                        title: data.title,
                        message: data.message
                    });
                });
            }
        });
    });
};

$(document).ready(function (){
    new Meus_enderecos();
});