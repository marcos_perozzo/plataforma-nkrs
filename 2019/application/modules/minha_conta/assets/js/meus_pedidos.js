/**
 * @author: Fabio Bachi
 * @copyright: StoreTrooper - 2016
 */

"use strict";

function Meus_pedidos(){ this.init(); };

Meus_pedidos.prototype.init = function() {

    $(window).on('resize',function(){
        if($('#order-detail').length) {
            var index = $('.overflow-scroll .current').index();
            $('.overflow-scroll').stop().animate({ scrollLeft: (($('.status-data').width() * index) + 60) },800);
        }
    }).resize();

};

$(document).ready(function (){
    new Meus_pedidos();
});