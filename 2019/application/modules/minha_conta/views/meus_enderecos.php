<main id="my-address">
    <?php $this->load->view('header',array('current_menu' => 'address')) ?>
    <section class="content">

        <div class="address-wrapper">
            <div id="address-container" class="change-content">
                <div class="content-top">
                    <h1 class="account-title">Meus Endereços</h1>
                    <button href="<?php echo site_url('minha-conta/meus-enderecos/endereco'); ?>" class="common-button colorbox" id="add-address">
                        <?php echo load_svg('more.svg'); ?>
                        <span>Adicionar endereço</span>
                    </button>
                </div>
                <?php $this->load->view('endereco'); ?>
            </div>

            <div id="new-address" class="change-content hide"></div>
            <div class="content-loader">
                <?php $this->load->view('comum/preloader'); ?>
            </div>
        </div>

    </section>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>