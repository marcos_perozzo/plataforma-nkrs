<section class="common-header">
    <!-- título e breadcrumbs -->
    <div class="page-data">
        <div class="icon desktop-only"><?php echo load_svg('user.svg'); ?></div>
        <div class="text">
            <ul class="breadcrumbs">
                <li class="breadcrumb-data"><a href="<?php echo site_url(); ?>">Página inicial</a></li>
                <li class="breadcrumb-data"><strong>Minhas Iformações</strong></li>
            </ul>
            <div class="icon mobile-only"><?php echo load_svg('user.svg'); ?></div>
            <h1 class="title">Olá, <?php echo $client->name; ?></h1>
        </div>
    </div>

</section>

<nav class="user-menu desktop-only">
    <ul class="menu-list">
        <li class="menu-data"><h2><a href="<?php echo site_url('minha-conta/meus-pedidos'); ?>" class="title<?php echo $current_menu == 'orders' ? ' current' : ''; ?>"><span>Meus Pedidos</span></a></h2></li>
        <li class="menu-data"><h2><a href="<?php echo site_url('minha-conta/meus-dados'); ?>" class="title<?php echo $current_menu == 'user_data' ? ' current' : ''; ?>"><span>Meus Dados</span></a></h2></li>
        <li class="menu-data"><h2><a href="<?php echo site_url('minha-conta/meus-enderecos'); ?>" class="title<?php echo $current_menu == 'address' ? ' current' : ''; ?>"><span>Meus Endereços</span></a></h2></li>
        <li class="menu-data"><h2><a href="<?php echo site_url('minha-conta/alterar-senha'); ?>" class="title<?php echo $current_menu == 'change_pass' ? ' current' : ''; ?>"><span>Alterar Senha</span></a></h2></li>
    </ul>
</nav>