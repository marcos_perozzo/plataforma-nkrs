<main id="change-password" class="common-account">
    <?php $this->load->view('header',array('current_menu' => 'change_pass')) ?>
    <section class="content">

        <div class="content-top">
            <h1 class="account-title">Alterar senha</h1>
        </div>
        <form action="<?php echo site_url('minha-conta/alterar-senha/change-password'); ?>" method="POST" id="form-password" class="ajax-form common-form" novalidate autocomplete="off">
            <div class="row">
                <div class="form-group">
                    <input type="password" name="current_password" id="old-password" placeholder="Senha Atual" required>
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="new-password" placeholder="Nova Senha" required>
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirm" id="confirm-password" placeholder="Confirmar Nova Senha" required>
                </div>
            </div>
            <div class="row info">
                <div class="common-text"><p class="text">Certifique-se que sua senha possui no mínimo uma letra, um número e ao menos 8 caracteres</p></div>
                <button type="submit" class="common-button">
                    <?php $this->load->view('comum/preloader'); ?>
                    <?php echo load_svg('save.svg'); ?>
                    <span>Salvar</span>
                </button>
            </div>
        </form>
    </section>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>