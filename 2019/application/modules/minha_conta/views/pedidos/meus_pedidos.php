<main id="my-orders" class="common-account">

    <?php $this->load->view('header',array('current_menu' => 'orders')) ?>

    <section class="content">

        <div class="content-top">
            <h1 class="account-title">Meus pedidos</h1>
        </div>

        <?php if(!empty($orders)) { ?>
        <div class="table-top desktop-only">
            <ul class="order-items">
                <li class="order-data order">Pedido</li>
                <li class="order-data items">Itens</li>
                <li class="order-data total">Total</li>
                <li class="order-data address">Endereços</li>
                <li class="order-data status">Status</li>
            </ul>
        </div>
        <ul class="orders-list">
            <?php foreach ($orders as $key => $order){ ?>
                <li class="order-dup">
                    <div class="order-content">
                        <div class="order-data order">
                            <strong class="code"><?php echo $order->reference; ?></strong>
                            <time class="date">Data <?php echo date("d/m/Y - H:i", strtotime($order->created)); ?>h</time>
                        </div>
                        <div class="order-data items">
                            <strong><?php echo $order->quantity; ?> <span class="mobile-only">Itens</span></strong>
                        </div>
                        <div class="order-data total">
                            <strong>R$<?php echo mysql_decimal_to_number($order->price); ?></strong>
                        </div>
                        <div class="order-data address">
                            <span class="type">Entrega</span>
                            <div class="common-text">
                                <?php echo $order->address; ?>
                            </div>
                        </div>
                        <div class="order-data status">
                            <strong class="desc"><?php echo $order->status; ?></strong>
                            <?php $history = current($order->history) ?>
                            <time class="date">Dia  <?php echo $history->date; ?>h</time>
                            <a class="details" href="<?php echo site_url('minha-conta/meus-pedidos/'.$order->reference); ?>">Detalhes do Pedido</a>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
        <?php } else { ?>
        <div class="common-empty">Você não possui nenhum pedido!</div>
        <?php } ?>
    </section>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>