<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Meus_pedidos extends MY_Controller
{

    public function index()
    {
        if (!$this->st->client->get()){
            redirect('login');
        }

        $orders = $this->st->orders->get(array('client' => $this->st->client->get()->id));

        $this->template
             ->set('title', 'Utimil - Meus Pedidos')
             ->add_css('css/meus_pedidos')
             ->add_js('js/meus_pedidos')
             ->set('orders', $orders)
             ->build('pedidos/meus_pedidos');
    }

    public function detalhes($reference = FALSE)
    {
        if (!$this->st->client->get())
            redirect('login');

        if (!$reference)
            show_404();

        $order = $this->st->orders->get(array('reference' => $reference));

        if (!$order)
            show_404();

        $this->template
             ->set('title', 'Utimil - Pedido '.$reference)
             ->add_css('css/meus_pedidos')
             ->add_js('js/meus_pedidos')
             ->set('order', $order)
             ->build('pedidos/detalhes');

    }

}
