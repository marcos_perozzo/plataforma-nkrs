<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alterar_senha extends MY_Controller {

    public function index()
    {
        if (!$this->st->client->get()){
            redirect('login');
        }

        $route = array('Minha Conta');

        $this->template
             ->add_css('css/alterar_senha')
             ->add_js('js/alterar_senha')
             ->set('title', 'Utimil - Alterar Senha')
             ->set('route', $route)
             ->build('alterar_senha');
    }

    public function change_password()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        if (!$this->st->client->get()){
            $response = array(
                'status'    => FALSE,
                'class'     => 'error',
                'title'     => 'Ocorreu um erro!',
                'message'   => 'Você precisa estar logado para efetuar essa ação',
                'redirect'  => site_url('login')
            );
        }else{

            // Validação
            $this->load->library('form_validation');
            $this->form_validation->set_rules('current_password', 'Senha Atual', 'trim|required');
            $this->form_validation->set_rules('password', 'Senha', 'trim|required|matches[password_confirm]|min_length[8]');
            $this->form_validation->set_rules('password_confirm', 'Repetir Senha', 'trim|required|min_length[8]');

            // Mensagens
            $this->form_validation->set_message('matches', 'Os campos de senhas são obrigatórios e devem ser preenchidos com a mesma senha.');
            $this->form_validation->set_message('min_length', 'Sua senha deve conter ao menos 8 caracteres.');

            if ($this->form_validation->run()) {

                $client = $this->st->client->edit_password($this->input->post());

                $response = array(
                    'status'=> TRUE,
                    'class' => 'success',
                    'title' => 'Sucesso!',
                    'message' => 'Sua senha foi alterada com sucesso!'
                );

            } else {
                $error = $this->form_validation->error_array();
                $response = array(
                    'status'=> FALSE,
                    'class' => 'error',
                    'title' => 'Ocorreu um erro!',
                    'message' => array_shift($error),
                    'fail' => $this->form_validation->error_array(TRUE)
                );
            }
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }

}
