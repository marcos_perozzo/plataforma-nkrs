/**
 * @author: Ralf da Rocha
 * @copyright: StoreTrooper - 2016
 */

"use strict";

var sobre;

function Sobre(){ return this; };

Sobre.prototype.init = function() {

};


$(document).ready(function (){
    sobre = new Sobre();
    sobre.init();
});