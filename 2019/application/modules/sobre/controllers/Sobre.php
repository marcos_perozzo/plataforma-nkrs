<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sobre extends MY_Controller {

    /**
     * @author Michael Cruz [michael.fernando.cruz@gmail.com]
     * @date   2017-11-05
     */
    public function index()
    {
        $this->template
             ->add_css('css/sobre')
             ->add_js('js/sobre')
             ->set('title', $this->site_title)
             ->build('sobre');
    }
}
