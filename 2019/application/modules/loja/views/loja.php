<section id="store" class="container store page">
    <div class="row">
        <div class="col-md-3">
            <!-- FILTRO -->
            <?php $this->load->view('aside'); ?>
        </div>
        <div class="col-md-9">
            <h3 class="section-title"><?=$page_name?></h3>
            <div class="store__list">
                <?php if(count($products)> 0) { ?>
                    <?php foreach($products as $product){ ?>
                        <div class="store__list__item product">
                            <a href="<?= site_url('loja/'.$product->slug); ?>" class="product__image">
                                <img src="<?=$product->image?>" alt="<?=$product->title?>"/>
                            </a>
                            <div class="product__info">
                                <p><?=$product->title?></p>
                                <h4>R$ <?=number_format($product->price, 2, ',', '.');?></h4>
                                <a href="<?= site_url('loja/detalhes'); ?>" class="btn btn-primary">Ver mais</a>
                            </div>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <p>Não há produtos para essa categoria.</p>
                <?php } ?>
            </div>
        </div>
    </div>
</section>