<div class="page">
    <section class="container product-detail">
        <div class="row">
            <div class="col-lg-4">
                <div class="product-detail__info">
                    <h3 class="section-title"><?=$product->title?></h3>
                    <legend><b>REF: <?=$product->code?></b></legend>
                    <?=$product->description?>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="product-detail__image">
                    <?php if(count($product->gallery) > 1){ ?>
                        <div id="sliderProduct" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <?php for($i=0;$i<count($product->gallery);$i++){ ?>
                                    <li data-target="#sliderProduct" data-slide-to="<?=$i?>" <?=$i == 0 ? 'class="active"' : ''?>></li>
                                <?php } ?>
                            </ol>
                            <div class="carousel-inner">
                            <?php foreach($product->gallery as $image){ ?>
                                <div class="carousel-item" style="background-image: url('<?=$image->path?>');"></div>
                            <?php } ?>
                            </div>
                            <a class="carousel-control-prev" href="#sliderProduct" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#sliderProduct" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    <?php } else { ?>
                        <img class="img-fluid" src="<?=$product->gallery[0]->path?>"/>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="product-detail__actions">
                    <!-- SELEÇÃO DAS OPÇÕES -->
                    <h5>Opções</h5>
                    <input type="hidden" name="product_id" value="<?=$product->id?>"/>
                    <?php if(!empty($product->additional)){ ?>
                        <?php $i = 1; ?>
                        <select class="form-control" name="additional">
                            <?php foreach($product->additional as $key=>$additional) { ?>
                                <?php if(count((array)$product->additional) == $i){ ?>
                                    <option value="" disabled selected><?=$additional->title?></option>
                                <?php } else { ?>
                                    <option value="<?=$additional->id?>"><?=$additional->title?></option>
                                <?php } ?>
                                <?php $i++; ?>
                            <?php } ?>
                        </select>
                    <?php } ?>
                    <?php if(!empty($product->optional)){ ?>
                        <?php $i = 1; ?>
                        <select class="form-control" name="optional">
                            <?php foreach($product->optional as $key=>$optional) { ?>
                                <?php if(count((array)$product->optional) == $i){ ?>
                                    <option value="" disabled selected><?=$optional->title?></option>
                                <?php } else { ?>
                                    <option value="<?=$optional->id?>"><?=$optional->title?></option>
                                <?php } ?>
                                <?php $i++; ?>
                            <?php } ?>
                        </select>
                    <?php } ?>
                    <hr/>
                    <div class="form-group row">
                        <label class="col col-form-label">Quantidade</label>
                        <div class="col">
                            <input type="number" class="form-control text-center mb-0" name="quantity" min="1" value="1" required/>
                        </div>
                    </div>
                    <hr/>
                    <h3 class="mb-5">R$ <?=$product->price?></h3>
                    <button type="button" class="btn btn-lg btn-primary" data-cart data-url="<?= site_url('cart/add'); ?>">Comprar</button>
                </div>
            </div>
        </div>
        <hr/>
    </section>
    <section class="container store">
        <h3 class="section-title">Veja também:</h3>
        <div class="store__list">
            <?php foreach($product->related as $product){ ?>
                <div class="store__list__item product">
                    <a href="<?= site_url('loja/'.$product->slug); ?>" class="product__image">
                        <img src="<?=$product->image?>" alt="<?=$product->title?>"/>
                    </a>
                    <div class="product__info">
                        <p><?=$product->title?></p>
                        <h4>R$ <?=number_format($product->price, 2, ',', '.');?></h4>
                        <a href="<?= site_url('loja/detalhes'); ?>" class="btn btn-primary">Ver mais</a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
</div>