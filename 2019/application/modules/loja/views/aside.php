<aside class="list-group">
    <a href="<?= site_url('loja'); ?>" class="list-group-item list-group-item-action" title="Dashboard">Todos os produtos</a>
    <?php foreach($categories as $category) { ?>
        <a href="<?= site_url('loja/'.$category->slug); ?>" class="list-group-item list-group-item-action" title="<?=$category->title?>"><?=$category->title?></a>
    <?php } ?>
</aside>