<main id="contact">

    <section class="common-header">
        <!-- título e breadcrumbs -->
        <div class="page-data">
            <div class="icon desktop-only"><?php echo load_svg('email.svg'); ?></div>
            <div class="text">
                <ul class="breadcrumbs">
                    <li class="breadcrumb-data"><a href="<?php echo site_url(); ?>">Página inicial</a></li>
                    <li class="breadcrumb-data"><strong>Contato</strong></li>
                </ul>
            </div>
        </div>

    </section>

    <section class="content">
        <div class="common-text">Preencha os campos abaixo e envie uma mensagem, seja uma dúvida ou sugestão responderemos o quanto antes!</div>
        <form action="<?php echo site_url('contato/send'); ?>" method="POST" id="form-contact" class="ajax-form common-form" novalidate>
            <div class="row">
                <div class="form-group">
                    <input type="text" name="name" required <?php echo $client && isset($client->name) ? 'value="'.$client->name.'"' : ''; ?> placeholder="Nome">
                </div>
                <div class="form-group">
                    <input type="email" name="email" required <?php echo $client && isset($client->email) ? 'value="'.$client->email.'"' : ''; ?> placeholder="E-mail">
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <input type="tel" name="phone" class="mask-phone" required <?php echo $client && isset($client->phone) ? 'value="'.$client->phone.'"' : ''; ?> placeholder="Telefone">
                </div>
                <div class="form-group">
                    <div class="select-wrapper">
                        <select name="subject" required>
                            <option value="">ASSUNTO</option>
                            <?php foreach($subjects as $subject) { ?>
                            <option value="<?php echo $subject->title; ?>"><?php echo $subject->title; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <textarea name="message" required placeholder="Mensagem"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="form-group submit">
                    <button type="submit" class="common-button g-recaptcha" data-sitekey="6LecV5IUAAAAAOWVkKbwrO8mW0dYJJh0XfNZ7Tkb" data-callback="recaptcha_callback">
                        <?php $this->load->view('comum/preloader'); ?>
                        <?php echo load_svg('paper-plane.svg'); ?>
                        <span>Enviar</span>
                    </button>
                </div>
            </div>
        </form>
        <div class="contact-info">
            <div class="left">
                <ul class="contact-list">
                    <?php if($store->phone_alternative > 0){?>
                    <li class="contact-data">
                        <a class="link whats" href="tel:<?php echo preg_replace('/\D/', '', $store->phone_alternative); ?>">
                            <span class="icon"><?php echo load_svg('whatsapp.svg'); ?></span>
                            <strong class="number"><?php echo $store->phone_alternative; ?></strong>
                        </a>
                    </li>
                    <?php }?>
                    <li class="contact-data">
                        <a class="link phone" href="tel:<?php echo preg_replace('/\D/', '', $store->phone); ?>">
                            <span class="icon"><?php echo load_svg('phone.svg'); ?></span>
                            <strong class="number"><?php echo $store->phone; ?></strong>
                        </a>
                    </li>
                    <li class="contact-data">
                        <a class="link email" href="mailTo:<?php echo $store->email; ?>">
                            <span class="icon"><?php echo load_svg('email.svg'); ?></span>
                            <strong class="text"><?php echo $store->email; ?></strong>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="right">
                <h5 class="address-title">Endereço</h5>
                <address class="address"><?php echo $store->address; ?></address>
                <a href="https://goo.gl/maps/9vZGvNaYZiG2" class="address-link" target="_blank">Veja como Chegar</a>
            </div>
        </div>
    </section>
    <aside class="common-advantages">
        <?php $this->load->view('comum/advantages'); ?>
    </aside>
</main>