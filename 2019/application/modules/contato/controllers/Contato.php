<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contato extends MY_Controller
{

    public function index()
    {
        $client = $this->st->client->get();

        $route = array('Contato');
        $this->template
             ->set('title', 'Utimil - Contato')
             ->add_css('css/contato')
             ->add_js('https://www.google.com/recaptcha/api.js', NULL,  'body', TRUE, TRUE)
             ->add_js('js/contato')
             ->set('route', $route)
             ->set('client', $client)
             ->set('subjects', $this->st->content->get(array('content' => 'contato-assuntos')))
             ->build('contato');
    }

    public function send()
    {
        if (!$this->input->is_ajax_request())
            show_404();

        // Validação
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nome', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Telefone', 'trim|required');
        $this->form_validation->set_rules('message', 'Mensagem', 'trim|required');
        $this->form_validation->set_rules('g-recaptcha-response', 'Recaptcha', 'trim|required|valid_recaptcha');

        if ($this->form_validation->run()) {
            $data = $this->input->post();
            unset($data['g-recaptcha-response']);
            $contact = $this->st->contact->add($data, array('subject' => $data['subject'], 'send_client' => true));

            if ($contact){
                $response = array(
                    'status'=> TRUE,
                    'class' => 'success',
                    'title' => 'Sucesso!',
                    'message' => 'Contato efetuado com sucesso! Responderemos sua mensagem o mais breve possível.',
                    'reset' => TRUE,
                );
            }else{
                $response = array(
                    'status'=> FALSE,
                    'class' => 'error',
                    'title' => 'Ops!',
                    'message' => 'Ocorreu um erro inesperado. Por favor, tente novamente.'
                );
            }

        } else {
            $error = $this->form_validation->error_array();
            $response = array(
                'status'=> FALSE,
                'class' => 'error',
                'title' => 'Ocorreu um erro!',
                'message' => (count($error) > 1 ? 'Preencha os campos corretamente!' : array_shift($error)),
                'fail' => $this->form_validation->error_array(TRUE)
            );
        }

        $this->output->set_content_type('application/json')
                     ->set_output(json_encode($response));
    }


}
