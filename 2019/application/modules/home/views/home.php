<section id="sliderHome" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#sliderHome" data-slide-to="0" class="active"></li>
        <li data-target="#sliderHome" data-slide-to="1"></li>
        <li data-target="#sliderHome" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active" style="background-image: url('<?= site_url('image/resize?w=1920&h=535&src=../assets/site/images/slide1.jpg'); ?>');">
            <div class="carousel-caption d-none d-md-block">
                <div class="carousel-caption__text">
                    <h2>Slide 1</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vulputate libero ut dolor ultrices, mattis suscipit nunc pretium.</p>
                </div>
                <div class="carousel-caption__cta">
                    <a href="#" class="btn btn-lg btn-primary">Veja mais</a>
                </div>
            </div>
        </div>
        <div class="carousel-item" style="background-image: url('<?=site_url()?>assets/site/images/slide2.png');">
            <div class="carousel-caption d-none d-md-block">
                <div class=" carousel-caption__text">
                    <h2>Slide 2</h2>
                    <p>Segundo Slide</p>
                </div>
                <div class=" carousel-caption__cta">
                    <a href="#" class="btn btn-lg btn-primary">Veja mais</a>
                </div>
            </div>
        </div>
        <div class="carousel-item" style="background-image: url('<?=site_url()?>assets/site/images/slide3.jpg');">
            <div class="carousel-caption d-none d-md-block">
                <div class="carousel-caption__text">
                    <h2>Slide 3</h2>
                    <p>Terceiro Slide</p>
                </div>
                <div class="carousel-caption__cta">
                    <a href="#" class="btn btn-lg btn-primary">Veja mais</a>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#sliderHome" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#sliderHome" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</section>
<section id="home" class="container">
    <div class="row">
        <div class="col-sm-8">
            <h3 class="section-title">Últimos posts</h3>
            <?php if(!empty($posts)) { ?>
                <?php foreach($posts as $post) { ?>
                    <a href="<?= site_url(); ?>blog/detalhes?slug=<?=$post->slug?>" class="row box-news">
                        <div class="col-sm-4 col-md-3 box-news__thumb" style="background-image: url(<?=$post->image?>)"></div>
                        <div class="col-sm-8 box-news__info">
                            <span class="box-news__category"><?=$post->category?></span>
                            <p class="box-news__title"><?=$post->title?></p>
                            <p class="box-news__date"><?=$post->date?></p>
                        </div>
                    </a>
                <?php } ?>
            <?php } else { ?>
                <p>Não foi possível carregar os posts.</p>
            <?php } ?>
        </div>
        <div class="col-sm-4 side-content">
            <?php echo $this->load->view('comum/aside'); ?>
        </div>
    </div>
</section>