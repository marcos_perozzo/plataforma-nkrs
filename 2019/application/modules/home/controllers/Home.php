<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    /**
     * @author Marcos Orlando Perozzo [moperozzo@gmail.com]
     * @date   2020-02-03
     */
    public function index()
    {

        $banners = $this->st->content->get(array(
            'content' => 'banners'
        ));

        $posts = $this->st->blog->get(array('limit' => 5));

        $this->template
             ->add_css('css/home')
             ->add_js('js/home')
             ->set('title', $this->site_title)
             ->set('posts', $posts)
             ->set('banners', $banners)
             ->build('home');
    }

}
