/**
 * @author: Marcos Orlando Perozzo
 * @copyright: NKRS Competições - 2019
 */

"use strict";

var blog;

function Blog(){ return this; };

Blog.prototype.init = function() {

};


$(document).ready(function (){
    blog = new Blog();
    blog.init();
});