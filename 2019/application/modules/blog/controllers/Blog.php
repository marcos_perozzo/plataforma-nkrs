<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller {

    /**
     * @author Marcos Orlando Perozzo [moperozzo@gmail.com]
     * @date   20/11/2019
     */
    public function index()
    {
        $posts = $this->st->blog->get(array('limit' => 10));

        $this->template
             ->add_css('css/blog')
             ->add_js('js/blog')
             ->set('title', $this->site_title)
             ->set('posts', $posts)
             ->build('blog');
    }

    public function detalhes($c2 = FALSE)
    {
        if (!$c2)
            show_404();

        $post = $this->st->blog->get(array('slug' => $c2));

        $title = $post->title;
        $image = isset($post->gallery[0]->path) ? $post->gallery[0]->path : null;

        if (!empty($image)){
            $this->template
                 ->add_metadata('image', site_url('image/resize_canvas?w=600&h=315&src='.$image))
                 ->add_metadata('og:image', site_url('image/resize_canvas?w=600&h=315&src='.$image),'property')
                 ->add_metadata('twitter:image', site_url('image/resize_canvas?w=600&h=315&src='.$image));
        }else{
            $this->template
                 ->add_metadata('image', site_url('image/resize_canvas?w=600&h=315&src=application/modules/comum/assets/img/logo-color.png'))
                 ->add_metadata('og:image', site_url('image/resize_canvas?w=600&h=315&bg=2&src=application/modules/comum/assets/img/logo-color.png'),'property')
                 ->add_metadata('twitter:image', site_url('image/resize_canvas?w=600&h=315&src=application/modules/comum/assets/img/logo-color.png'));
        }

        $description = !empty($post->description) ? $post->description : null;

        if($description) {
            $this->template
                 ->add_metadata('description', html_entity_decode(character_limiter(strip_tags($description), 200)))
                 ->add_metadata('og:description', html_entity_decode(character_limiter(strip_tags($description), 200)), 'property')
                 ->add_metadata('twitter:description', html_entity_decode(character_limiter(strip_tags($description), 200)));
        }

        $this->template
             ->add_metadata('og:locale', 'pt_BR', 'property')
             ->add_metadata('og:url', site_url($this->uri->uri_string()), 'property')
             ->add_metadata('og:title', $title, 'property')
             ->add_metadata('og:site_name', $this->site_title, 'property')
             ->add_metadata('og:type', 'post.item', 'property')
             ->add_metadata('og:image:width', '600', 'property')
             ->add_metadata('og:image:height', '315', 'property')
             ->add_css('css/detalhes')
             ->add_js('js/detalhes')
             ->set('title', $title.' - '.$this->site_title)
             ->build('detalhes');
    }
    
    public function _remap($method, $params = array())
    {
        if (method_exists($this, $method))
            return call_user_func_array(array($this, $method), $params);
        else{
            $segments = $params;
            array_unshift($segments, $method);
            $segments = array_map(array($this, 'fix_slug'), $segments);
            $count = count($segments);
            if ($count == 1){
                call_user_func_array(array($this, 'detalhes'), array($segments));
            }else{
                $this->show_404();
            }
        }
    }
}
