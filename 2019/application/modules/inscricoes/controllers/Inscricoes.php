<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inscricoes extends MY_Controller {

    /**
     * @author Michael Cruz [michael.fernando.cruz@gmail.com]
     * @date   2017-11-05
     */
    public function index()
    {
        $this->template
             ->add_css('css/inscricoes')
             ->add_js('js/inscricoes')
             ->set('title', $this->site_title)
             ->build('inscricoes');
    }
}
