/**
 * @author: Marcos Orlando Perozzo
 * @copyright: NKRS Competições - 2019
 */

"use strict";

var inscricoes;

function Inscricoes(){ return this; };

Inscricoes.prototype.init = function() {

};


$(document).ready(function (){
    inscricoes = new Inscricoes();
    inscricoes.init();
});