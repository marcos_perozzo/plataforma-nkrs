/**
 * @author: Ralf da Rocha
 * @copyright: StoreTrooper - 2016
 */

"use strict";

var home;

function Home(){ return this; };

Home.prototype.init = function() {
    this.main_banner();
    this.promotion();
    this.trends();
};

Home.prototype.main_banner = function() {
    var items = 1;
    $('.main-banner .banner-list').slick({
        dots: true,
        slidesToShow: items,
        slidesToScroll: items,
        infinite: false,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 5000,
        appendDots: $('.main-banner .dots'),
        prevArrow: $('.main-banner .prev'),
        nextArrow: $('.main-banner .next'),
        responsive: [
            {
                breakpoint: 960,
                settings: {
                    arrows: false
                }
            },
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var selector = $('.main-banner .banner-data:eq('+nextSlide+') .lazyload');
        if(!selector.hasClass('loaded'))
            selector.lazyload('load');
    });
    $('.main-banner .banner-list').slick('slickPause');
    $('.main-banner .banner-data.slick-active .lazyload').lazyload({ background:true, onLoad: function (){ $('.main-banner .banner-list').slick('slickPlay'); }}).lazyload('load');
    $('.main-banner .banner-data:not(.slick-active) .lazyload').lazyload({background:true});
};

Home.prototype.promotion = function() {

    var items = 4;
    $('.promotion .products-list').slick({
        dots: true,
        slidesToShow: items,
        slidesToScroll: items,
        infinite: false,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: $('.promotion .prev'),
        nextArrow: $('.promotion .next'),
        appendDots: $('.promotion .common-dots'),
        responsive: [
            {
                breakpoint: 1700,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 1100,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 960,
                dots: true,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 570,
                dots: true,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        for (var i = 0; i < items; i++) {
            var selector = $('.promotion .product-data:eq('+(nextSlide+i)+') .lazyload');
            if(!selector.hasClass('loaded'))
                selector.lazyload('load');
        }
    });
    $('.promotion .product-data .lazyload').lazyload();
    $('.promotion .product-data.slick-active .lazyload').lazyload('load');

};

Home.prototype.trends = function() {

    var items = 4;
    $('.trends .products-list').slick({
        dots: true,
        slidesToShow: items,
        slidesToScroll: items,
        infinite: false,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: $('.trends .prev'),
        nextArrow: $('.trends .next'),
        appendDots: $('.trends .common-dots'),
        responsive: [
            {
                breakpoint: 1700,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 1100,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 960,
                dots: true,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 570,
                dots: true,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        for (var i = 0; i < items; i++) {
            var selector = $('.trends .product-data:eq('+(nextSlide+i)+') .lazyload');
            if(!selector.hasClass('loaded'))
                selector.lazyload('load');
        }
    });
    $('.trends .product-data .lazyload').lazyload();
    $('.trends .product-data.slick-active .lazyload').lazyload('load');

};

$(document).ready(function (){
    home = new Home();
    home.init();
});