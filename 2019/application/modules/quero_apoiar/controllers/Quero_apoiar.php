<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quero_apoiar extends MY_Controller {

    /**
     * @author Michael Cruz [michael.fernando.cruz@gmail.com]
     * @date   2017-11-05
     */
    public function index()
    {

        $banners = $this->st->content->get(array(
            'content' => 'banners'
        ));

        $posts = $this->st->blog->get(array('limit' => 5));

        $this->template
             ->add_css('css/home')
             ->add_js('js/home')
             ->set('title', $this->site_title)
             ->set('posts', $posts)
             ->set('banners', $banners)
             ->build('home');
    }

}
