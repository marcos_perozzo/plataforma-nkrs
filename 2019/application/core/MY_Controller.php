<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Controller.php";

class MY_Controller extends MX_Loader {

    protected $module;
    protected $method;
    protected $class;
    protected $isRun;
    protected $isMobile;
    protected $site_title;
    protected $client;
    public $store;

    public function __construct($isRun = FALSE)
    {
        parent::__construct();
        $this->isRun = $isRun;

        $this->load->library('storetrooper_ci', NULL, 'st');
        $this->load->library('user_agent');

        $this->load->helper('numeric');
        $this->module = $this->router->fetch_module();
        $this->method = $this->router->fetch_method();
        $this->class = $this->router->fetch_class();

        $this->site_title = 'NKRS Competições';

        //$this->sendEmail();

        $this->client = $this->st->client->get();

        $this->isMobile = $this->agent->is_mobile();
        
        if (!$this->input->is_ajax_request() && !$this->isRun){

            $this->store = $this->st->store->get();

            $this->categories = $this->st->categories->get(array('search' => array('id_parent' => ''), 'show_langs' => TRUE, 'key_slug' => TRUE, 'recursive' => TRUE));
            
            $this->template->add_js('plugins/plugins', 'comum')
                           ->set_partial('breadcrumb', 'breadcrumb', 'comum')
                           ->set_partial('header', 'header', 'comum')
                           ->set_partial('footer', 'footer', 'comum')
                           ->add_css('css/main', 'comum')
                           ->add_js('https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js')
                           ->add_js('https://kit.fontawesome.com/0fad5e6cde.js')
                           ->add_js('https://unpkg.com/aos@2.3.1/dist/aos.js')
                           ->add_css('https://fonts.googleapis.com/css?family=Exo+2|Orbitron')
                           ->add_css('https://use.fontawesome.com/releases/v5.6.3/css/all.css')
                           ->add_css('https://unpkg.com/aos@2.3.1/dist/aos.css')
                           ->add_css('https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css')
                           ->add_js('js/main', 'comum')
                           ->set('_module', $this->module)
                           ->set('_method', $this->method)
                           ->set('_class', $this->class)
                           ->set('_is_mobile', $this->isMobile)
                           ->set('client', $this->client)
                           ->set('total_cart', $this->st->cart->get_total())
                           ->set('categories', $this->categories)
                           ->set('current', $this->uri->segment(1))
                           ->set('hour_info', $this->st->content->get(array('content' => 'horario-de-funcionamento-texto')))
                           ->set('store', $this->store);
        }
    }

    public function sendEmail()
    {
        require("./application/libraries/sendgrid/sendgrid-php.php");
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("contato@adsites.store", "Example User");
        $email->setSubject("Teste com email");
        $email->addTo("diogotaparello@hotmail.com", "Example User");
        $email->addContent(
            "text/plain", "Mudei"
        );
        $email->addContent(
            "text/html", "Testando final com diogotaparello no whitelist"
        );
        //$key = getenv('SENDGRID_API_KEY');
        $sendgrid = new \SendGrid('SG.BPanGOQmRGGFn68hHqChbw.YrOOfutUazYgg-QlwRcglYjSY8KB-wuOnmQArWlRrzo');
        try {
            $response = $sendgrid->send($email);
            echo '<pre>';var_dump($response);
            exit();
            print $response->statusCode() . "\n";
            print_r($response->headers());
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    protected function fix_slug($value)
    {
        return str_replace('_', '-', $value);
    }

    protected function _initPagination($params)
    {
        $default = array(
            'segment'   => 3,
            'max'       => 9,
            'links'     => 2
        );
        $params = array_merge($default,$params);

        $this->load->library('pagination');

        $config['num_links']            = $params['links'];
        $config['full_tag_open']        = '<div class="pagination-list">';
        $config['full_tag_close']       = "</div>";
        $config['first_tag_open']       = "";
        $config['last_tag_open']        = "";
        $config['first_tag_close']      = "";
        $config['last_tag_close']       = "";
        $config['next_tag_open']        = '<button class="pagination-data last">';
        $config['prev_tag_open']        = '<button class="pagination-data first">';
        $config['next_tag_close']       = $config['prev_tag_close'] = "</button>";
        $config['first_link']           = FALSE;
        $config['last_link']            = FALSE;
        $config['next_link']            = '<i class="fa fa-angle-right"></i>';
        $config['prev_link']            = '<i class="fa fa-angle-left"></i>';
        $config['num_tag_open']         = '<div class="pagination-data number">';
        $config['num_tag_close']        = "</div>";
        $config['cur_tag_open']         = '<div class="pagination-data number current"><span>';
        $config['cur_tag_close']        = '</span></div>';
        $config['per_page']             = $params['max'];
        $config['use_page_numbers']     = TRUE;
        $config['reuse_query_string']   = TRUE;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment'] = 'pg';

        $query_string = $_GET;
        if(isset($query_string['pg']))
            unset($query_string['pg']);

        $config['base_url']     = trim($params['url'],'/').'/';
        $config['total_rows']   = $params['total'];
        $config['uri_segment']  = $params['segment'];
        $config['first_url']    = $config['base_url'].(!empty($query_string) ? '?'.http_build_query($query_string) : '');

        $this->pagination->initialize($config);
        $pagination = $this->pagination->create_links();
        return $this->load->view('comum/pagination', array('pagination' => $pagination, 'total' => ceil($params['total']/$params['max'])), TRUE);

    }

}