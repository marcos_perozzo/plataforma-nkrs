<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Config extends CI_Config {

    /**
     * Class constructor
     *
     * Sets the $config data from the primary config.php file as a class variable.
     *
     * @return  void
     */
    public function __construct()
    {
        $this->config =& get_config();

        // Set the base_url automatically if none was provided
        if (empty($this->config['base_url']) )
        {
            if (isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] != '::1')
            {
                $base_url = (is_https() ? 'https' : 'http').'://'.$_SERVER['SERVER_ADDR']
                    .substr($_SERVER['SCRIPT_NAME'], 0, strpos($_SERVER['SCRIPT_NAME'], basename($_SERVER['SCRIPT_FILENAME'])));
            }
            else
            {
                $base_url = 'http://localhost'
                    .substr($_SERVER['SCRIPT_NAME'], 0, strpos($_SERVER['SCRIPT_NAME'], basename($_SERVER['SCRIPT_FILENAME'])));
            }

            $this->set_item('base_url', $base_url);
        }

        log_message('info', 'Config Class Initialized');
    }

}