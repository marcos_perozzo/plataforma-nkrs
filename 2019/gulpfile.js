var gulp = require("gulp");
// Typescript
var notify = require("gulp-notify");
var es = require('event-stream');
var debug = require('gulp-debug');
var rename = require('gulp-rename');
var notifier = require('node-notifier');
// Less
var cssmin = require('gulp-minify-css');
var less = require('gulp-less');
var vinyl = require('vinyl');
var map = require('map-stream');
var findInFile = require('find-in-file');

var js =  ['application/modules/**/assets/js/*.js','!application/modules/**/assets/js/*.min.js'];

gulp.task('watch', function () {
    gulp.watch('application/modules/**/*.less', css);
    gulp.watch(js, ['compress']);
});


function getFilesThatContain(file){
    var regex = new RegExp('@import [\'"]?([^\'"]+)?('+file.replace('.','\\.')+')[\'"]?;', "g"),
        files = [],
        promises = [];
    return new Promise(
        (resolve) => {
            gulp.src(['application/modules/**/*.less', '!**/_*/**/*.less'])
                .pipe(map(function (file, cb) {
                    findInFile({
                        files: file.path,
                        find: regex
                    }, function(err, matchedFiles) {
                        if (matchedFiles.length){
                            matchedFiles.forEach((data) => {
                                var f = data.file.split('\\').pop();
                                if ((m = /^_/g.exec(f)) !== null) {
                                    promises.push(getFilesThatContain(f).then((fs)=>{
                                        files = files.concat(fs);
                                    }));
                                }else{
                                    files.push(data.file);
                                }
                            });
                        }
                    });
                    cb(null, file);
                })).on('end', function(){
                    Promise.all(promises).then(function(values) {
                        resolve(files)
                    });
                });
        }
    );
}

function css(file){
    var isFile = (typeof file === 'object'),
        lessPromise = new Promise((resolve) => {
            // Se for compilação inicial do gulp recompila tudo:
            if (!isFile){
                resolve(['application/modules/**/[^_]*.less', '!application/modules/comum/assets/plugins/**/*.less']);
            }
            // Se não abre o arquivo modificado
            var lessFile = new vinyl({ path: file.path });
            // Confere se arquivo começa com _
            if ((m = /^_/g.exec(lessFile.basename)) !== null) {
                getFilesThatContain(lessFile.basename).then((files)=>{
                    resolve(files);
                });
            }else{
                // Pega o arquivo a ser alterado
                resolve([lessFile.path]);
            }
        }).then((files)=>{
            // Compila e minifica os arquivos selecionados até aqui:
            return gulp.src(files, { base: 'application/modules' })
                    .pipe(
                        less({
                            paths: [
                                '.',
                                './node_modules',
                                './bower_components',
                                'application/modules/comum/assets/less'
                            ]
                        }).on("error", notify.onError(function (error) {
                            return "Error: " + error.message + '\n' + error.line + ':' + error.index
                        }))
                    )
                    .pipe(cssmin({compatibility:'ie9'}))
                    .pipe(rename(function(path) {
                        path.dirname += '/../css';
                    }))
                    .pipe(gulp.dest('application/modules'))
                    .pipe(debug({title:'Compiled LESS: '}))
                    .pipe(notify({
                        message:'Compiled LESS',
                        onLast:true
                    }));
        });
};

gulp.task('default', ["watch",'compress'], function(){
    css();
});

gulp.task('compress', function() {
    var minify = require('./node_modules/gulp-minify');

    gulp.src(js)
        .pipe(minify({
            ext:{
                min:'.min.js'
            },
            noSource: true,
            ignoreFiles: ['.min.js', '-min.js']
        }).on('error', notify.onError({
            message: "Error: <%= error.message %>",
            title: "Syntax error"
        })))
        .pipe(gulp.dest('application/modules'))
        .pipe(notify({
            message:'Compressed JS',
            onLast:true
        }));
});