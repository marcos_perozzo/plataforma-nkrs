<?php
/**
 * Class ST_Addresses
 *
* @package Storetrooper
**/
class ST_Addresses
{
    /**
     * Insert an address related to a client
     * @param  array     $data
     * @return object
     */
    public function add($data)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;

        if (!isset($data['client'])){
            require_once 'ST_Client.php';
            $stClient = new ST_Client;
            $client = $stClient->get();
            if ($client){
                $data['client'] = $client->id;
            }
        }
        return $curl->post('addresses/add', $data, FALSE);
    }

    public function edit($data)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;
        require_once 'ST_Client.php';
        $stClient = new ST_Client;

        $client = $stClient()->get();
        if ($client){
            $data['client'] = $client->id;
        }
        return $curl->post('addresses/edit', $data, FALSE);
    }

    /**
     * Get a json object with the client addresses
     * @param  array      $params
     * @return object
     */
    public function get($params = array())
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;
        require_once 'ST_Client.php';
        $stClient = new ST_Client;

        $mandatory = array(
            'client'    => $stClient->get()->id,
        );
        $params = array_merge($params, $mandatory);

        return $curl->get('addresses/get', $params, FALSE);
    }


    public function delete($id)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;
        require_once 'ST_Client.php';
        $stClient = new ST_Client;

        $params = array(
            'id'        => $id,
            'client'    => $stClient->get()->id
        );
        return $curl->post('addresses/delete', $params, FALSE);
    }

}