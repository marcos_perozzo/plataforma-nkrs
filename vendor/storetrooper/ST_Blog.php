<?php
/**
 * Class ST_Blog
 *
* @package Storetrooper
**/
class ST_Blog
{
    /*
     * Get a json object or array with the requested data
     * @param  array      $params
     * @return json
     */
    public function get($params = array())
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;

        $options = array(
            'limit'     => 12,
            'offset'    => 0,
            'slug'      => FALSE,
            'tag'       => FALSE,
            'category'  => FALSE,
            'total'     => FALSE
        );
        $params = array_merge($options, $params);

        return $curl->get('blog', $params, FALSE);
    }

    /*
     * Get a json object or array with the requested data
     * @param  array      $params
     * @return json
     */
    public function get_categories($params = array())
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;
        return $curl->get('blog/categories', $params, FALSE);
    }

}