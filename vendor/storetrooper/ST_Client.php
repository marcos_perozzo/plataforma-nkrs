<?php
/**
 * Class ST_Client
 *
* @package Storetrooper
**/
class ST_Client
{
    public function get($full = FALSE)
    {
        if ($full){
            require_once 'ST_Curl.php';
            $curl = new ST_Curl;
            return $curl->get('client/get', array('id' => $_SESSION['nkrs']['client']['id']));
        }
        else
            return is_null($_SESSION['nkrs']['client']) ? FALSE : (object) $_SESSION['nkrs']['client'];
    }

    public function add($data)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;

        if (isset($data['address'])){
            $address = $data['address'];
            unset($data['address']);
        }
        if (isset($data['newsletter'])){
            $newsletter = TRUE;
            unset($data['newsletter']);
        }

        $client = $curl->post('client/add', $data, FALSE);

        if (!empty($client)){

            $this->_login($client);

            if (isset($address)){
                require_once 'ST_Addresses.php';
                $addresses = new ST_Addresses;

                $address['client']  = $client->id;
                $address['billing'] = 1;
                $address['default'] = 1;
                $addresses->add($address);
            }

            if (isset($newsletter)){
                require_once 'ST_Utils';
                $utils = new ST_Utils;

                $newsletter = array(
                    'name'   => $client->name.' '.$client->last_name,
                    'email'  => $client->email,
                    'status' => 1
                );
                $utils->add_newsletter($newsletter);
            }
        }

        return $client;
    }

    public function edit($data)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;

        if (isset($data['newsletter'])){
            $newsletter = TRUE;
            unset($data['newsletter']);
        }else{
            $newsletter = FALSE;
        }

        //$data['id'] = $_SESSION['nkrs']['client']['id'];

        $client = $curl->post('client/edit', $data, FALSE);
        // if (!empty($client)){
        //     require_once 'ST_Utils.php';
        //     $utils = new ST_Utils;
        //     // Edit client session info
        //     $this->_login($client);

        //     $newsletter = array(
        //         'name'   => $client->name.' '.$client->last_name,
        //         'email'  => $client->email,
        //         'status' => $newsletter ? 1 : 0
        //     );
        //     $utils->update_newsletter($newsletter);
        // }

        return $client;
    }

    public function edit_password($data)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;
        $data['id'] = $_SESSION['nkrs']['client']['id'];
        return $curl->post('client/edit-password', $data, FALSE);
    }

    public function retrieve_password($email)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;

        $data = array(
            'email' => $email
        );
        $response = $curl->get('client/retrieve-password', $data, FALSE);

        return $response;
    }

    public function new_password($data)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;

        $client = $curl->post('client/new-password', $data, FALSE);

        $this->_login($client);

        return $client;
    }

    public function check_hash($hash)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;

        $data = array(
            'hash' => $hash
        );
        $response = $curl->get('client/check-hash', $data, FALSE);

        return $response;
    }

    public function login($data)
    {
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;

        $session = $_SESSION['nkrs'];
        if (isset($session['id_cart']) && $session['id_cart'])
            $data['id_cart'] = $session['id_cart'];

        $client = json_decode($curl->post('client/login', $data, FALSE));
        $this->_login($client->data);

        return json_encode($client);
    }

    public function facebook_login($data)
    {
        require 'ST_Curl';
        $curl = new ST_Curl;

        $session = $_SESSION['nkrs'];
        if (isset($session['id_cart']) && $session['id_cart'])
            $data['id_cart'] = $session['id_cart'];
        $client = json_decode($curl->post('client/facebook-login', $data, FALSE));

        if($client && !empty($data['picture'])){
            $client->picture = $data['picture'];
        }

        if($client){
            $this->_login($client);
        }

        return $client;
    }

    public function logout()
    {
        require_once 'Storetrooper.php';
        $storetrooper = new Storetrooper(ST_KEY, ST_URL);

        $storetrooper->clear_session('client');
    }

    private function _login($client)
    {
        session_start();
        $_SESSION['nkrs']['client'] = array(
            'id'        => $client->id,
            'name'      => $client->name,
            'email'     => $client->email,
            'phone'     => $client->phone,
            'picture'   => isset($client->picture) ? $client->picture : FALSE,
            'is_valid'  => (boolean) $client->cpf,
        );
    }

}