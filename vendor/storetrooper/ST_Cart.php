<?php
/**
 * Class ST_Cart
 *
* @package Storetrooper
**/
class ST_Cart
{
    /**
     * Add a product to the cart
     * @param  array     $params
     * @return object
     */
    public function add($params = array())
    {
        session_start();
        require_once 'ST_Curl.php';
        $curl = new ST_Curl;
        
        $options = array(
            //'quantity'  => 1,
            'variation' => FALSE
        );
        $params = array_merge($options, $params);

        // Get the session
        $session = $_SESSION['nkrs'];
        $cart = $session['cart'];
        // Create the product unique id
        $id = $params['product'].($params['variation'] ? '-'.$params['variation'] : '');
        // Adds the cart ID
        if (isset($session['id_cart']) && $session['id_cart']){
            $params['id_cart'] = $session['id_cart'];
        }
        // Increase the amount if the product is in cart
        if (isset($cart[$id])){
            $params['quantity'] += (int) $cart[$id]->quantity;
        }
        // Set the client id if is logged
        if (!is_null($session['client']) && isset($session['client']['id']))
            $params['client'] = $session['client']['id'];
        // Insert product in the cart
        $product = $curl->post('cart/add', $params, FALSE);
        // Create or update product object in the cart:
        if (!isset($cart[$id])){
            var_dump($params);
            var_dump('Cria dado');
            var_dump($product);
            $cart[$id] = (object) array(
                'id'        => $product->product->id,
                'image'     => $product->product->image,
                'variation' => $params['variation'] ? (object) array('id' => $product->variation->id, 'title' => $product->variation->title) : FALSE,
                'title'     => $product->product->title,
                'slug'      => $product->product->slug,
                'code'      => $product->product->code,
                'price'     => $params['variation'] ? $product->variation->price : $product->product->price,
                'weight'    => $product->product->weight,
                'quantity'  => $params['quantity'] ? (int) $params['quantity'] : 1,
                'promo' => isset($params['promo']) && $params['promo']
            );
            $_SESSION['nkrs']['resume']['items'] += 1;
        }else{
            $cart[$id]->quantity = $params['quantity'];
        }
        // Calculate the total price of the product
        $cart[$id]->total = $cart[$id]->price * $cart[$id]->quantity;
        // Update the session
        $_SESSION['nkrs']['id_cart'] = $product->id_cart;
        $_SESSION['nkrs']['cart'][$id] = $cart[$id];
        $this->_calc_cart();
        // Return the product object
        return $cart[$id];
    }

    /**
     * Saves the cart to go to checkout method
     * @param  array     $data
     * @return object
     */
    public function save($data = array(), $site = false)
    {
        // Get the cart
        $cart = $this->get();
        if (!empty($data) && $data){
            $data = !is_object($data) ? (object) $data : $data;
            // Update the quantity
            foreach ($cart as $id => $item) {
                $cart[$id]->quantity = $data->quantity[$id];
                $cart[$id]->total = $cart[$id]->price * $data->quantity[$id];
                if($site)
                    $cart[$id]->image = str_replace($site, '', $cart[$id]->image);
            }
        }
        // Update session
        $_SESSION['nkrs']['cart'] = $cart;
        ST_Curl::post('cart/save', array(
            'id_cart' => $_SESSION['nkrs']['id_cart'],
            'products' => $cart
        ), FALSE);
        // Calculate the cart
        $this->_calc_cart();
        return $this;
    }

    public function get()
    {
        return $_SESSION['nkrs']['cart'];
    }

    public function get_total()
    {
        return $this->get_resume()->items;
    }

    public function get_resume()
    {
        return (object) $_SESSION['nkrs']['resume'];
    }

    public function reset()
    {
        $_SESSION['nkrs']['cart'] = array();
        $_SESSION['nkrs']['resume'] = array(
            'coupon'    => 0,
            'freight'   => 0,
            'gift'      => 0,
            'items'     => 0,
            'subtotal'  => 0,
            'total'     => 0
        );
        $this->save();
    }

    public function remove($key)
    {
        if (isset($_SESSION['nkrs']['cart'][$key])){
            $_SESSION['nkrs']['resume']['items'] -= 1;
            unset($_SESSION['nkrs']['cart'][$key]);
            $this->_calc_cart();
            $this->save();
            return $this->get_total();
        } else
            return FALSE;
    }

    private function _calc_cart()
    {
        $cart = $this->get();

        $subtotal = 0;
        foreach ($cart as $id => $item) {
            $subtotal += $item->total;
        }

        $_SESSION['nkrs']['resume']['subtotal']  = $subtotal;
        $_SESSION['nkrs']['resume']['total']     = $subtotal;
    }

}