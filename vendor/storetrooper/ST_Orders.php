<?php
/**
 * Class ST_Order
 *
* @package Storetrooper
**/
class ST_Orders
{

    public function add($data)
    {
        $gift = $_SESSION['nkrs']['gift'];
        $id_cart = $_SESSION['nkrs']['id_cart'];

        if ($gift){
            $data['gift'] = $gift;
        }
        if ($id_cart){
            $data['id_cart'] = $id_cart;
        }

        return ST_Curl::post('orders/add', $data, FALSE);
    }

    public function get($params = array())
    {
        $params['client'] = $_SESSION['nkrs']['client']['id'];

        return ST_Curl::get('orders/get', $params, FALSE);
    }

}